package com.gambeat.mobile;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class  MainActivity extends FlutterActivity {

    private static final  String CHANNEL = "sexy";

    MethodChannel channel;


  private Socket mSocket;
  {
    try {
      //mSocket = IO.socket("http://192.168.56.1:4000/");
      mSocket = IO.socket("http://40.87.11.131:4000");

    } catch (URISyntaxException e) {}

  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    GeneratedPluginRegistrant.registerWith(this);

      channel = new MethodChannel(getFlutterView(),CHANNEL);

      channel.setMethodCallHandler(new MethodChannel.MethodCallHandler() {
          @Override
          public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {

              if(methodCall.method.equalsIgnoreCase("connectToChatRelayServer")){

                  Log.v("tiller","bitch got here");
                  mSocket.connect();



                  mSocket.on("new text", onNewText);

                  mSocket.on("receiving message", onReceivingMessage);

                  mSocket.on("sent message seen", onMessageSeen);

                  mSocket.on("sent message delivered", onMessageDelivered);

                  result.success(true);

              }else if(methodCall.method.equalsIgnoreCase("sendingMessage")){

                  Log.v("tiller",methodCall.arguments.toString().replace("=", ":"));

                  mSocket.emit("sending message", methodCall.arguments.toString().replace("=", ":"));

              }else if(methodCall.method.equalsIgnoreCase("readMessage")){

                  Log.v("tiller",methodCall.arguments.toString().replace("=", ":"));

                  mSocket.emit("message is read", methodCall.arguments.toString().replace("=", ":"));

              }else if(methodCall.method.equalsIgnoreCase("deliveredMessage")){

                  Log.v("tiller",methodCall.arguments.toString().replace("=", ":"));

                  mSocket.emit("message delivered", methodCall.arguments.toString().replace("=", ":"));

              }else if(methodCall.method.equalsIgnoreCase("joinChatServer")){


              Log.v("tiller",methodCall.arguments.toString().replace("=", ":"));

                  try {
                      mSocket.emit("join chat server", new JSONObject(methodCall.arguments.toString().replace("=", ":")));
                  } catch (JSONException e) {
                      e.printStackTrace();
                  }


              }


          }
      });


  }


    private Emitter.Listener onNewText = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //Log.v("tiller","blew");
            channel.invokeMethod("callTimer","from socketio");
        }
    };

    private Emitter.Listener onReceivingMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //Log.v("tiller","blew");
            channel.invokeMethod("receivingMessage",args);
        }
    };

    private Emitter.Listener onMessageSeen = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //Log.v("tiller","blew");
            channel.invokeMethod("sentMessageSeen",args);
        }
    };

    private Emitter.Listener onMessageDelivered = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            //Log.v("tiller","blew");
            channel.invokeMethod("sentMessageDelivered",args);
        }
    };

}
