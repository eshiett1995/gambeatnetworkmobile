import 'package:Gambeat/models/Competition.dart';

class CompetitionPageModel{

  CompetitionPageModel();

  int totalElements;

  int totalPage;

  bool last;

  int size;

  int number;

  String sort;

  bool first;

  int numberOfElements;

  List<Competition> content;

  factory CompetitionPageModel.fromJson(Map<String, dynamic> parsedJson) {

    CompetitionPageModel competitionPageModel = new CompetitionPageModel();

    competitionPageModel.totalElements = parsedJson['totalElements'];

    competitionPageModel.totalPage = parsedJson['totalPage'];

    competitionPageModel.last = parsedJson['last'];

    competitionPageModel.size = parsedJson['size'];

    competitionPageModel.number = parsedJson['number'];

    competitionPageModel.sort = parsedJson['sort'];

    competitionPageModel.first = parsedJson['first'];

    competitionPageModel.numberOfElements = parsedJson['numberOfElements'];

    var list = parsedJson['content'] as List;

    competitionPageModel.content = list.map((i) => Competition.fromJson(i)).toList();

    return competitionPageModel;
  }

}