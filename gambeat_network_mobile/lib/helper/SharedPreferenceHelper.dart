import 'dart:convert';

import 'package:Gambeat/models/User.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceHelper {
  ///
  /// Instantiation of the SharedPreferences library
  ///
  final String _sessionId = "sessionId";
  final String _firebaseToken = "firebaseToken";
  final String _authToken = "authToken";
  final String _currentUserPrefs = "currentUser";
  final String _isLoggedIn = "isLoggedIn";


/// ------------------------------------------------------------
  /// Method that returns the sessionID of the logged in user
  /// ------------------------------------------------------------
  Future<String> getSessionID() async {

    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_sessionId) ?? null;
  }

  /// ----------------------------------------------------------
  /// Method that saves the user's Session ID
  /// ----------------------------------------------------------
  Future<bool> setSessionId(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_sessionId, value);
  }


  /// ------------------------------------------------------------
  /// Method that returns the sessionID of the logged in user
  /// ------------------------------------------------------------
  Future<String> getFirebaseToken() async {

    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_firebaseToken) ?? null;

  }

  /// ----------------------------------------------------------
  /// Method that saves the user's Session ID
  /// ----------------------------------------------------------
  Future<bool> setFirebaseToken(String value) async {

    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_firebaseToken, value);

  }

  

  /// ------------------------------------------------------------
  /// Method that returns the jwt token of the logged in user
  /// ------------------------------------------------------------
  Future<String> getAuthToken() async {

    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_authToken) ?? null;
  }

  /// ----------------------------------------------------------
  /// Method that saves the user's jwt token 
  /// ----------------------------------------------------------
  Future<bool> setAuthToken(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_authToken, value);
  }

   /// ------------------------------------------------------------
  /// Method that returns the isLogged in status of the phone
  /// ------------------------------------------------------------
  Future<bool> getIsLoggedInStatus() async {

    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getBool(_isLoggedIn) ?? false;
  }

  /// ----------------------------------------------------------
  /// Method that saves the user's logged in status
  /// ----------------------------------------------------------
  Future<bool> setIsLoggedInStatus(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setBool(_isLoggedIn, value);
  }

  Future<bool> setUser(User user) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    //print(user.toJson().toString());

    return prefs.setString(_currentUserPrefs, user.toJson().toString());
  }

  Future<User> getUser() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    String foundString = prefs.getString(_currentUserPrefs);

   

    Map valueMap = json.decode(foundString);

    //print(valueMap);

    //print(valueMap['id']);

    User user = new User.fromJson(valueMap);

    //print(user.firstName);

    return user;
  }


  Future<bool> clearSharedPreference() async {

    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.clear();
  }
}
