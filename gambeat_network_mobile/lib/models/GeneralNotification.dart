



import 'package:Gambeat/models/Competition.dart';
import 'package:Gambeat/models/DefaultEntity.dart';
import 'package:Gambeat/models/Enum.dart';
import 'package:Gambeat/models/User.dart';

class GeneralNotification extends DefaultEntity {

  GeneralNotification();

  User user;

  String notificationHeader;

  String notificationMessage;

  NotificationType notificationType;

  User reference;

  Competition competition;

  bool isRead;

  factory GeneralNotification.fromJson(Map<String, dynamic> parsedJson) {

    GeneralNotification notification = new GeneralNotification();

    notification.id = parsedJson['id'];

    notification.dateCreated = parsedJson['dateCreated'];

    notification.user = User.fromJson(parsedJson['sender']);

    notification.notificationHeader = parsedJson['notificationHeader'];

    notification.notificationMessage = parsedJson['notificationMessage'];

    notification.reference = User.fromJson(parsedJson['reference']);

    notification.notificationType = getNotificationType(parsedJson['notificationType']);

    notification.competition = Competition.fromJson(parsedJson['competition']);

    notification.isRead = parsedJson['isRead'];

    return notification;

  }

  ///Returns a MessageStatus corresponding to the String value gotten from the Api
  static NotificationType getNotificationType(String notificationType) {
    switch (notificationType) {
      case "FRIENDREQUESTACCEPTED":
        return NotificationType.FRIENDREQUESTACCEPTED;
      case "PLAYERKNOCKEDOUT":
        return NotificationType.PLAYERKNOCKEDOUT;
      case "PLAYERWINSMATCH":
        return NotificationType.PLAYERWINSMATCH;
      case "TOURNAMENTSTARTED":
        return NotificationType.TOURNAMENTSTARTED;
      case "FRIENDHASMATCH":
        return NotificationType.FRIENDHASMATCH;
      case "TOURNAMENTWINNER":
        return NotificationType.TOURNAMENTWINNER;
      case "TOURNAMENTENDED":
        return NotificationType.TOURNAMENTENDED;
      case "FRIENDELIMINATED":
        return NotificationType.FRIENDELIMINATED;
      default:
        return NotificationType.FRIENDREQUESTACCEPTED;
    }
  }

  Map<String, dynamic> toJson() => {

    '"_id"': '"$id"',

    '"dateCreated"': dateCreated,

    '"user"': user == null ? null : user.toJson(),

    '"notificationHeader"': '"$notificationHeader"',

    '"notificationMessage"': '"$notificationMessage"',

    '"notificationType"': '"$notificationType"',

    '"reference"': reference == null ? null : reference.toJson(),

    '"competition"': competition == null ? null : competition.toJson(),

    '"isRead"': isRead,
  };
}