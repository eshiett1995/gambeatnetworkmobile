import 'dart:convert';
import 'dart:io';

import 'package:badge/badge.dart';
import 'package:Gambeat/chatpage.dart';
import 'package:Gambeat/fragments/messages/chat_list_item.dart';
import 'package:Gambeat/helper/SharedPreferenceHelper.dart';
import 'package:Gambeat/home2.dart';
import 'package:Gambeat/models/Message.dart';
import 'package:Gambeat/models/User.dart';
import 'package:Gambeat/models/rest-models/response/FriendAndMessagesModel.dart';
import 'package:Gambeat/models/rest-models/response/FriendsAndMessagesPaginationModel.dart';
import 'package:Gambeat/models/rest-models/response/MessageModel.dart';
import 'package:Gambeat/network/api/ApiHelper.dart';
import 'package:flutter/material.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

class ChatList extends StatefulWidget {
  var homePageState;

  ChatList({this.homePageState});

  @override
  ChatListState createState() => ChatListState();
}

class ChatListState extends State<ChatList> with TickerProviderStateMixin {
  int totalNumberOfPages = 0;
  List<FriendAndMessagesModel> friendAndMessagesModelList = new List();
  var _isInitialListLoading = false;
  User currentUser = new User();
  int currentPageNumber = 0;
  bool isLastPage = false;
  int currentLength = 0;
  bool _isSubsequentListLoading = false;

  int endOfPageCalled = 0;

  VoidCallback listenerCallback;

//  @override
//  void dispose() {
////    super.dispose();
//    //getTimeAgoF.removeListener();
//    print("chat-list dispose called ****************************** ");
//    getTimeAgoF = null;
//    super.dispose();
//  }

  @override
  Widget build(BuildContext context) {
    controller = new AnimationController(
        vsync: this, duration: new Duration(minutes: 1));
    Map<User, List<Message>> map = _getFriendAndMessageList();
    List<User> friends = _getListOfFriends();
    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      body: new Container(
          decoration: new BoxDecoration(
              image: new DecorationImage(
                colorFilter: new ColorFilter.mode(
                    Colors.black.withOpacity(0.2), BlendMode.dstATop),
                image: new AssetImage('images/background_main.png'),
                fit: BoxFit.cover,
              )),
          child: new Column(
            children: <Widget>[
              new Expanded(
                child: _isInitialListLoading
                    ? Container(
//                color:Colors.blueAccent,
                  height: MediaQuery
                      .of(context)
                      .size
                      .height,
                  decoration: new BoxDecoration(
                      image: new DecorationImage(
                        colorFilter: new ColorFilter.mode(
                            Colors.black.withOpacity(0.2), BlendMode.dstATop),
                        image: new AssetImage('images/games.jpg'),
                        fit: BoxFit.cover,
                      )),
                  child: Center(child: CircularProgressIndicator()),
                )
                    : LazyLoadScrollView(
                    child: ListView.builder(
                      itemCount: currentLength,
                      itemBuilder: (BuildContext context, int index) {
                        return new ChatListItem(
                          map: map,
                          friend: friends[index],
                          controller: controller,
                          currentUser: currentUser,
                          itemTappedFunc: () {
                            friendPanelTapped(friends[index]);
                          },
                        );
//                        return _getListItemWidget(map, friends[index]);
                      },
                    ),
                    onEndOfPage: () => _lazyLoading()),
              ),
              _isSubsequentListLoading ? _getBottomProgressbar() : Center()
            ],
          )),
    );
  }

  void friendPanelTapped(User friend) {
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return new Container(
            color: Colors.white,
            child: new Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(
                  height: 20.0,
                ),
                Text("SELECT AN OPTION:"),
                ListTile(
                  leading: Icon(Icons.account_circle),
                  title: Text("Profile"),
                ),
                ListTile(
                  leading: Icon(Icons.message),
                  title: Text("Message"),
                  onTap: () {
                    goToChatPage(
                        friend: friend, friendMsg: _getFriendAndMessageList());
                  },
                ),
                ListTile(
                  leading: Icon(Icons.reply),
                  title: Text("Unfollow"),
                ),
              ],
            ),
          );
        });
  }

  void goToChatPage({User friend, Map<User, List<Message>> friendMsg}) {
    //getTimeAgoF.removeListener();
    //print("go to chat page called ****************************** ");
    if (currentUser == null) return;
    Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => new ChatScreen(
          currentUser: currentUser,
          friend: friend,
          messages: friendMsg[friend],
        )));
  }

  @override
  void initState() {
    super.initState();
    _getMessageModel(pageNum: 1);
    _getCurrentUser();
    widget.homePageState.initSearchCallback((String searchText) {
      _searchDatabase(searchText);
    });
  }

  ///This method takes care of searching through the database for the required text
  _searchDatabase(String searchText) {
    print("Search Text Chat-List -- " + searchText);
  }

  ///When there's no unread message this simply returns a widget showing the
  ///profile if unread message count != 0 it returns a badge widget showing the
  /// unread message count
  _getBadge(bool hasUnreadMsg, String unreadMsgCount) {
    if (hasUnreadMsg)
      return new Badge(
        color: Colors.blue,
        borderSize: 1.0,
        positionTop: 0.0,
        textStyle: TextStyle(
          color: Colors.white,
        ),
        positionRight: -3.0,
        borderColor: Colors.black,
        value: unreadMsgCount,
        // value to show inside the badge
        child: new CircleAvatar(
          radius: 35.0,
          backgroundImage: AssetImage('images/chris.jpg'),
        ), // text to append (required)
      );
    else
      return new CircleAvatar(
        radius: 35.0,
        backgroundImage: AssetImage('images/chris.jpg'),
      );
  }

  Widget _getBottomProgressbar() {
    return new LinearProgressIndicator();
  }

  ///-------------------------------------------------------------------------
  ///This method gets and initialize the current user
  ///-------------------------------------------------------------------------
  Future _getCurrentUser() async {
    SharedPreferenceHelper sharedPreferenceHelper =
    new SharedPreferenceHelper();
    currentUser = await sharedPreferenceHelper.getUser();
    return currentUser;
  }

  ///This method returns a map of key User and value of List<Message>
  ///to enable easy mapping when initializing the item widget
  Map<User, List<Message>> _getFriendAndMessageList() {
    Map<User, List<Message>> userLastMessageMap = new Map();
    for (FriendAndMessagesModel friendAndMessagesModel
    in friendAndMessagesModelList) {
      userLastMessageMap[friendAndMessagesModel.friend] =
          friendAndMessagesModel.messages;
    }
    return userLastMessageMap;
  }

//  timeAgoCallBack(String newTimeAgo) {
//    setState(() {
//      timeAgo = newTimeAgo;
//    });
//  }

  String time = "";
  AnimationController controller;

//  GetTimeAgo getTimeAgoF;

  ///This method returns the list item widget passing in the necessary params
  ///to help initialize the value of the list item widget
//  Widget _getListItemWidget(Map<User, List<Message>> map, User friend) {
//    Message message = map[friend][map[friend].length - 1];
//    //String messageCount = message.;
//    String messageStr = message.message;
//    getTimeAgoF =
//        new GetTimeAgo(message.dateCreated, function: (String newTimeAgo) {
//      ValueKey b = new ValueKey("hi");
////          if(this.mounted)
////          setState(() {
////            time = newTimeAgo;
////          });
//    }, controller: controller, string: "chat-list");
//    time = getTimeAgoF.getTimeAgo();
//    String unreadMsgCount;
//    int unreadMsgCountInt;
//    User user = new User();
//    bool isLastMsgSenderTheCurrentUser =
//        _getSenderIsCurrentUser(message.sender, message.receiver);
//    if (isLastMsgSenderTheCurrentUser) {
//      user = message.sender;
//      unreadMsgCount = null;
//    } else {
//      user = message.receiver;
//      unreadMsgCount = user.unreadMessagesCount.toString();
//      unreadMsgCountInt = user.unreadMessagesCount;
//    }
//    return GestureDetector(
//      onTap: () {
//        friendPanelTapped(friend);
//      },
//      child: Card(
//        margin: EdgeInsets.symmetric(vertical: 15.0, horizontal: 10.0),
//        elevation: 3.0,
//        child: Padding(
//            padding: EdgeInsets.all(15.0),
//            child: new Row(
//              children: <Widget>[
//                //If unreadMsgCount is null (no unread message(s)) hide the badge
//                //else show it
//                _getBadge(unreadMsgCount != null && unreadMsgCountInt != 0,
//                    unreadMsgCount),
//                Padding(
//                  padding: EdgeInsets.only(right: 10.0),
//                ),
//                Flexible(
//                  child: Column(
//                    crossAxisAlignment: CrossAxisAlignment.start,
//                    children: <Widget>[
//                      Container(
//                        child: Stack(
//                          children: <Widget>[
//                            Row(
//                              mainAxisAlignment: MainAxisAlignment.end,
//                              children: <Widget>[
//                                Text(
//                                  time,
//                                  textDirection: TextDirection.ltr,
//                                  textAlign: TextAlign.left,
//                                  style: TextStyle(fontWeight: FontWeight.bold),
//                                )
//                              ],
//                            ),
//                            Row(
//                              children: <Widget>[
//                                Text(
//                                  "eshiett1995",
//                                  textAlign: TextAlign.left,
//                                  style: TextStyle(fontWeight: FontWeight.bold),
//                                ),
//                              ],
//                            ),
//                          ],
//                        ),
//                      ),
//                      SizedBox(
//                        height: 7.0,
//                      ),
//                      Text(
//                        messageStr,
//                        textAlign: TextAlign.left,
//                        style: TextStyle(color: Colors.black54),
//                        overflow: TextOverflow.ellipsis,
//                      ),
//                    ],
//                  ),
//                )
//              ],
//            )),
//      ),
//    );
//  }

  ///This method returns a list of Friends (User), which is gotten from the api
  ///call
  List<User> _getListOfFriends() {
    List<User> friends = new List<User>();
    for (FriendAndMessagesModel friendAndMessagesModel
    in friendAndMessagesModelList) {
      friends.add(friendAndMessagesModel.friend);
    }
    return friends;
  }

  ///An async task method to get the items from the API
  Future _getMessageModel({int pageNum}) async {
    //setState(() {
    if (pageNum == 0) _setInitialListLoadingTrue();
//      else
//        _setInitialListLoadingFalse();
    //});
    MessageModel messageModel = new MessageModel();

    ApiHelper apiHelper = new ApiHelper();
    HttpClientResponse messageModelResponse =
    await apiHelper.getFriendsMessageModel(pageNum);
    String respnse = await messageModelResponse.transform(utf8.decoder).join();
    if (messageModelResponse.statusCode == 200) {
      print("messageModelResponse statusCode == 200");
      final responseJson = json.decode(respnse);

      messageModel = MessageModel.fromJson(json.decode(respnse));
      print("ayeee");
      //print(messageModel.friendsAndMessagesModelPage.content[0].messages[0].dateCreated);
      if (messageModel != null) _initFieldsRelatingToThisPage(messageModel);
      if (pageNum == 0) {
        _setInitialListLoadingFalse();
      }
    } else {
      if (pageNum == 0) _setInitialListLoadingFalse();
      print("messageModelResponse statusCode != 200");
    }

    print("_getMessageModel() --- " + respnse);
  }

  ///-------------------------------------------------------------------------
  ///This method checks to see if the current logged in account is the sender of
  ///the last message, returns true or is the receiver of the last message and returns false
  ///-------------------------------------------------------------------------
  bool _getSenderIsCurrentUser(User sender, User receiver) {
    if (sender.id == currentUser.id) return true;
    if (receiver.id == currentUser.id) return false;
    return false;
  }

  void _initFieldsRelatingToThisPage(MessageModel messageModel) {
    FriendsAndMessagesPaginationModel friendAndMessagesPageModel =
        messageModel.friendsAndMessagesModelPage;
    isLastPage = friendAndMessagesPageModel.last;
    totalNumberOfPages = friendAndMessagesPageModel.totalElements;
    setState(() {
      friendAndMessagesModelList.addAll(friendAndMessagesPageModel.content);
      currentLength = friendAndMessagesPageModel.content.length;
    });
  }

  void _lazyLoading() {
    endOfPageCalled = endOfPageCalled + 1;
    print("===========================================================");
    print("LazyMethod Called!! --- end of Page");
    print("===========================================================");
    if (!(endOfPageCalled > 1)) //This is so to prevent multiple calling
      if (!(currentPageNumber >= totalNumberOfPages)) {
        print(
            "currentPageNumber NOT greater than or equal to totalNumberOfPages -- ");
        currentPageNumber = currentPageNumber + 1;
        _loadMoreData(pageNum: currentPageNumber);
      }
  }

  Future _loadMoreData({int pageNum}) async {
    _setSubsequentListLoadingTrue();

    MessageModel messageModel = new MessageModel();

    ApiHelper apiHelper = new ApiHelper();
    HttpClientResponse messageModelResponse =
    await apiHelper.getFriendsMessageModel(pageNum);
    String respnse = await messageModelResponse.transform(utf8.decoder).join();
    if (messageModelResponse.statusCode == 200) {
      print("messageModelResponse statusCode == 200");
      final responseJson = json.decode(respnse);
      print(responseJson.toString());
      messageModel = MessageModel.fromJson(json.decode(respnse));
      if (messageModel != null) {
        setState(() {
          endOfPageCalled = 0;
          for (FriendAndMessagesModel friendAndMessagesModel in messageModel
              .friendsAndMessagesModelPage
              .content) friendAndMessagesModelList.add(friendAndMessagesModel);
          currentLength = friendAndMessagesModelList.length;
        });
        _setSubsequentListLoadingFalse();
      }
    } else
      _setSubsequentListLoadingFalse();
  }

  ///-------------------------------------------------------------------------
  ///This method sets state setting _initialListLoading bool var to false
  ///so as to control the progressbar indicating initial list items are being
  ///fetched from the api
  ///-------------------------------------------------------------------------
  _setInitialListLoadingFalse() {
    setState(() {
      _isInitialListLoading = false;
    });
  }

  ///This method sets state setting _initialListLoading bool var to true
  ///so as to control the progressbar indicating initial list items are being
  ///fetched from the api
  _setInitialListLoadingTrue() {
    setState(() {
      _isInitialListLoading = true;
    });
  }

  _setSubsequentListLoadingFalse() {
    setState(() {
      _isSubsequentListLoading = false;
    });
  }

  _setSubsequentListLoadingTrue() {
    setState(() {
      _isSubsequentListLoading = true;
    });
  }
}
