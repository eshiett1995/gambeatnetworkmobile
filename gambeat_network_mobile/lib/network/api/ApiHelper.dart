import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:Gambeat/models/FriendRequest.dart';
import 'package:Gambeat/models/Game.dart';
import 'package:Gambeat/models/Transactions.dart';
import 'package:Gambeat/models/rest-models/request/EnterCompetitionModel.dart';
import 'package:Gambeat/models/rest-models/request/FirebaseModel.dart';
import 'package:Gambeat/models/rest-models/request/GameSearchModel.dart';
import 'package:async/async.dart';
import 'package:dio/dio.dart';
import 'package:Gambeat/helper/SharedPreferenceHelper.dart';
import 'package:Gambeat/models/User.dart';
import 'package:Gambeat/models/rest-models/request/TemporaryStore.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';

class ApiHelper {
  String baseURL = "http://40.87.11.131:3000/";//"http://gambeat.com.ng:8080/";
  static const String FRIENDS_MESSAGES_URL = "api/message/chat/all/";
  static const String MESSAGES_URL = "api/message/chat/";
  static const String SEND_FRIEND_REQUEST_URL =
      "api/social/send/friend-request";
  static const String REJECT_FRIEND_REQUEST_URL =
      "api/social/decline/friend-request/";
  static const String ACCEPT_FRIEND_REQUEST_URL =
      "api/social/accept/friend-request/";
  static const String GET_ALL_FRIEND_REQUEST_URL =
      "api/social/friend-requests";

  static const String JOIN_COMPETITION =
      "api/third-party/activity/join/competition";
  static const String CREATE_COMPETITION =
      "api/third-party/activity/create/competition";
  static const String GET_ALL_COMPETITIONS =
      "api/activity/competitions/";
  static const String GET_ALL_GAMES = "api/activity/games/";


  Future<HttpClientResponse> signUp(User user) async {
    user.gender = "UNSPECIFIED";

    user.onlineStatus = "ONLINE";

    user.userType = "PLAYER";

    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient
        .postUrl(Uri.parse(baseURL + "api/signup"));
    request.headers.set('content-type', 'application/json');
    request.add(utf8.encode(user.toJson().toString()));
    HttpClientResponse response = await request.close();
    httpClient.close();

    return response;
  }

  Future<HttpClientResponse> login(User user) async {
    user.gender = "UNSPECIFIED";

    user.onlineStatus = "ONLINE";

    user.userType = "PLAYER";

    HttpClient httpClient = new HttpClient();

    HttpClientRequest request =
        await httpClient.postUrl(Uri.parse(baseURL + "api/login"));
    request.headers.set('content-type', 'application/json');
    request.add(utf8.encode(user.toJson().toString()));
    HttpClientResponse response = await request.close();
    //print(response.statusCode);
    // todo - you should check the response.statusCode
    //String reply = await response.transform(utf8.decoder).join();
    httpClient.close();

//print(reply);

    return response;
  }

  Future<http.Response> register(User user) async {
    http.Response response = await http.post(
      Uri.encodeFull(baseURL + "register/"),
      headers: {"Accepts": "application/json"},
      body: user,
    );

    return response;
  }


  Future<HttpClientResponse> initTransaction(Transactions transaction) async {

    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(baseURL + "api/wallet/init-transaction"));
    request.headers.set('content-type', 'application/json');
    request.headers.add('bearer', await SharedPreferenceHelper().getAuthToken());
    request.add(utf8.encode(transaction.toJson().toString()));
    HttpClientResponse response = await request.close();
    httpClient.close();

    return response;

  }


  Future<HttpClientResponse> getTransactionsByPage(int page) async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.getUrl(
        Uri.parse(baseURL + "api/third-party/wallet/transactions/$page"));
    request.headers.set('content-type', 'application/json');
    request.headers.add('bearer',
        "Bearer ${await SharedPreferenceHelper().getAuthToken()}");

    HttpClientResponse response = await request.close();

    httpClient.close();

    return response;
  }

  Future<HttpClientResponse> getNotificationsByPage(int page) async {

    HttpClient httpClient = new HttpClient();

    HttpClientRequest request = await httpClient.getUrl(Uri.parse(baseURL + "api/notification/general-notification/$page"));

    request.headers.set('content-type', 'application/json');

    request.headers.add('bearer', await SharedPreferenceHelper().getAuthToken());

    HttpClientResponse response = await request.close();

    httpClient.close();

    return response;

  }

  Future<HttpClientResponse> getGamificationsByPage(int page) async {

    HttpClient httpClient = new HttpClient();

    HttpClientRequest request = await httpClient.getUrl(Uri.parse(baseURL + "api/notification/gamification/$page"));

    request.headers.set('content-type', 'application/json');

    request.headers.add('bearer', await SharedPreferenceHelper().getAuthToken());

    HttpClientResponse response = await request.close();

    httpClient.close();

    return response;

  }

  Future<HttpClientResponse> getFriendsByPage(int page) async {

    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient
        .getUrl(Uri.parse(baseURL + "api/social/friends/$page"));
    request.headers.set('content-type', 'application/json');
    request.headers.add('bearer', await SharedPreferenceHelper().getAuthToken());

    HttpClientResponse response = await request.close();
    //print(response.statusCode);
    // todo - you should check the response.statusCode
    //String reply = await response.transform(utf8.decoder).join();
    httpClient.close();

    return response;
  }


  Future<HttpClientResponse> getAllUsersByPage(int page) async {

    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient
        .getUrl(Uri.parse(baseURL + "api/social/users/$page"));
    request.headers.set('content-type', 'application/json');
    request.headers.add('bearer', await SharedPreferenceHelper().getAuthToken());

    HttpClientResponse response = await request.close();
    //print(response.statusCode);
    // todo - you should check the response.statusCode
    //String reply = await response.transform(utf8.decoder).join();
    httpClient.close();

    return response;
  }

  Future<HttpClientResponse> getFriendsMessageModel(int page) async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient
        .getUrl(Uri.parse(baseURL + FRIENDS_MESSAGES_URL + "$page"));
    request.headers.set('content-type', 'application/json');
    request.headers.add('bearer', await SharedPreferenceHelper().getAuthToken());

    HttpClientResponse response = await request.close();
    httpClient.close();

    return response;
  }

  Future<HttpClientResponse> getAllGames(int page, GameSearchModel gameSearchModel) async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request =
        await httpClient.postUrl(Uri.parse(baseURL + GET_ALL_GAMES + "$page"));
    request.headers.set('content-type', 'application/json');
    request.add(utf8.encode(gameSearchModel.toJson().toString()));
    HttpClientResponse response = await request.close();
    httpClient.close();

    return response;
  }


  Future<HttpClientResponse> getAllCompetitions(
      int page, {bool participant: false}) async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(
        Uri.parse(baseURL + GET_ALL_COMPETITIONS + "$participant/$page"));
    request.headers.set('content-type', 'application/json');
    request.headers.add('bearer',await SharedPreferenceHelper().getAuthToken());

    HttpClientResponse response = await request.close();
    httpClient.close();

    return response;
  }

  Future<HttpClientResponse> getMessageModel(String userId, int page) async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient
        .getUrl(Uri.parse(baseURL + MESSAGES_URL + "$userId" + "/" + "$page"));
    print("messageModelUrl ---- " +
        Uri.parse(baseURL + MESSAGES_URL + "$userId" + "/" + "$page")
            .toString());
    request.headers.set('content-type', 'application/json');
    request.headers.add('bearer', await SharedPreferenceHelper().getAuthToken());

    HttpClientResponse response = await request.close();
    httpClient.close();

    return response;
  }

  Future<HttpClientResponse> getAllFriendRequest() async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.getUrl(Uri.parse(baseURL + GET_ALL_FRIEND_REQUEST_URL));
    request.headers.set('content-type', 'application/json');
    request.headers.add('bearer', await SharedPreferenceHelper().getAuthToken());
    HttpClientResponse response = await request.close();
    httpClient.close();

    return response;
  }

  Future<HttpClientResponse> acceptOrRejectFriendRequest(FriendRequest friendRequest, bool accept) async {
    HttpClient httpClient = new HttpClient();


    HttpClientRequest request = await httpClient.getUrl(Uri.parse(accept
        ? baseURL + ACCEPT_FRIEND_REQUEST_URL + "${friendRequest.iid}"
        : baseURL + REJECT_FRIEND_REQUEST_URL + "${friendRequest.iid}"));
    request.headers.set('content-type', 'application/json');
    request.headers.add('bearer', await SharedPreferenceHelper().getAuthToken());

    HttpClientResponse response = await request.close();
    httpClient.close();

    return response;
  }

  Future<HttpClientResponse> sendFriendRequest(FriendRequest friendRequest) async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient
        .postUrl(Uri.parse(baseURL + SEND_FRIEND_REQUEST_URL));
    request.headers.set('content-type', 'application/json');
    request.headers.add('bearer', await SharedPreferenceHelper().getAuthToken());

    HttpClientResponse response = await request.close();
    httpClient.close();

    return response;
  }

  Future<HttpClientResponse> createCompetition(
      EnterCompetitionModel competitionModel) async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient
        .getUrl(Uri.parse(baseURL + CREATE_COMPETITION + "$competitionModel"));
    request.headers.set('content-type', 'application/json');
    request.headers.add('bearer',
        'Bearer ${await SharedPreferenceHelper().getAuthToken()}');

    HttpClientResponse response = await request.close();
    httpClient.close();

    return response;
  }

  Future<HttpClientResponse> joinCompetition(
      EnterCompetitionModel competitionModel) async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient
        .getUrl(Uri.parse(baseURL + JOIN_COMPETITION + "$competitionModel"));
    request.headers.set('content-type', 'application/json');
    request.headers.add('bearer',
        'Bearer ${await SharedPreferenceHelper().getAuthToken()}');

    HttpClientResponse response = await request.close();
    httpClient.close();

    return response;
  }

  Future<HttpClientResponse> removeUserProfilePicture() async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.getUrl(
        Uri.parse(baseURL + "api/third-party/profile-picture/social/remove"));
    request.headers.set('content-type', 'application/json');
    request.headers.add('bearer',
        'Bearer ${await SharedPreferenceHelper().getAuthToken()}');

    HttpClientResponse response = await request.close();
    //print(response.statusCode);
    // todo - you should check the response.statusCode
    //String reply = await response.transform(utf8.decoder).join();
    httpClient.close();

//print(reply);

    return response;
  }

  Future<Response> uploadProfileImage(File file, User user) async {
    var dio = new Dio();

    FormData formData =
        new FormData.from({"file": new UploadFileInfo(file, user.userName)});

    Response response = await new Dio().post("/info", data: formData);

    return response;
  }

  Future<HttpClientResponse> updateUserBasicInfo(User user) async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient
        .postUrl(Uri.parse(baseURL + "api/update/profile/basic"));
    request.headers.set('content-type', 'application/json');
    request.headers.add('bearer', await SharedPreferenceHelper().getAuthToken());
    request.add(utf8.encode(user.toJson().toString()));
    HttpClientResponse response = await request.close();
    //print(response.statusCode);
    // todo - you should check the response.statusCode
    //String reply = await response.transform(utf8.decoder).join();
    httpClient.close();

//print(reply);

    return response;
  }

  Future<HttpClientResponse> updateUserEmail(
      TemporaryStore temporaryStore) async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(
        Uri.parse(baseURL + "api/update/profile/email"));
    request.headers.set('content-type', 'application/json');
    request.headers.add('bearer', await SharedPreferenceHelper().getAuthToken());
    request.add(utf8.encode(temporaryStore.toJson().toString()));
    HttpClientResponse response = await request.close();
    //print(response.statusCode);
    // todo - you should check the response.statusCode
    //String reply = await response.transform(utf8.decoder).join();
    httpClient.close();

//print(reply);

    return response;
  }


  Future<HttpClientResponse> verifySecurityUpdate(TemporaryStore temporaryStore) async {

    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(
        Uri.parse(baseURL + "api/update/authorize"));
    request.headers.set('content-type', 'application/json');
    request.headers.add('bearer', await SharedPreferenceHelper().getAuthToken());
    request.add(utf8.encode(temporaryStore.toJson().toString()));
    HttpClientResponse response = await request.close();
    //print(response.statusCode);
    // todo - you should check the response.statusCode
    //String reply = await response.transform(utf8.decoder).join();
    httpClient.close();

//print(reply);

    return response;
  }

  Future<HttpClientResponse> updateUserPhoneNumber(
      TemporaryStore temporaryStore) async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(
        baseURL + "api/update/profile/phoneNumber"));
    request.headers.set('content-type', 'application/json');
    request.headers.add('bearer', await SharedPreferenceHelper().getAuthToken());
    request.add(utf8.encode(temporaryStore.toJson().toString()));
    HttpClientResponse response = await request.close();
    //print(response.statusCode);
    // todo - you should check the response.statusCode
    //String reply = await response.transform(utf8.decoder).join();
    httpClient.close();

//print(reply);

    return response;
  }

  Future<HttpClientResponse> updateUsersUserName(
      TemporaryStore temporaryStore) async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(
        Uri.parse(baseURL + "api/update/profile/userName"));
    request.headers.set('content-type', 'application/json');
    request.headers.add('bearer', await SharedPreferenceHelper().getAuthToken());
    request.add(utf8.encode(temporaryStore.toJson().toString()));
    HttpClientResponse response = await request.close();
    //print(response.statusCode);
    // todo - you should check the response.statusCode
    //String reply = await response.transform(utf8.decoder).join();
    httpClient.close();

//print(reply);

    return response;
  }

  Future<HttpClientResponse> updateUserPassword(
      TemporaryStore temporaryStore) async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(baseURL + "api/update/profile/password"));
    request.headers.set('content-type', 'application/json');
    request.headers.add('bearer', await SharedPreferenceHelper().getAuthToken());
    request.add(utf8.encode(temporaryStore.toJson().toString()));
    HttpClientResponse response = await request.close();
    httpClient.close();

    return response;
  }

  Future<HttpClientResponse> registerTokenWithUser(String token) async {


    FirebaseModel firebaseModel = new FirebaseModel();

    firebaseModel.token = token;

    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(
        Uri.parse(baseURL + "api/firebase/register/token"));
    request.headers.set('content-type', 'application/json');
    request.headers.add('bearer',await SharedPreferenceHelper().getAuthToken());
    request.add(utf8.encode(firebaseModel.toJson().toString()));

    HttpClientResponse response = await request.close();

    //print(response.statusCode);
    // todo - you should check the response.statusCode
    //String reply = await response.transform(utf8.decoder).join();
    httpClient.close();

//print(reply);

    return response;
  }

  Future<http.StreamedResponse> uploadProfileImageToServer(File imageFile) async{

    var stream = new http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
    var length = await imageFile.length();

    var uri = Uri.parse("${baseURL}api/update/profile-picture");

    http.MultipartRequest request = new http.MultipartRequest("POST", uri);

    request.headers['bearer'] = await SharedPreferenceHelper().getAuthToken();

    var multipartFile = new http.MultipartFile('file', stream, length, filename: basename(imageFile.path));
    //contentType: new MediaType('image', 'png'));

    request.files.add(multipartFile);

    var response = await request.send();

    return response;

  }

}
