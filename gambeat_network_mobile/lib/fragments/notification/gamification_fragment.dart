import 'dart:convert';
import 'dart:io';

import 'package:Gambeat/models/rest-models/response/GamificationPaginationModel.dart';
import 'package:Gambeat/network/api/ApiHelper.dart';
import 'package:flutter/material.dart';

class GamificationFragment extends StatefulWidget {
  @override
  _GamificationFragmentState createState() => _GamificationFragmentState();
}

class _GamificationFragmentState extends State<GamificationFragment> {


  GamificationPaginationModel gamificationPaginationModel;

  void getGamificationsFromServer() async{

    HttpClientResponse gamificationModelResponse = await new ApiHelper().getGamificationsByPage(1);

    String response = await gamificationModelResponse.transform(utf8.decoder).join();

    if (gamificationModelResponse.statusCode == 200) {

       final responseJson = json.decode(response);

       gamificationPaginationModel = GamificationPaginationModel.fromJson(json.decode(responseJson));

    } else {

    }

  }

  @override
  void initState() {

    super.initState();

    getGamificationsFromServer();

  }


  @override
  Widget build(BuildContext context) {

    void showMatchInfo() {
      showModalBottomSheet(
          context: context,
          builder: (builder) {
            return new Container(
              padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
              color: Colors.white,
              child: ListView(

                children: <Widget>[
                  new Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(
                        height: 20.0,
                      ),

                      Row(
                        children: <Widget>[
                          Text("Hello Bassey", style: TextStyle(fontWeight: FontWeight.bold),),

                          Spacer(),

                          new Image.asset('images/scroll.gif', width: 50.0, height: 50.0,),

                        ],
                      ),

                      SizedBox(
                        height: 20.0,
                      ),

                      Text("          A matched has been fixed between you and Nsikan, he seems to be a tough opponent but i feel" +
                          "he can be beaten with enough concentration and a good tactic. We on the Gambeat team are wishing you the best of luck.",

                        textAlign: TextAlign.justify,
                        style: TextStyle(
                          height: 1.3,
                          color: Colors.black87
                        ),
                      ),

                      Text("          Please note that every match is supposed to be played within 2 days of notification and " +
                          "any delay in match will lead to your disqualification also we advice that you should be in constant" +
                          "communication with your competitor, so as to set a date and time for the match.",

                        style: TextStyle(
                          height: 1.3,
                        ),
                        textAlign: TextAlign.justify,),

                      Text("          Finally, please note that you have the option to forfiet the match, the forfiet button is below.",
                         style: TextStyle(
                           height: 1.3,
                         ),
                      ),

                      SizedBox(height: 30.0,),

                      RaisedButton(
                          onPressed: (){},
                          color: Colors.blue,
                          child: Container(
                            width: double.infinity,
                            height: 50.0,
                            child: Center(
                              child: Text("NSIKANS PROFILE", style: TextStyle(color: Colors.white),),
                            ),
                          )
                      ),

                      SizedBox(height: 30.0,),

                      RaisedButton(
                          onPressed: (){},
                          color: Colors.red,
                          child: Container(
                            width: double.infinity,
                            height: 50.0,
                            child: Center(
                              child: Text("FORFEIT", style: TextStyle(color: Colors.white),),
                            ),
                          )
                      ),




                    ],
                  ),
                ],

              )
            );
          });
    }

    return new Container(

      decoration: BoxDecoration(
        color: Colors.white,
        image: DecorationImage(
          colorFilter: new ColorFilter.mode(
              Colors.white.withOpacity(1.0), BlendMode.dstATop),
          image: AssetImage('images/background_main.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: RefreshIndicator(
        onRefresh: (){},
        child: ListView(
          children: <Widget>[
            GestureDetector(
              onTap: (){
                showMatchInfo();
              },
              child: new Card(
                  elevation: 5.0,
                  margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                    child: Column(
                      children: <Widget>[

                        Text("Gambeat XnO", style: TextStyle(fontWeight: FontWeight.bold),),

                        SizedBox(height: 10.0,),

                        Row(
                          mainAxisAlignment : MainAxisAlignment.spaceEvenly,
                          children: <Widget>[

                            Column(
                                children: <Widget>[

                                  CircleAvatar(

                                    backgroundImage: AssetImage('images/chris.jpg'),

                                    minRadius: 40.0,

                                  ),

                                  SizedBox(height: 5.0,),

                                  Text("Mr Bassey", style: TextStyle(fontWeight: FontWeight.bold),),

                                ]
                            ),

                            new Image.asset('images/versus.png', width: 100.0, height: 100.0,),

                            Column(
                                children: <Widget>[

                                  CircleAvatar(

                                    backgroundImage: AssetImage('images/chris.jpg'),

                                    minRadius: 40.0,

                                  ),

                                  SizedBox(height: 5.0,),

                                  Text("Nsikan", style: TextStyle(fontWeight: FontWeight.bold),),

                                ]
                            ),

                          ],
                        )
                      ],
                    ),
                  )
              ),
            )
          ],
        ),
      ),
    );
  }
}
