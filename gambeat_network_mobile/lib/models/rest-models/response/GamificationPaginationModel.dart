import 'package:Gambeat/models/Gamification.dart';


class GamificationPaginationModel{

  GamificationPaginationModel();

  int totalElements;

  int totalPage;

  bool last;

  int size;

  int number;

  String sort;

  bool first;

  int numberOfElements;

  List<Gamification> content;

  factory GamificationPaginationModel.fromJson(Map<String, dynamic> parsedJson) {

    GamificationPaginationModel gamificationPaginationModel = new GamificationPaginationModel();

    gamificationPaginationModel.totalElements = parsedJson['totalElements'];

    gamificationPaginationModel.totalPage = parsedJson['totalPages'];

    gamificationPaginationModel.last = parsedJson['last'];

    gamificationPaginationModel.size = parsedJson['size'];

    gamificationPaginationModel.number = int.tryParse(parsedJson['number']);

    gamificationPaginationModel.sort = parsedJson['sort'];

    gamificationPaginationModel.first = parsedJson['first'];

    gamificationPaginationModel.numberOfElements = parsedJson['numberOfElements'];

    var list = parsedJson['content'] as List;

    gamificationPaginationModel.content = list.map((i) => Gamification.fromJson(i)).toList();

    return gamificationPaginationModel;
  }

}