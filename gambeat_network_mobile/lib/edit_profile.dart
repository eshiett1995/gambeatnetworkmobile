import 'package:Gambeat/fragments/edit_profile/basic_info.dart';
import 'package:Gambeat/fragments/edit_profile/security_info.dart';
import 'package:flutter/material.dart';

class EditProfile extends StatefulWidget {

  @override
  _EditProfileState createState() => _EditProfileState();

}

class _EditProfileState extends State<EditProfile> with SingleTickerProviderStateMixin{


  TabController controller;

  @override
  void initState() {
    super.initState();
    controller = new TabController(vsync: this, length: 3);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {

    TabBar tab = new TabBar(
        controller: controller,
        tabs: <Tab>[
          new Tab( text: "Basic Info",),
          new Tab( text: "Security Info",),
        ]
    );

    return Scaffold(
        appBar: new PreferredSize(
        preferredSize: tab.preferredSize,
        child: new Card(
          margin : EdgeInsets.all(0.0),
        elevation: 2.0,
        color: Theme.of(context).primaryColor,
    child: Column(
      mainAxisSize:  MainAxisSize.min,
      children: <Widget>[
        SizedBox(height: 15.0,),
        tab,
      ],
    ),
    ),
        ),

        /**bottomNavigationBar: new Material(
            color: Colors.blue,
            child: tab
        ),**/
        body: new TabBarView(
            controller: controller,
            children: <Widget>[

              new BasicInfo(),

              new SecurityInfo(),
              //new third.Third()
            ]
        )
    );
  }
}
