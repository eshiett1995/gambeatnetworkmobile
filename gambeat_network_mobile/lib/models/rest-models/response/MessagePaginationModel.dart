import 'package:Gambeat/models/Message.dart';

class MessagePaginationModel{

  MessagePaginationModel();

  int totalElements;

  int totalPage;

  bool last;

  int size;

  int number;

  String sort;

  bool first;

  int numberOfElements;

  List<Message> content;

  factory MessagePaginationModel.fromJson(Map<String, dynamic> parsedJson) {

    MessagePaginationModel userPaginationModel = new MessagePaginationModel();

    userPaginationModel.totalElements = parsedJson['totalElements'];

    userPaginationModel.totalPage = parsedJson['totalPage'];

    userPaginationModel.last = parsedJson['last'];

    userPaginationModel.size = parsedJson['size'];

    userPaginationModel.number = int.tryParse(parsedJson['number']);

    userPaginationModel.sort = parsedJson['sort'];

    userPaginationModel.first = parsedJson['first'];

    userPaginationModel.numberOfElements = parsedJson['numberOfElements'];

    var list = parsedJson['content'] as List;

    userPaginationModel.content = list.map((i) => Message.fromJson(i)).toList();

    return userPaginationModel;
  }

}