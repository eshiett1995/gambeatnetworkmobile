import 'package:Gambeat/edit_profile.dart';
import 'package:Gambeat/edit_profile_picture.dart';
import 'package:flutter/material.dart';

class SettingsFragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: ListView(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(15.0),
                      child: Text("Main"),
                    ),
                    Container(
                      height: 1.0,
                      color: Colors.black12,
                    ),
                    Container(
                      color: Colors.white,
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[

                            GestureDetector(
                              onTap: (){

                                var route = new MaterialPageRoute(

                                  builder: (BuildContext context) => new EditProfile(),

                                );

                                Navigator.of(context).push(route);
                              },
                              child: Container(
                                margin: EdgeInsets.only(bottom: 15.0, top: 15.0),
                                child: Row(
                                  children: <Widget>[
                                    Icon(Icons.person),
                                    SizedBox(
                                      width: 10.0,
                                    ),
                                    Text("My Profile Settings"),
                                    Spacer(),
                                    Icon(
                                      Icons.keyboard_arrow_right,
                                      color: Colors.black45,
                                    )
                                  ],
                                ),
                              ),
                            ),

                            Container(
                              height: 1.0,
                              color: Colors.black12,
                            ),
                            GestureDetector(
                              onTap: (){
                                var route = new MaterialPageRoute(
                                  builder: (BuildContext context) => new EditProfilePicture(),
                                );

                                Navigator.of(context).push(route);
                              },
                              child: Container(
                                margin: EdgeInsets.only(bottom: 15.0, top: 15.0),
                                child: Row(
                                  children: <Widget>[
                                    Icon(Icons.camera),
                                    SizedBox(
                                      width: 10.0,
                                    ),
                                    Text("Edit Profile Picture"),
                                    Spacer(),
                                    Icon(
                                      Icons.keyboard_arrow_right,
                                      color: Colors.black45,
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              height: 1.0,
                              color: Colors.black12,
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 15.0, top: 15.0),
                              child: Row(
                                children: <Widget>[
                                  Text("Push Notifications"),
                                  Switch(
                                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                    activeColor: Colors.blue,
                                    value: true,
                                    onChanged: null,
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 1.0,
                      color: Colors.black12,
                    ),
                    Padding(
                      padding: EdgeInsets.all(15.0),
                      child: Text("Support"),
                    ),
                    Container(
                      height: 1.0,
                      color: Colors.black12,
                    ),
                    Container(
                      color: Colors.white,
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(bottom: 15.0, top: 15.0),
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.message),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  Text("Report A Problem"),
                                  Spacer(),
                                  Icon(
                                    Icons.keyboard_arrow_right,
                                    color: Colors.black45,
                                  )
                                ],
                              ),
                            ),
                            Container(
                              height: 1.0,
                              color: Colors.black12,
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 15.0, top: 15.0),
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.sort),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  Text("Privacy Policy"),
                                  Spacer(),
                                  Icon(
                                    Icons.keyboard_arrow_right,
                                    color: Colors.black45,
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 1.0,
                      color: Colors.black12,
                    ),
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
