

import 'package:Gambeat/models/DefaultEntity.dart';
import 'package:Gambeat/models/Game.dart';
import 'package:Gambeat/models/User.dart';

class Gamification  extends DefaultEntity{

  Gamification();

  User contestantOne;

  User contestantTwo;

  bool isRead;

  Game game;

  String gamificationMessage;

  factory Gamification.fromJson(Map<String, dynamic> parsedJson) {
    Gamification gamification = new Gamification();

    gamification.id = parsedJson['_id'];

    gamification.dateCreated = parsedJson['dateCreated'];

    gamification.contestantOne = User.fromJson(parsedJson['contestantOne']);

    gamification.contestantTwo = User.fromJson(parsedJson['contestantTwo']);

    gamification.isRead = parsedJson['isRead'];

    gamification.game = Game.fromJson(parsedJson['messageStatus']);

    gamification.gamificationMessage = parsedJson['gamificationMessage'];

    return gamification;
  }


  Map<String, dynamic> toJson() => {

    '"_id"': '"$id"',

    '"dateCreated"': dateCreated,

    '"contestantOne"': contestantOne == null ? null : contestantOne.toJson(),

    '"contestantTwo"': contestantTwo == null ? null : contestantTwo.toJson(),

    '"isRead"': isRead,

    '"game"': game == null ? null : game.toJson(),

    '"gamificationMessage"': '"$gamificationMessage',
  };

}