import 'package:Gambeat/models/DefaultEntity.dart';
import 'package:Gambeat/models/Enum.dart';
import 'package:Gambeat/models/User.dart';

class FriendRequest extends DefaultEntity{

  FriendRequest();

  String iid;

  User sender;

  FriendRequestStatus friendRequestStatus;

  User recipient;

  factory FriendRequest.fromJson(Map<String, dynamic> parsedJson) {

    FriendRequest friendRequest = new FriendRequest();

    friendRequest.id = parsedJson['id'];

    friendRequest.iid = parsedJson['_id'];

    //friendRequest.dateCreated = DateTime.fromMicrosecondsSinceEpoch(parsedJson['dateCreated']);

    friendRequest.sender = User.fromJson(parsedJson['sender']);

    friendRequest.friendRequestStatus = getFriendRequestStatus(parsedJson['friendRequestStatus']);

    friendRequest.recipient = User.fromJson(parsedJson['recipient']);

    return friendRequest;

  }

  ///Returns a FriendRequestStatus corresponding to the String value gotten from the Api
  static FriendRequestStatus getFriendRequestStatus(String friendRequestStat) {
    switch (friendRequestStat) {
      case "ACCEPTED":
        return FriendRequestStatus.ACCEPTED;
      case "PENDING":
        return FriendRequestStatus.PENDING;
      case "DECLINED":
        return FriendRequestStatus.DECLINED;
      default:
        return FriendRequestStatus.PENDING;
    }
  }

}
