import 'package:Gambeat/models/rest-models/response/ResponseModel.dart';

class ProfileUpdateModel {

  ProfileUpdateModel();

  String temporaryStoreId;

  ResponseModel responseModel;

  factory ProfileUpdateModel.fromJson(Map<String, dynamic> parsedJson) {
    ProfileUpdateModel profileUpdateModel = new ProfileUpdateModel();

    profileUpdateModel.temporaryStoreId = parsedJson['temporaryStoreId'];

    profileUpdateModel.responseModel = ResponseModel.fromJson(parsedJson['responseModel']);

    return profileUpdateModel;
  }



}
