import 'dart:convert';
import 'dart:io';

import 'package:Gambeat/Callbacks.dart';
import 'package:Gambeat/custom-widgets/custom_dialog_box.dart';
import 'package:Gambeat/custom-widgets/custom_radio_button_for_gender.dart';
import 'package:Gambeat/fragments/activities/competitors.dart';
import 'package:Gambeat/fragments/activities/stages.dart';
import 'package:Gambeat/helper/SharedPreferenceHelper.dart';
import 'package:Gambeat/models/Competition.dart';
import 'package:Gambeat/models/rest-models/request/EnterCompetitionModel.dart';
import 'package:Gambeat/models/rest-models/request/GameSearchModel.dart';
import 'package:Gambeat/models/rest-models/response/CompetitionPageModel.dart';
import 'package:Gambeat/network/api/ApiHelper.dart';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/material.dart';
import 'package:autocomplete_textfield/autocomplete_textfield.dart'
    as autoCompleteTextField;
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

class CompetitionFragment extends StatefulWidget {
  var homePageState;

  CompetitionFragment({this.homePageState});

  @override
  _CompetitionsState createState() => _CompetitionsState();
}

class _CompetitionsState extends State<CompetitionFragment> {


  List<Competition> competitions = new List();
  String optionMenuChoice = "";
  ApiHelper apiHelper = new ApiHelper();
  int currentPageNumber = 0;
  bool isLastPage = false;
  int endOfPageCalled;
  int currentLength = 0;
  bool _isInitialListLoading = false;
  int totalNumberOfPages = 0;

  bool _isSubsequentListLoading = false;

  @override
  void initState() {
    widget.homePageState.initCompetitionDropDownMenuCallback((button) {

      if (button == CompetionDropDownMenuSearchValue) {

        _performSearchAction();

      } else if (button == CompetionDropDownMenuCreateValue) {

        _performCreatingCompetitionAction();

      }

    });

    _getCompetitionFromServer();

  }

  ///-------------------------------------------------------------------------
  ///This method sets state setting _initialListLoading bool var to false
  ///so as to control the progressbar indicating initial list items are being
  ///fetched from the api
  ///-------------------------------------------------------------------------
  _setInitialListLoadingFalse() {
    setState(() {
      _isInitialListLoading = false;
    });
  }

  ///This method sets state setting _initialListLoading bool var to true
  ///so as to control the progressbar indicating initial list items are being
  ///fetched from the api
  _setInitialListLoadingTrue() {
    setState(() {
      _isInitialListLoading = true;
    });
  }

  void _setSubsequentListLoadingTrue() {
    setState(() {
      _isSubsequentListLoading = true;
    });
  }

  void _setSubsequentListLoadingFalse() {
    setState(() {
      _isSubsequentListLoading = false;
    });
  }

  Widget _getBottomProgressBar() {
    return new LinearProgressIndicator();
  }

  void _getCompetitionFromServer() async {

    _setInitialListLoadingTrue();

    ApiHelper apiHelper = new ApiHelper();

    HttpClientResponse c = await apiHelper.getAllCompetitions(0);

    //var mm = json.decode(response.body);

    String mm = await c.transform(utf8.decoder).join();
    print(mm);

    var jsonString = json.decode(mm);

    if (c.statusCode == 200) {
      print("\n*********************************************");
      print("StatusCode == 200\n");
      _setInitialListLoadingFalse();

      CompetitionPageModel competitionPageModel =
          CompetitionPageModel.fromJson(jsonString);
      totalNumberOfPages = competitionPageModel.totalPage;
      setState(() {
        competitionPageModel.content.forEach((competition) {
          competitions.add(competition);
        });
      });
    } else {
      print("\n*********************************************");
      print("StatusCode != 200\n");
      _setInitialListLoadingFalse();
    }
  }

  ///When the create a competition option is clicked this method is called
  _performCreatingCompetitionAction() {
    showDialog(
        context: widget.homePageState.context,
        builder: (BuildContext context) {
          return _getCreateACompetitionActionDialogBox();
        });
  }

  ///When the search option is clicked this method is called
  _performSearchAction() {
    showDialog(
        context: widget.homePageState.context,
        builder: (BuildContext context) {
          return _getSearchActionDialogBox();
        });
  }

  GlobalKey<AutoCompleteTextFieldState<String>> key = new GlobalKey();
  TextEditingController controller = new TextEditingController();
  AutoCompleteTextField searchTextField;
  String searchCompetitionAmountChoice = "Non";
  String createCompetitionAmountChoice = "Non";

  var color;

  TextEditingController searchCompetitionTextController =
      new TextEditingController();

  /// Returns a dialog box so as to be used when search action is clicked
  _getSearchActionDialogBox() {
    searchTextField = new autoCompleteTextField.AutoCompleteTextField<String>(
      style: new TextStyle(color: Colors.black, fontSize: 20.0),
      decoration: new InputDecoration(
          fillColor: Colors.white,
          focusedBorder: new OutlineInputBorder(
              borderSide: BorderSide(color: Colors.blue, width: 2.0),
              borderRadius: BorderRadius.circular(15.0)),
          border: new OutlineInputBorder(
              borderSide: BorderSide(color: Colors.blue, width: 2.0),
              borderRadius: BorderRadius.circular(15.0)),
          enabledBorder: new OutlineInputBorder(
              borderSide: BorderSide(color: Colors.blue, width: 2.0),
              borderRadius: BorderRadius.circular(15.0)),
          suffixIcon: new Icon(Icons.search),
          contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
          filled: true,
          hintText: 'Search Competition Name',
          hintStyle: TextStyle(color: Colors.grey)),
      suggestions: <String>["Hey", "Me", "WAAAT"],
      itemBuilder: (BuildContext context, String suggestion) {
        return new Container(
          child: Text(
            (suggestion),
            style: new TextStyle(color: Colors.black),
          ),
          color: Colors.white70,
        );
      },
      itemFilter: (String suggestion, String query) {
        print(
            "itemFilter ------- suggestion : $suggestion -- query: $query --- ");
        suggestion.toLowerCase().startsWith(query.toLowerCase());
        print("Does Item match wit any???? ---- " +
            suggestion
                .toLowerCase()
                .startsWith(query.toLowerCase())
                .toString());
      },
      itemSorter: (String a, String b) {
        print("itemSorter ------- a : $a -- b: $b --- ");
        return a.compareTo(b);
      },
      itemSubmitted: (String data) {
        setState(() => searchTextField.textField.controller.text = data);
      },
      key: key,
    );
    return new CustomAlertDialog(
        title: new Container(
            height: 40.0,
            decoration: new BoxDecoration(
                color: Colors.blue,
                borderRadius: BorderRadius.only(
                    topLeft: new Radius.circular(23.0),
                    topRight: new Radius.circular(23.0))),
            child: new Center(
                child: new Text("Search a Competition",
                    style: new TextStyle(color: Colors.white)))),
        content: new Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(24.0)),
//            padding: EdgeInsets.only(bottom: 32.0),
            height: 250.0,
            width: 280.0,
            child:
                new Column(mainAxisSize: MainAxisSize.max, children: <Widget>[
              new Align(
                alignment: Alignment.topCenter,
                child: new Container(
                    margin: EdgeInsets.only(
                        top: 6.0, bottom: 6.0, left: 10.0, right: 10.0),
                    height: 60.0,
                    child: TypeAheadField(
                      suggestionsBoxDecoration: new SuggestionsBoxDecoration(),
                      textFieldConfiguration: TextFieldConfiguration(
                          controller: searchCompetitionTextController,
                          autofocus: true,
                          style: DefaultTextStyle.of(context)
                              .style
                              .copyWith(fontStyle: FontStyle.italic),
                          decoration: InputDecoration(
                              hintText: "Create a Competition",
                              hintStyle: new TextStyle(fontSize: 12.0),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(24.0)))),
                      suggestionsCallback: (pattern) {
                        return [
                          // "Hello",
                          // "Waat",
                          // "HHaa",
                          // "Stressed",
                          // "Taaa",
                          // "Fool"
                        ];
                      },
                      itemBuilder: (context, suggestion) {
                        return ListTile(
                          leading: Icon(Icons.shopping_cart),
                          title: Text(suggestion),
                        );
                      },
                      onSuggestionSelected: (suggestion) {
                        searchCompetitionTextController.text = suggestion;
                      },
//                    ),
                    )),
              ),
              new Align(
                alignment: Alignment.center,
                child: new CustomRadioButton(
                  genderSelectionCallback: (choice) {
                    this.searchCompetitionAmountChoice = choice;
                  },
                  radioVal: "Non",
                  values: <String>["₦ 500", "₦ 1000"],
                ),
              ),
              new Container(
                margin: EdgeInsets.only(top: 8.0),
                alignment: Alignment.bottomCenter,
                child: new RaisedButton(
                  padding: EdgeInsets.only(
                      left: 32.0, right: 32.0, top: 8.0, bottom: 8.0),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(24.0)),
                  elevation: 10.0,
                  child: new Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Container(
                            child: new Icon(Icons.search, color: Colors.white),
                            margin: EdgeInsets.only(right: 16.0)),
                        new Text(
                          "SUBMIT",
                          style: new TextStyle(
                              color: Colors.white, fontSize: 24.0),
                        )
                      ]),
                  onPressed: () {},
                  splashColor: Colors.orangeAccent,
                  color: Colors.blueAccent,
                ),
              ),
            ])));
  }

  TextEditingController createCompetitionTextController =
      new TextEditingController();

  /// Returns a dialog box so as to be used when create a competition action is clicked
  _getCreateACompetitionActionDialogBox() {
    return new CustomAlertDialog(
        title: new Container(
            height: 40.0,
            decoration: new BoxDecoration(
                color: Colors.blue,
                borderRadius: BorderRadius.only(
                    topLeft: new Radius.circular(23.0),
                    topRight: new Radius.circular(23.0))),
            child: new Center(
                child: new Text("Create a Competition",
                    style: new TextStyle(color: Colors.white)))),
        content: new Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(24.0)),
//            padding: EdgeInsets.only(bottom: 32.0),
            height: 250.0,
            width: 280.0,
            child:
                new Column(mainAxisSize: MainAxisSize.max, children: <Widget>[
              new Align(
                alignment: Alignment.topCenter,
                child: new Container(
                    margin: EdgeInsets.only(
                        top: 6.0, bottom: 6.0, left: 10.0, right: 10.0),
                    height: 60.0,
                    child: TypeAheadField(
                      suggestionsBoxDecoration: new SuggestionsBoxDecoration(),
                      textFieldConfiguration: TextFieldConfiguration(
                          controller: createCompetitionTextController,
                          autofocus: true,
                          style: DefaultTextStyle.of(context)
                              .style
                              .copyWith(fontStyle: FontStyle.italic),
                          decoration: InputDecoration(
                              hintText: "Create a Competition",
                              hintStyle: new TextStyle(fontSize: 12.0),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(24.0)))),
                      suggestionsCallback: (pattern) {
                        return [
                          // "Hello",
                          // "Waat",
                          // "HHaa",
                          // "Stressed",
                          // "Taaa",
                          "Fool"
                        ];
                      },
                      itemBuilder: (context, suggestion) {
                        return ListTile(
                          leading: Icon(Icons.shopping_cart),
                          title: Text(suggestion),
                        );
                      },
                      onSuggestionSelected: (suggestion) {
                        createCompetitionTextController.text = suggestion;
                      },
//                    ),
                    )),
              ),
              new Align(
                alignment: Alignment.center,
                child: new CustomRadioButton(
                  genderSelectionCallback: (choice) {
                    this.createCompetitionAmountChoice = choice;
                  },
                  radioVal: "Non",
                  values: <String>["₦ 500", "₦ 1000"],
                ),
              ),
              new Container(
                margin: EdgeInsets.only(top: 8.0),
                alignment: Alignment.bottomCenter,
                child: new RaisedButton(
                  padding: EdgeInsets.only(
                      left: 32.0, right: 32.0, top: 8.0, bottom: 8.0),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(24.0)),
                  elevation: 10.0,
                  child: new Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Container(
                            child: new Icon(Icons.search, color: Colors.white),
                            margin: EdgeInsets.only(right: 16.0)),
                        new Text(
                          "SUBMIT",
                          style: new TextStyle(
                              color: Colors.white, fontSize: 24.0),
                        )
                      ]),
                  onPressed: () {
                    EnterCompetitionModel competitionModel =
                        new EnterCompetitionModel();

                    competitionModel.competition = _getCompetition(true);

                    apiHelper.createCompetition(competitionModel);
                  },
                  splashColor: Colors.orangeAccent,
                  color: Colors.blueAccent,
                ),
              ),
            ])));
  }

  ///Get and returns an amount in int dependening on the string amount passed
  _getAmountFromRadioButtons(String amount) {
    switch (amount) {
      case "₦ 500":
        // assert amount == "₦ 500";
        return 500.00;
      case "₦ 1000":
        return 1000.00;
      default:
        return 0.0;
    }
  }

  ///Returns a Competition type
  _getCompetition(bool isCreateCompetition) {

    Competition competition = new Competition();

    if (isCreateCompetition) {

      competition.competitionName = createCompetitionTextController.text;
      competition.entryFee = _getAmountFromRadioButtons(createCompetitionAmountChoice);
      competition.dateCreated = DateTime.now().millisecondsSinceEpoch;

    } else {

    }
    return competition;
  }

  void competitionOptionsMenu(Competition competition) {
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return new Container(
            color: Colors.white,
            child: new Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(
                  height: 20.0,
                ),
                Text("SELECT AN OPTION:"),
                ListTile(
                  leading: Icon(Icons.account_circle),
                  title: Text("View Competitors"),
                  onTap: () {
                    var route = new MaterialPageRoute(
                      builder: (BuildContext context) => new Competitors(),
                    );

                    Navigator.of(context).push(route);
                  },
                ),
                ListTile(
                  leading: Icon(Icons.message),
                  title: Text("View Stages"),
                  onTap: () {
                    var route = new MaterialPageRoute(
                      builder: (BuildContext context) => new Stages(),
                    );

                    Navigator.of(context).push(route);
                  },
                ),
                ListTile(
                  leading: Icon(Icons.reply),
                  title: Text("View Game"),
                ),
                ListTile(
                  leading: Icon(Icons.message),
                  title: Text("Join Competition"),
                  onTap: () async {
                    EnterCompetitionModel competitionModel =
                        new EnterCompetitionModel();
                    competitionModel.user =
                        await SharedPreferenceHelper().getUser();
                    competitionModel.competition = competition;

                    apiHelper.joinCompetition(competitionModel);
                    showDialog(
                      context: widget.homePageState.context,
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text(''),
                          content: SingleChildScrollView(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                new Expanded(
                                  flex: 2,
                                  child: Text(
                                      "Just incase you dont get a slot in this competition, can we add you to the next upcoming ones ?"),
                                ),
                                new Expanded(
                                    flex: 1,
                                    child: Switch(
                                      value: continueIfCompetitionEntryClosed,
                                      onChanged: (val) {
                                        setState(() {
                                          print("Val oooh ---- " +
                                              val.toString());
                                          continueIfCompetitionEntryClosed =
                                              val;
                                        });
                                      },
                                      activeTrackColor: Colors.lightGreenAccent,
                                      activeColor: Colors.green,
                                    ))
                              ],
                            ),
                          ),
                          actions: <Widget>[
                            FlatButton(
                              child: Text('Submit'),
                              onPressed: () async {
                                EnterCompetitionModel competitionModel =
                                    new EnterCompetitionModel();
                                competitionModel.user =
                                    await SharedPreferenceHelper().getUser();
                                competitionModel.competition = competition;
                                competitionModel
                                    .continueIfCompetitionEntryClosed = true;

                                apiHelper.joinCompetition(competitionModel);
                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        );
                      },
                    );
                  },
                )
              ],
            ),
          );
        });
  }

  ///Returns a list builder to populate the list gotten from the api
  _getListWidget() {
    return new ListView.builder(
      itemCount: competitions.length,
      itemBuilder: (BuildContext context, int index) {
        Competition competition = competitions[index];
        return new GestureDetector(
          onTap: () {
            competitionOptionsMenu(null);
          },
          child: new Card(
              color: Colors.blueGrey,
              child: Container(
                  margin: EdgeInsets.all(10.0),
                  padding: EdgeInsets.only(left: 5.0, right: 5.0, top: 5.0),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                competition.game.name,
                                style: TextStyle(
                                    color: Colors.white,
                                    height: 1.5,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20.0),
                              ),
                              Text(
                                competition.competitionName,
                                style: TextStyle(
                                    color: Colors.white,
                                    height: 1.5,
                                    fontSize: 12.0),
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Image.asset(
                                'images/trophy.png',
                                height: 70.9,
                              ),
                            ],
                          )
                        ],
                      ),
                      Divider(
                        color: Colors.white,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.people,
                                    color: Colors.white,
                                  ),
                                  SizedBox(
                                    width: 5.0,
                                  ),
                                  RichText(
                                    text: new TextSpan(
                                      // Note: Styles for TextSpans must be explicitly defined.
                                      // Child text spans will inherit styles from parent
                                      style: new TextStyle(
                                          color: Colors.white, height: 1.5),

                                      children: <TextSpan>[
                                        new TextSpan(
                                            text: '77',
                                            style: new TextStyle(
                                                fontWeight: FontWeight.bold)),
                                        new TextSpan(
                                            text: '/',
                                            style: new TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 10.0)),
                                        new TextSpan(
                                            text: '100',
                                            style: new TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 10.0)),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                color: Colors.white,
                                width: 20.0,
                                height: 1.0,
                              )
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.credit_card,
                                    color: Colors.white,
                                  ),
                                  SizedBox(
                                    width: 5.0,
                                  ),
                                  new Text('100,000',
                                      style:
                                          new TextStyle(color: Colors.white)),
                                ],
                              ),
                              Container(
                                color: Colors.white,
                                width: 20.0,
                                height: 1.0,
                              )
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.compare_arrows,
                                    color: Colors.white,
                                  ),
                                  SizedBox(
                                    width: 5.0,
                                  ),
                                  new Text('Open',
                                      style:
                                          new TextStyle(color: Colors.white)),
                                ],
                              ),
                              Container(
                                color: Colors.white,
                                width: 20.0,
                                height: 1.0,
                              )
                            ],
                          )
                        ],
                      )
                    ],
                  ))),
        );
      },
    );
  }

  ///For lazy loading list
  void _lazyLoading() {
    endOfPageCalled = endOfPageCalled + 1;
    print("===========================================================");
    print("LazyMethod Called!! --- end of Page");
    print("===========================================================");
    if (!(endOfPageCalled > 1)) //This is so to prevent multiple calling
    if (!(currentPageNumber >= totalNumberOfPages)) {
      print(
          "currentPageNumber NOT greater than or equal to totalNumberOfPages -- ");
      currentPageNumber = currentPageNumber + 1;
      _loadMoreCompetitionsFromServer(pageNum: currentPageNumber);
    }
  }

  bool continueIfCompetitionEntryClosed = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        image: DecorationImage(

          image: AssetImage('images/background_main.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: new Column(
        children: <Widget>[
          new Expanded(
            child: _isInitialListLoading
                ? Container(
//                color:Colors.blueAccent,
                    height: MediaQuery.of(context).size.height,

                    child: Center(child: CircularProgressIndicator()),
                  )
                : LazyLoadScrollView(
                    child: _getListWidget(),
                    onEndOfPage: _lazyLoading,
                  ),
          ),
          _isSubsequentListLoading ? _getBottomProgressBar() : Center()
        ],
      ),
    );
  }

  ///Called when lazy loading is required
  void _loadMoreCompetitionsFromServer(
      {int pageNum, bool participant: false}) async {
    _setSubsequentListLoadingTrue();
    print("got ete");

    ApiHelper apiHelper = new ApiHelper();

    HttpClientResponse c = await apiHelper.getAllGames(pageNum, new GameSearchModel());

    //var mm = json.decode(response.body);

    String mm = await c.transform(utf8.decoder).join();

    var jsonString = json.decode(mm);

    if (c.statusCode == 200) {
      CompetitionPageModel competitionPageModel =
          CompetitionPageModel.fromJson(jsonString);

      setState(() {
        endOfPageCalled = 0;
        competitionPageModel.content.forEach((competition) {
          competitions.add(competition);
        });
      });
      _setSubsequentListLoadingFalse();
    } else {
      //TODO: Display a toast or snackbar that getting the friends from server failed
      _setSubsequentListLoadingFalse();
      Scaffold.of(context).showSnackBar(new SnackBar(
        content: new Text("Error in getting competition"),
      ));
    }
  }
}
