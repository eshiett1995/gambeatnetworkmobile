

import 'package:Gambeat/models/DefaultEntity.dart';
import 'package:Gambeat/models/Enum.dart';
import 'package:Gambeat/models/Game.dart';
import 'package:Gambeat/models/Punter.dart';

class Competition extends DefaultEntity {

  Competition();

  String competitionName;

  Game game;

  List<Punter> punters = new List();

  List<Punter> winners = new List();

  double winningAmount;

  CompetitionStatus competitionStatus;

  double entryFee;

  factory Competition.fromJson(Map<String, dynamic> parsedJson) {
    Competition competition = new Competition();

    competition.id = parsedJson['_id'];

    competition.dateCreated = parsedJson['dateCreated'];

    competition.competitionName = parsedJson['competitionName'];

    competition.game =  Game.fromJson(parsedJson['game']);

    competition.punters = jsonToListPunters(parsedJson);

    competition.winners = jsonToListWinners(parsedJson);

    competition.winningAmount = parsedJson['winningAmount'];

    competition.competitionStatus = getCompetitionStatus(parsedJson['competitionStatus']);

    competition.entryFee = parsedJson['entryFee'];

    return competition;
  }

  ///Returns a MessageStatus corresponding to the String value gotten from the Api
  static CompetitionStatus getCompetitionStatus(String competitionStatus) {
    switch (competitionStatus) {
      case "STARTED":
        return CompetitionStatus.STARTED;
      case "OPEN":
        return CompetitionStatus.OPEN;
      case "ENDED":
        return CompetitionStatus.ENDED;
      default:
        return CompetitionStatus.OPEN;
    }
  }


  static List<Punter> jsonToListPunters(Map<String, dynamic> parsedJson){

    var list = parsedJson['punters'] as List;

    print(list.runtimeType);

    List<Punter> punterList = list.map((i) => Punter.fromJson(i)).toList();

    return punterList;
  }


  static List<Punter> jsonToListWinners(Map<String, dynamic> parsedJson){

    var list = parsedJson['winners'] as List;

    print(list.runtimeType);

    List<Punter> punterList = list.map((i) => Punter.fromJson(i)).toList();

    return punterList;
  }

  Map<String, dynamic> toJson() => {

    '"_id"': '"$id"',

    '"dateCreated"': dateCreated,

    '"competitionName"': '"$competitionName"',

    '"game"': game == null ? null : game.toJson(),

    '"punters"': punters.toString(),

    '"winners"': winners.toString(),

    '"winningAmount"': '"$winningAmount"',

    '"competitionStatus"': '"$competitionStatus"',

    '"entryFee"': '"$entryFee"',

  };

}