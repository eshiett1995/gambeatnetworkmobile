import 'package:Gambeat/models/Competition.dart';
import 'package:Gambeat/models/User.dart';

class EnterCompetitionModel {

  EnterCompetitionModel();

  Competition competition;

  User user;

  bool continueIfCompetitionEntryClosed;

  factory EnterCompetitionModel.fromJson(Map<String, dynamic> parsedJson) {
    EnterCompetitionModel enterCompetitionModel = new EnterCompetitionModel();

    enterCompetitionModel.competition = Competition.fromJson(parsedJson['competition']);

    enterCompetitionModel.user = User.fromJson(parsedJson['user']);

    enterCompetitionModel.continueIfCompetitionEntryClosed = parsedJson['continueIfCompetitionEntryClosed'];

    return enterCompetitionModel;
  }





  Map<String, dynamic> toJson() => {

    '"competition"': competition == null ? null : competition.toJson(),

    '"user"': user == null ? null : user.toJson(),

    '"continueIfCompetitionEntryClosed"': '"$continueIfCompetitionEntryClosed"',

  };


}
