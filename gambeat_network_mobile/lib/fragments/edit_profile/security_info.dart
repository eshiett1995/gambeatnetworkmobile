import 'dart:convert';
import 'dart:io';

import 'package:Gambeat/models/rest-models/response/ResponseModel.dart';
import 'package:country_pickers/country.dart';
import 'package:country_pickers/utils/utils.dart';
import 'package:Gambeat/custom-widgets/custom_radio_butto_for_message_sender.dart';
import 'package:Gambeat/helper/SharedPreferenceHelper.dart';
import 'package:Gambeat/models/Enum.dart';
import 'package:Gambeat/models/User.dart';
import 'package:Gambeat/models/rest-models/request/TemporaryStore.dart';
import 'package:Gambeat/models/rest-models/response/ProfileUpdateModel.dart';
import 'package:Gambeat/network/api/ApiHelper.dart';
import 'package:flutter/material.dart';
import 'package:country_pickers/countries.dart';
import 'package:flutter_sweet_alert/flutter_sweet_alert.dart';

class SecurityInfo extends StatefulWidget {
  @override
  _SecurityInfoState createState() => _SecurityInfoState();
}

class _SecurityInfoState extends State<SecurityInfo> {

  bool isVerifyingChange = false;

  bool isUpdatingEmail = false;

  bool isUpdatingPhoneNumber = false;

  bool isUpdatingUserName = false;

  bool isUpdatingPassword  = false;


  User user;

  TextEditingController tokenController = TextEditingController();

  TextEditingController emailController = TextEditingController();

  TextEditingController phoneNumberController = TextEditingController();

  TextEditingController usernameController = TextEditingController();

  TextEditingController passwordController = TextEditingController();

  TextEditingController newPasswordController = TextEditingController();

  TextEditingController confirmPasswordController = TextEditingController();

  String phoneCode = "+";


  void loadUsersDetails() async {



    user = await new SharedPreferenceHelper().getUser();

    emailController.text = user.email;

    phoneNumberController.text = user.phoneNumber;

    usernameController.text = user.userName;

    final _countries = countriesList.map((item) => Country.fromMap(item)).toList();

    var country =_countries.firstWhere((country) => country.name.startsWith(user.country), orElse: () => null);

    phoneCode += country.phoneCode + " ";


    setState(() {

    });
  }

  void showAlertDialog(AlertType alertType, String title, String content) {
    SweetAlert.dialog(
        type: alertType,
        title: title,
        content: content,
        cancelButtonText: "Okay",
        confirmButtonText: "ok");
  }


  /*-----------------------------------------------------------
   ---------------------BUILD BUTTON METHODS--------------------
   ------------------------------------------------------------*/

  Widget _buildConfirmTokenButton(TemporaryStore tempStore){

    if(!isVerifyingChange){

      return new RaisedButton(
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(5.0),
        ),
        color: Colors.blue,
        onPressed: () {
          verifyChangeWithToken(tempStore);
        },
        child: new Container(
          padding: const EdgeInsets.symmetric(
            vertical: 10.0,
            horizontal: 10.0,
          ),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[

              Text(
                "VERIFY CHANGE",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),

              SizedBox(width: 15.0,),

              Icon(Icons.update, color: Colors.white,),
            ],
          ),
        ),
      );

    }else{

      return Container(
        height: 48.0,
        width: 48.0,
        child: RaisedButton(
          padding: EdgeInsets.all(0.0),
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0),
          ),
          child: SizedBox(
              height: 30.0,
              width: 30.0,
              child: Stack(
                children: <Widget>[
                  CircularProgressIndicator(
                    value: null,
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                  ),
                  Positioned(
                    child: Icon(Icons.update, size: 18.0, color: Colors.white,),
                    left: 0.0,
                    right: 0.0,
                    top: 0.0,
                    bottom: 0.0,
                  )
                ],
              )
          ),
          disabledColor: Colors.blue,
          onPressed: null,
        ),
      );

    }
  }

  Widget _buildUpdateEmailButton(){

    if(!isUpdatingEmail){

      return  Expanded(child: new RaisedButton(
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(5.0),
        ),
        color: Colors.blue,
        onPressed: () {
          updateEmail();
        },
        child: new Container(
          padding: const EdgeInsets.symmetric(
            vertical: 12.0,
            horizontal: 10.0,
          ),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[

              Text(
                "UPDATE EMAIL",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),

              SizedBox(width: 15.0,),

              Icon(Icons.update, color: Colors.white,),
            ],
          ),
        ),
      ));

    }else{

      return Container(
        height: 48.0,
        width: 48.0,
        child: RaisedButton(
          padding: EdgeInsets.all(0.0),
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0),
          ),
          child: SizedBox(
              height: 30.0,
              width: 30.0,
              child: Stack(
                children: <Widget>[
                  CircularProgressIndicator(
                    value: null,
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                  ),
                  Positioned(
                    child: Icon(Icons.update, size: 18.0, color: Colors.white,),
                    left: 0.0,
                    right: 0.0,
                    top: 0.0,
                    bottom: 0.0,
                  )
                ],
              )
          ),
          disabledColor: Colors.blue,
          onPressed: null,
        ),
      );

    }
  }

  Widget _buildUpdatePhoneNumberButton(){

    if(!isUpdatingPhoneNumber){

      return  Expanded(child: new RaisedButton(
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(5.0),
        ),
        color: Colors.blue,
        onPressed: () {
          updatePhoneNumber();
        },
        child: new Container(
          padding: const EdgeInsets.symmetric(
            vertical: 12.0,
            horizontal: 10.0,
          ),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[

              Text(
                "UPDATE PHONENUMBER",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),

              SizedBox(width: 15.0,),

              Icon(Icons.update, color: Colors.white,),
            ],
          ),
        ),
      ));

    }else{

      return Container(
        height: 48.0,
        width: 48.0,
        child: RaisedButton(
          padding: EdgeInsets.all(0.0),
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0),
          ),
          child: SizedBox(
              height: 30.0,
              width: 30.0,
              child: Stack(
                children: <Widget>[
                  CircularProgressIndicator(
                    value: null,
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                  ),
                  Positioned(
                    child: Icon(Icons.update, size: 18.0, color: Colors.white,),
                    left: 0.0,
                    right: 0.0,
                    top: 0.0,
                    bottom: 0.0,
                  )
                ],
              )
          ),
          disabledColor: Colors.blue,
          onPressed: null,
        ),
      );

    }
  }

  Widget _buildUserNameButton(){

    if(!isUpdatingUserName){

      return  Expanded(child: new RaisedButton(
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(5.0),
        ),
        color: Colors.blue,
        onPressed: () {
          updateUserName();
        },
        child: new Container(
          padding: const EdgeInsets.symmetric(
            vertical: 12.0,
            horizontal: 10.0,
          ),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[

              Text(
                "UPDATE USERNAME",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),

              SizedBox(width: 15.0,),

              Icon(Icons.update, color: Colors.white,),
            ],
          ),
        ),
      ));

    }else{

      return Container(
        height: 48.0,
        width: 48.0,
        child: RaisedButton(
          padding: EdgeInsets.all(0.0),
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0),
          ),
          child: SizedBox(
              height: 30.0,
              width: 30.0,
              child: Stack(
                children: <Widget>[
                  CircularProgressIndicator(
                    value: null,
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                  ),
                  Positioned(
                    child: Icon(Icons.update, size: 18.0, color: Colors.white,),
                    left: 0.0,
                    right: 0.0,
                    top: 0.0,
                    bottom: 0.0,
                  )
                ],
              )
          ),
          disabledColor: Colors.blue,
          onPressed: null,
        ),
      );

    }
  }

  Widget _buildPasswordButton(){

    if(!isUpdatingPassword){

      return  Expanded(child: new RaisedButton(
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(5.0),
        ),
        color: Colors.blue,
        onPressed: () {
          updatePassword();
        },
        child: new Container(
          padding: const EdgeInsets.symmetric(
            vertical: 12.0,
            horizontal: 10.0,
          ),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[

              Text(
                "UPDATE PASSWORD",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),

              SizedBox(width: 15.0,),

              Icon(Icons.update, color: Colors.white,),
            ],
          ),
        ),
      ));

    }else{

      return Container(
        height: 48.0,
        width: 48.0,
        child: RaisedButton(
          padding: EdgeInsets.all(0.0),
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0),
          ),
          child: SizedBox(
              height: 30.0,
              width: 30.0,
              child: Stack(
                children: <Widget>[
                  CircularProgressIndicator(
                    value: null,
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                  ),
                  Positioned(
                    child: Icon(Icons.update, size: 18.0, color: Colors.white,),
                    left: 0.0,
                    right: 0.0,
                    top: 0.0,
                    bottom: 0.0,
                  )
                ],
              )
          ),
          disabledColor: Colors.blue,
          onPressed: null,
        ),
      );

    }
  }



  /*-----------------------------------------------------------
   -----------------------UPDATE METHODS------------------------
   ------------------------------------------------------------*/


  void verifyChangeWithToken(TemporaryStore temporaryStore) async{

    isVerifyingChange = true;

    setState(() {

    });


    temporaryStore.token = tokenController.text;

    HttpClientResponse tokenAuthenticationResponse = await new ApiHelper().verifySecurityUpdate(temporaryStore);

    if (tokenAuthenticationResponse.statusCode == 200) {

      String tokenAuthenticationResponseJsonString = await tokenAuthenticationResponse.transform(utf8.decoder).join();

      ResponseModel responseModel = new ResponseModel.fromJson(json.decode(tokenAuthenticationResponseJsonString));

      if(responseModel.isSuccessful){

        Navigator.pop(context);

        showAlertDialog(  AlertType.SUCCESS, "Success", "Your ${TemporaryStore.getRequestedUpdateAsString(temporaryStore.requestedUpdate)} has been successfully updated");

        switch(temporaryStore.requestedUpdate){

          case RequestedUpdate.EMAIL:

            user.email = emailController.text;

            break;

          case RequestedUpdate.PASSWORD:

            break;

          case RequestedUpdate.PHONENUMBER:

            user.phoneNumber = phoneNumberController.text;

            break;

          case RequestedUpdate.USERNAME:

            user.userName = usernameController.text;

            break;

          default:

            break;

        }

        await new SharedPreferenceHelper().setUser(user);

        setState(() {});

      }else{

        showAlertDialog(
            AlertType.ERROR, "Token Authorization", responseModel.responseMessage);

      }

    } else {
      showAlertDialog(
          AlertType.ERROR, "Email Update", "Ooop! connection error");
    }

    isUpdatingEmail = false;

    setState(() {});


  }

  void updateEmail() async{

    isUpdatingEmail = true;

    setState(() {

    });

    TemporaryStore temporaryStore = new TemporaryStore();

    temporaryStore.userName = user.userName;

    temporaryStore.value = emailController.text;

    temporaryStore.presentValue = user.email;

    temporaryStore.sendVia = MessageSender.SMS;

    temporaryStore.requestedUpdate = RequestedUpdate.EMAIL;

    HttpClientResponse emailUpdateResponse = await new ApiHelper().updateUserEmail(temporaryStore);

    if (emailUpdateResponse.statusCode == 200) {

      String emailUpdateResponseJsonString = await emailUpdateResponse.transform(utf8.decoder).join();

      print(emailUpdateResponseJsonString);

      ProfileUpdateModel profileUpdateModel = new ProfileUpdateModel.fromJson(json.decode(emailUpdateResponseJsonString));

      if(profileUpdateModel.responseModel.isSuccessful){

        temporaryStore.id = profileUpdateModel.temporaryStoreId;

        showAuthorizationTokenPanel("email", "sms", temporaryStore );

      }else{

        showAlertDialog(
            AlertType.ERROR, "Email Update", profileUpdateModel.responseModel.responseMessage);

      }

    } else {
      showAlertDialog(
          AlertType.ERROR, "Email Update", "Ooop! connection error");
    }

    isUpdatingEmail = false;

    setState(() {

    });


  }

  void updatePhoneNumber() async{

    isUpdatingPhoneNumber = true;

    setState(() {

    });

    TemporaryStore temporaryStore = new TemporaryStore();

    temporaryStore.userName = user.userName;

    temporaryStore.value = phoneNumberController.text;

    temporaryStore.presentValue = user.phoneNumber;

    temporaryStore.sendVia = MessageSender.EMAIL;

    temporaryStore.requestedUpdate = RequestedUpdate.PHONENUMBER;

    HttpClientResponse phoneNumberUpdateResponse = await new ApiHelper().updateUserPhoneNumber(temporaryStore);

    if (phoneNumberUpdateResponse.statusCode == 200) {

      String phoneNumberUpdateResponseJsonString = await phoneNumberUpdateResponse.transform(utf8.decoder).join();

      ProfileUpdateModel profileUpdateModel = new ProfileUpdateModel.fromJson(json.decode(phoneNumberUpdateResponseJsonString));

      if(profileUpdateModel.responseModel.isSuccessful){

        temporaryStore.id = profileUpdateModel.temporaryStoreId;

        showAuthorizationTokenPanel("phone number", "email", temporaryStore);

      }else{

        showAlertDialog(
            AlertType.ERROR, "Phone number update", profileUpdateModel.responseModel.responseMessage);

      }

    } else {
      showAlertDialog(
          AlertType.ERROR, "Phone number update", "Ooop! connection error");
    }

    isUpdatingPhoneNumber = false;

    setState(() {

    });


  }

  void updateUserName() async{

    isUpdatingUserName = true;

    setState(() {

    });

    TemporaryStore temporaryStore = new TemporaryStore();

    temporaryStore.userName = user.userName;

    temporaryStore.value = usernameController.text;

    temporaryStore.presentValue = user.userName;

    temporaryStore.sendVia = MessageSender.EMAIL;

    temporaryStore.requestedUpdate = RequestedUpdate.USERNAME;

    HttpClientResponse userNameUpdateResponse = await new ApiHelper().updateUsersUserName(temporaryStore);

    if (userNameUpdateResponse.statusCode == 200) {

      String userNameUpdateResponseJsonString = await userNameUpdateResponse.transform(utf8.decoder).join();

      ProfileUpdateModel profileUpdateModel = new ProfileUpdateModel.fromJson(json.decode(userNameUpdateResponseJsonString));

      if(profileUpdateModel.responseModel.isSuccessful){

        temporaryStore.id = profileUpdateModel.temporaryStoreId;

        showAuthorizationTokenPanel("user name", "email", temporaryStore);

      }else{

        showAlertDialog(
            AlertType.ERROR, "Error", profileUpdateModel.responseModel.responseMessage);

      }

    } else {
      showAlertDialog(
          AlertType.ERROR, "Error", "Ooop! connection error");
    }

    isUpdatingUserName = false;

    setState(() {

    });

  }


  void updatePassword() async{

    isUpdatingPassword = true;

    setState(() {

    });

    TemporaryStore temporaryStore = new TemporaryStore();

    temporaryStore.userName = user.userName;

    temporaryStore.value = newPasswordController.text;

    temporaryStore.presentValue = passwordController.text;

    temporaryStore.confirmValue = confirmPasswordController.text;

    temporaryStore.sendVia = MessageSender.EMAIL;

    temporaryStore.requestedUpdate = RequestedUpdate.PASSWORD;

    HttpClientResponse passwordUpdateResponse = await new ApiHelper().updateUserPassword(temporaryStore);

    if (passwordUpdateResponse.statusCode == 200) {

      String passwordUpdateResponseJsonString = await passwordUpdateResponse.transform(utf8.decoder).join();

      ProfileUpdateModel profileUpdateModel = new ProfileUpdateModel.fromJson(json.decode(passwordUpdateResponseJsonString));

      if(profileUpdateModel.responseModel.isSuccessful){

        temporaryStore.id = profileUpdateModel.temporaryStoreId;

        showAuthorizationTokenPanel("password", "email", temporaryStore);

      }else{

        showAlertDialog(
            AlertType.ERROR, "Password Update", profileUpdateModel.responseModel.responseMessage);

      }

    } else {
      showAlertDialog(
          AlertType.ERROR, "Password Update", "Ooop! connection error");
    }

    isUpdatingPassword = false;

    setState(() {

    });


  }



  /*------------------------------------------------------------
   -------------------SHOW BOTTOM SHEET METHOD------------------
  ------------------------------------------------------------**/


  void showAuthorizationTokenPanel(String updateField, String recipient, TemporaryStore temporaryStore ) {
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return new Container(
            padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
            color: Colors.white,
            child: new Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(
                  height: 10.0,
                ),
                Row(
                  children: <Widget>[
                    Text("ENTER YOUR CONFIRMATION TOKEN:",
                    style: TextStyle(
                        fontWeight: FontWeight.bold
                    ),),
                  ],
                ),

                SizedBox(
                  height: 10.0,
                ),

                Text(
                    "          A token was sent to your $recipient, for the update of your $updateField, Please input"
                        "the token below as a final step to finalize the update of your $updateField, or please"
                        "kindly ignore this if you have changed your mind, thank you.",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                    height: 1.5,
                  ),

                ),

                new Container(

                  margin: const EdgeInsets.only( top: 10.0, bottom: 20.0),
                  alignment: Alignment.center,
                  padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                  child: new TextFormField(

                    controller: tokenController,
                    decoration: new InputDecoration(

                        enabledBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          borderSide: BorderSide(
                            color: Colors.blue,
                            width: 1.0,
                          ),
                        ),
                        prefixIcon: Icon(Icons.security),
                        contentPadding: const EdgeInsets.only(
                            left: 10.0, top: 10.0, bottom: 10.0),
                        border: new OutlineInputBorder(
                          borderSide: BorderSide(
                            width: 1.0,
                          ),
                          gapPadding: 1.0,
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(5.0),
                          ),
                        ),
                        labelText: "Please input your token",
                        filled: true,
                        fillColor: Colors.white70),
                  ),
                ),

                _buildConfirmTokenButton(temporaryStore),

              ],
            ),
          );
        });
  }





  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    loadUsersDetails();

  }




  @override
  Widget build(BuildContext context) {





    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        image: DecorationImage(
          colorFilter: new ColorFilter.mode(
              Colors.black.withOpacity(0.1), BlendMode.dstATop),
          image: AssetImage('images/games.jpg'),
          fit: BoxFit.cover,
        ),
      ),
      
      child: ListView(
        children: <Widget>[

          Container(
            width: double.infinity,
            padding: EdgeInsets.only(top:50.0, bottom: 50.0, left: 20.0),

            child: Text("Security Info",
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: Colors.blue,
                    fontSize: 30.0
                )
            ),
          ),

          Stack(

            overflow: Overflow.visible,
            children: <Widget>[
              Container(
                width: double.infinity,
                child:Card(
                    margin: EdgeInsets.symmetric(horizontal: 20.0),
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 10.0),
                      margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
                      child: Form(
                        child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[


                          new Container(
                            width: MediaQuery.of(context).size.width,
                            margin: const EdgeInsets.only( top: 10.0, bottom: 20.0),
                            alignment: Alignment.center,
                            padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                            child: new Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                new Expanded(
                                  child: new TextFormField(

                                    controller: emailController,
                                    //validator: _nameValidator,
                                    decoration: new InputDecoration(
                                        enabledBorder: new OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(5.0),
                                          borderSide: BorderSide(
                                            color: Colors.blue,
                                            width: 1.0,
                                          ),
                                        ),
                                        prefixIcon: Icon(Icons.alternate_email),
                                        contentPadding: const EdgeInsets.only(
                                            left: 10.0, top: 10.0, bottom: 10.0),
                                        border: new OutlineInputBorder(
                                          borderSide: BorderSide(
                                            width: 1.0,
                                          ),
                                          gapPadding: 1.0,
                                          borderRadius: const BorderRadius.all(
                                            const Radius.circular(5.0),
                                          ),
                                        ),
                                        helperText: "A confirmation token will be sent to your phone",
                                        filled: true,
                                        fillColor: Colors.white70),
                                  ),
                                ),
                              ],
                            ),
                          ),

                         Row(
                           mainAxisAlignment: MainAxisAlignment.center,
                           children: <Widget>[
                             _buildUpdateEmailButton(),
                           ],
                         )



                        ],
                      ),
                      )
                    )
                ),
              ),

              Positioned(child: Card(
                  color:Colors.blue,
                  child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Text("Change E-mail", style: TextStyle(color: Colors.white),),
                  )
              ),
                left: 25.0,
                top: -12.0,
              )
            ],
          ),







          SizedBox(
            height: 70.0,
          ),















          Stack(

            overflow: Overflow.visible,
            children: <Widget>[
              Container(
                width: double.infinity,
                child:Card(
                    margin: EdgeInsets.symmetric(horizontal: 20.0),
                    child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
                        child: Form(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[


                              new Container(
                                width: MediaQuery.of(context).size.width,
                                margin: const EdgeInsets.only( top: 10.0, bottom: 20.0),
                                alignment: Alignment.center,
                                padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                                child: new Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    new Expanded(
                                      child: new TextFormField(

                                        controller: phoneNumberController,
                                        //validator: _nameValidator,
                                        decoration: new InputDecoration(
                                            enabledBorder: new OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(5.0),
                                              borderSide: BorderSide(
                                                color: Colors.blue,
                                                width: 1.0,
                                              ),
                                            ),
                                            prefixText: phoneCode,
                                            prefixIcon: Icon(Platform.isAndroid ? Icons.phone_android : Icons.phone_iphone),
                                            contentPadding: const EdgeInsets.only(
                                                left: 10.0, top: 10.0, bottom: 10.0),
                                            border: new OutlineInputBorder(
                                              borderSide: BorderSide(
                                                width: 1.0,
                                              ),
                                              gapPadding: 1.0,
                                              borderRadius: const BorderRadius.all(
                                                const Radius.circular(5.0),
                                              ),
                                            ),
                                            helperText: "A confirmation token will be sent to your email",
                                            filled: true,
                                            fillColor: Colors.white70),
                                      ),
                                    ),
                                  ],
                                ),
                              ),

                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  _buildUpdatePhoneNumberButton(),
                                ],
                              )


                            ],
                          ),
                        )
                    )
                ),
              ),

              Positioned(child: Card(
                  color:Colors.blue,
                  child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Text("Change Phone number", style: TextStyle(color: Colors.white),),
                  )
              ),
                left: 25.0,
                top: -12.0,
              )
            ],
          ),


















          SizedBox(
            height: 70.0,
          ),















          Stack(

            overflow: Overflow.visible,
            children: <Widget>[
              Container(
                width: double.infinity,
                child:Card(
                    margin: EdgeInsets.symmetric(horizontal: 20.0),
                    child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
                        child: Form(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[


                              new Container(
                                width: MediaQuery.of(context).size.width,
                                margin: const EdgeInsets.only( top: 10.0, bottom: 20.0),
                                alignment: Alignment.center,
                                padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                                child: new Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    new Expanded(
                                      child: new TextFormField(

                                        controller: usernameController,
                                        //validator: _nameValidator,
                                        decoration: new InputDecoration(
                                            enabledBorder: new OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(5.0),
                                              borderSide: BorderSide(
                                                color: Colors.blue,
                                                width: 1.0,
                                              ),
                                            ),
                                            prefixIcon: Icon(Icons.perm_identity),
                                            contentPadding: const EdgeInsets.only(
                                                left: 10.0, top: 10.0, bottom: 10.0),
                                            border: new OutlineInputBorder(
                                              borderSide: BorderSide(
                                                width: 1.0,
                                              ),
                                              gapPadding: 1.0,
                                              borderRadius: const BorderRadius.all(
                                                const Radius.circular(5.0),
                                              ),
                                            ),
                                            helperText: "Where should the token be sent to ?",
                                            filled: true,
                                            fillColor: Colors.white70),
                                      ),
                                    ),
                                  ],
                                ),
                              ),

                              new CustomMessageSenderRadioButton(
                                genderSelectionCallback: (sender){

                                },
                              ),

                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  _buildUserNameButton(),
                                ],
                              )


                            ],
                          ),
                        )
                    )
                ),
              ),

              Positioned(child: Card(
                  color:Colors.blue,
                  child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Text("Change User name", style: TextStyle(color: Colors.white),),
                  )
              ),
                left: 25.0,
                top: -12.0,
              )
            ],
          ),


























          SizedBox(
            height: 70.0,
          ),















          Stack(

            overflow: Overflow.visible,
            children: <Widget>[
              Container(
                width: double.infinity,
                child:Card(
                    margin: EdgeInsets.symmetric(horizontal: 20.0),
                    child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
                        child: Form(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[


                              new Container(
                                width: MediaQuery.of(context).size.width,
                                margin: const EdgeInsets.only( top: 10.0, bottom: 20.0),
                                alignment: Alignment.center,
                                padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,

                                  children: <Widget>[

                                    new TextFormField(

                                        controller: passwordController,
                                        //validator: _nameValidator,
                                        decoration: new InputDecoration(
                                            enabledBorder: new OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(5.0),
                                              borderSide: BorderSide(
                                                color: Colors.blue,
                                                width: 1.0,
                                              ),
                                            ),
                                            labelText : "Current password",
                                            prefixIcon: Icon(Icons.lock),
                                            contentPadding: const EdgeInsets.only(
                                                left: 10.0, top: 10.0, bottom: 10.0),
                                            border: new OutlineInputBorder(
                                              borderSide: BorderSide(
                                                width: 1.0,
                                              ),
                                              gapPadding: 1.0,
                                              borderRadius: const BorderRadius.all(
                                                const Radius.circular(5.0),
                                              ),
                                            ),
                                            filled: true,
                                            fillColor: Colors.white70),
                                      ),

                                     SizedBox(
                                       height: 30.0,
                                     ),


                                    new TextFormField(

                                      controller: newPasswordController,
                                      //validator: _nameValidator,
                                      decoration: new InputDecoration(
                                          enabledBorder: new OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5.0),
                                            borderSide: BorderSide(
                                              color: Colors.blue,
                                              width: 1.0,
                                            ),
                                          ),
                                          labelText: "New password",
                                          prefixIcon: Icon(Icons.lock),
                                          contentPadding: const EdgeInsets.only(
                                              left: 10.0, top: 10.0, bottom: 10.0),
                                          border: new OutlineInputBorder(
                                            borderSide: BorderSide(
                                              width: 1.0,
                                            ),
                                            gapPadding: 1.0,
                                            borderRadius: const BorderRadius.all(
                                              const Radius.circular(5.0),
                                            ),
                                          ),
                                          filled: true,
                                          fillColor: Colors.white70),
                                    ),

                                    SizedBox(
                                      height: 30.0,
                                    ),


                                     new TextFormField(

                                         controller: confirmPasswordController,
                                        //validator: _nameValidator,
                                        decoration: new InputDecoration(
                                            enabledBorder: new OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(5.0),
                                              borderSide: BorderSide(
                                                color: Colors.blue,
                                                width: 1.0,
                                              ),
                                            ),
                                            prefixIcon: Icon(Icons.security),
                                            labelText: "Confirm new password",
                                            contentPadding: const EdgeInsets.only(
                                                left: 10.0, top: 10.0, bottom: 10.0),
                                            border: new OutlineInputBorder(
                                              borderSide: BorderSide(
                                                width: 1.0,
                                              ),
                                              gapPadding: 1.0,
                                              borderRadius: const BorderRadius.all(
                                                const Radius.circular(5.0),
                                              ),
                                            ),
                                            helperText: "Where should the token be sent to ?",
                                            filled: true,
                                            fillColor: Colors.white70),
                                      ),

                                  ],
                                ),
                              ),

                              new CustomMessageSenderRadioButton(
                                genderSelectionCallback: (sender){

                                },
                              ),

                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  _buildPasswordButton(),
                                ],
                              )

                            ],
                          ),
                        )
                    )
                ),
              ),

              Positioned(child: Card(
                  color:Colors.blue,
                  child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Text("Change Password", style: TextStyle(color: Colors.white),),
                  )
              ),
                left: 25.0,
                top: -12.0,
              )
            ],
          ),




          SizedBox(height: 30.0,)


        ],
      ),
    );
  }
}
