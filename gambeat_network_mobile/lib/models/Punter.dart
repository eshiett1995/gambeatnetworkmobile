

import 'package:Gambeat/models/DefaultEntity.dart';
import 'package:Gambeat/models/User.dart';

class Punter {

  Punter();

  User player;

  bool isWinner;

  bool isEliminated = false;

  double winAmount;

  double risk;


  factory Punter.fromJson(Map<String, dynamic> parsedJson) {
    Punter punter = new Punter();

    punter.player = User.fromJson(parsedJson['player']);

    punter.isWinner = parsedJson['isWinner'];

    punter.isEliminated = parsedJson['isEliminated'];

    punter.winAmount = parsedJson['winAmount'];

    punter.risk = parsedJson['risk'];

    return punter;
  }


  Map<String, dynamic> toJson() => {

    '"player"': player == null ? null : player.toJson(),

    '"isWinner"': isWinner,

    '"isEliminated"': isEliminated,

    '"winAmount"': winAmount,

    '"risk"': risk,

  };


}
