import 'dart:convert';
import 'dart:io';

import 'package:Gambeat/models/Transactions.dart';
import 'package:Gambeat/models/rest-models/response/TransactionPaginationModel.dart';
import 'package:Gambeat/network/api/ApiHelper.dart';
import 'package:flutter/material.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

class TransactionsFragment extends StatefulWidget {
  @override
  _TransactionFragmentState createState() => _TransactionFragmentState();
}

class _TransactionFragmentState extends State<TransactionsFragment> {
  List<Transactions> transactionList = [];

  var _isSubsequentListLoading = false;

  var _isInitialListLoading = false;

  var endOfPageCalled = 0;

  var currentPageNumber = 0;

  int totalNumberOfPages = 0;

  void getInitialTransactionsFromServer() async {
    _setInitialListLoadingTrue();
    print("got ete");

    ApiHelper apiHelper = new ApiHelper();

    HttpClientResponse response = await apiHelper.getTransactionsByPage(0);

    //var mm = json.decode(response.body);

    String mm = await response.transform(utf8.decoder).join();

    var jsonString = json.decode(mm);
    if(response.statusCode == 200) {
      _setInitialListLoadingFalse();
      TransactionPaginationModel transactionPaginationModel =
      TransactionPaginationModel.fromJson(jsonString);
      totalNumberOfPages = transactionPaginationModel.totalPage;
      setState(() {
        transactionPaginationModel.content.forEach((user) {
          transactionList.add(user);
        });
      });

      //final userBean = UserBean(_adapter);

      //userBean.insertMany(friendList);
    }else _setInitialListLoadingFalse();
  }

  void _lazyLoading() {
    endOfPageCalled = endOfPageCalled + 1;
    print("===========================================================");
    print("LazyMethod Called!! --- end of Page");
    print("===========================================================");
    if (!(endOfPageCalled > 1)) //This is so to prevent multiple calling
      if (!(currentPageNumber >= totalNumberOfPages)) {
        print(
            "currentPageNumber NOT greater than or equal to totalNumberOfPages -- ");
        currentPageNumber = currentPageNumber + 1;
        getSubsequentTransactionsFromServer(pageNum: currentPageNumber);
      }
  }

  void getSubsequentTransactionsFromServer({int pageNum}) async {
    _setSubsequentListLoadingTrue();
    print("got ete");

    ApiHelper apiHelper = new ApiHelper();

    HttpClientResponse response = await apiHelper.getTransactionsByPage(pageNum);

    //var mm = json.decode(response.body);

    String mm = await response.transform(utf8.decoder).join();

    var jsonString = json.decode(mm);
    if(response.statusCode == 200) {
      _setSubsequentListLoadingFalse();
      TransactionPaginationModel transactionPaginationModel =
      TransactionPaginationModel.fromJson(jsonString);

      setState(() {
        transactionPaginationModel.content.forEach((user) {
          transactionList.add(user);
        });
      });

      //final userBean = UserBean(_adapter);

      //userBean.insertMany(friendList);
    }else _setSubsequentListLoadingFalse();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getInitialTransactionsFromServer();
  }

  @override
  Widget build(BuildContext context) {
    void transactionsPanelTapped(Transactions transction) {
      showModalBottomSheet(
          context: context,
          builder: (builder) {
            return new Container(
              padding: EdgeInsets.symmetric(horizontal: 15.0),
              color: Colors.white,
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    height: 20.0,
                  ),
                  Text("Transaction Detail:"),
                  SizedBox(
                    height: 20.0,
                  ),
                  Row(
                    children: <Widget>[
                      Text(transction.transactiontype)
                      ],
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(
                        Icons.receipt,
                        color: Colors.blue,
                        size: 20.0,
                      ),
                      SizedBox(
                        width: 3.0,
                      ),
                      Flexible(
                        fit: FlexFit.tight,
                        child: Text(
                          transction.referenceID,
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Colors.black54),
                          overflow: TextOverflow.ellipsis,
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(
                        Icons.credit_card,
                        color: Colors.blue,
                        size: 20.0,
                      ),
                      SizedBox(
                        width: 3.0,
                      ),
                      Flexible(
                        fit: FlexFit.tight,
                        child: Text(
                          transction.amount.toString(),
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Colors.black54),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(
                        Icons.info,
                        color: Colors.blue,
                        size: 20.0,
                      ),
                      SizedBox(
                        width: 3.0,
                      ),
                      Flexible(
                        fit: FlexFit.tight,
                        child: Text(
                          transction.detail,
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Colors.black54),
                          overflow: TextOverflow.fade,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.timer,
                        color: Colors.blue,
                        size: 20.0,
                      ),
                      SizedBox(
                        width: 3.0,
                      ),
                      Text("11 wednesday 2018",
                          style: TextStyle(color: Colors.black54)),
                    ],
                  ),
                  SizedBox(
                    height: 40.0,
                  ),
                ],
              ),
            );
          });
    }

    return Container(
      child: new Column(
        children: <Widget>[
          new Expanded(
              child: _isInitialListLoading
                  ?Center(child: CircularProgressIndicator(),)
                  :LazyLoadScrollView(
                onEndOfPage: _lazyLoading,
                child: ListView.builder(
                    itemCount: transactionList == null ? 0 : transactionList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return GestureDetector(
                          onTap: () {
                            transactionsPanelTapped(transactionList[index]);
                          },
                          child: Card(
                              margin:
                              EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                              child: Padding(
                                padding:
                                EdgeInsets.symmetric(vertical: 15.0, horizontal: 10.0),
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Text(
                                          transactionList[index].transactiontype,
                                          style: TextStyle(fontSize: 20.0),
                                        ),
                                        Spacer(),
                                        Text(
                                            "N ${transactionList[index].amount.toString()}"),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Icon(
                                          Icons.info,
                                          color: Colors.blue,
                                          size: 20.0,
                                        ),
                                        SizedBox(
                                          width: 3.0,
                                        ),
                                        Flexible(
                                          fit: FlexFit.tight,
                                          child: Text(
                                            transactionList[index].detail,
                                            textAlign: TextAlign.left,
                                            style: TextStyle(color: Colors.black54),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.timer,
                                          color: Colors.blue,
                                          size: 20.0,
                                        ),
                                        SizedBox(
                                          width: 3.0,
                                        ),
                                        Text("11 wednesday 2018",
                                            style: TextStyle(color: Colors.black54)),
                                      ],
                                    )
                                  ],
                                ),
                              )));
                    }),
              )
          ),
          _isSubsequentListLoading ? _getBottomProgressbar() : Center()
        ],
      ),
    );
  }

  Widget _getBottomProgressbar() {
    return new LinearProgressIndicator();
  }


  ///-------------------------------------------------------------------------
  ///This method sets state setting _initialListLoading bool var to false
  ///so as to control the progressbar indicating initial list items are being
  ///fetched from the api
  ///-------------------------------------------------------------------------
  _setInitialListLoadingFalse() {
    setState(() {
      _isInitialListLoading = false;
    });
  }

  ///This method sets state setting _initialListLoading bool var to true
  ///so as to control the progressbar indicating initial list items are being
  ///fetched from the api
  _setInitialListLoadingTrue() {
    setState(() {
      _isInitialListLoading = true;
    });
  }

  void _setSubsequentListLoadingTrue() {
    setState(() {
      _isSubsequentListLoading = true;
    });
  }

  void _setSubsequentListLoadingFalse() {
    setState(() {
      _isSubsequentListLoading = false;
    });
  }
}
