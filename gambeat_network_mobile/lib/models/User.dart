import 'dart:core';

import 'package:Gambeat/models/DefaultEntity.dart';
import 'package:Gambeat/models/PlayerStats.dart';

import 'Enum.dart';

class User extends DefaultEntity {

  User();

  String firstName;

  String lastName;

  String userName;

  String email;

  String password;

  //Gender gender;

  String gender;

  String address;

  String country;

  String picture;

  String phoneNumber;

  String bio;

  String isFriend;

  //OnlineStatus onlineStatus;
  String onlineStatus;

  //UserType userType;
  String userType;

  double money;

  PlayerStats playerStats;

  bool locked;

  bool activated;

  int unreadMessagesCount;

  factory User.fromJson(Map<String, dynamic> parsedJson) {
    User user = new User();

    user.id = parsedJson['_id'];

    // user.dateCreated = parsedJson['dateCreated'];

    user.firstName = parsedJson['firstName'];

    user.lastName = parsedJson['lastName'];

    user.userName = parsedJson['userName'];

    user.email = parsedJson['email'];

    user.password = parsedJson['password'];

    user.gender = parsedJson['gender'];

    user.address = parsedJson['address'];

    user.country = parsedJson['country'];

    user.isFriend = parsedJson['isFriend'];

    user.picture = parsedJson['picture'];

    user.phoneNumber = parsedJson['phoneNumber'];

    user.bio = parsedJson['bio'];

    user.onlineStatus = parsedJson['onlineStatus'];

    user.userType = parsedJson['userType'];

    user.money = parsedJson['money'].toDouble();

    user.playerStats = PlayerStats.fromJson(parsedJson['playerStats']);

    user.locked = parsedJson['locked'];

    user.activated = parsedJson['activated'];

    user.unreadMessagesCount = parsedJson['unreadMessagesCount'];

    return user;
  }

  Map<String, dynamic> toJson() => {
        '"_id"': '"$id"',
        '"dateCreated"': dateCreated,
        '"firstName"': '"$firstName"',
        '"lastName"': '"$lastName"',
        '"userName"': '"$userName"',
        '"email"': '"$email"',
        '"password"': '"$password"',
        '"gender"': '"$gender"',
        '"address"': '"$address"',
        '"country"': '"$country"',
        '"picture"': '"$picture"',
        '"phoneNumber"': '"$phoneNumber"',
        '"bio"': '"$bio"',
        '"isFriend"': isFriend,
        '"onlineStatus"': '"$onlineStatus"',
        '"userType"': '"$userType"',
        '"money"': money,
        '"playerStats"': playerStats == null ? null : playerStats.toJson(),
        '"locked"': locked,
        '"activated"': activated,
        '"unreadMessagesCount"': unreadMessagesCount,
      };
}
