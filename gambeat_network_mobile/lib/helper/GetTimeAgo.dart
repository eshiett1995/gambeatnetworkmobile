import 'package:intl/intl.dart';

///This method is not used
getTimeAgo(DateTime loadedTime) {
  if (_checkIfMonthAndDayEqualWithToday(loadedTime))
    return DateFormat.jm().format(loadedTime);
  if (_checkIfMessageWasSentYesterday(loadedTime))
    return "Yesterday";
  if (!(_checkIfMonthAndDayEqualWithToday(loadedTime) &&
      _checkIfMessageWasSentYesterday(loadedTime)))
    return new DateFormat.yMd().format(loadedTime);
}

_checkIfMonthAndDayEqualWithToday(DateTime loadedTime) {
  final now = new DateTime.now();
  if (loadedTime.year == now.year) {
    if (loadedTime.month == now.month)
      return true;
  }
  return false;
}

_checkIfMessageWasSentYesterday(DateTime loadedTime) {
  final now = new DateTime.now();
  if (loadedTime.year == now.year) {
    if (loadedTime.month == now.month)
      if (loadedTime.day == (loadedTime.day - 1))
        return true;
  }
  return false;
}
