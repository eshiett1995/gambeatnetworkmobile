import 'dart:convert';
import 'dart:io';

import 'package:Gambeat/helper/SharedPreferenceHelper.dart';
import 'package:Gambeat/models/User.dart';
import 'package:Gambeat/models/rest-models/response/ResponseModel.dart';
import 'package:Gambeat/network/api/ApiHelper.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;

class EditProfilePicture extends StatefulWidget {
  @override
  _EditProfilePictureState createState() => _EditProfilePictureState();
}

class _EditProfilePictureState extends State<EditProfilePicture> {

  File _image;

  bool isUploadingImage = false;

  SharedPreferenceHelper sharedPreference = new SharedPreferenceHelper();

  User user = new User();

  void loadUsersDetails() async {

    user = await sharedPreference.getUser();

    setState((){

    });
  }

  Future openCamera() async {

    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    uploadImageToServer(image);

    setState(() {

      _image = image;


    });

  }

  Future openGallery() async {

    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    uploadImageToServer(image);

    setState(() {

      _image = image;

    });

  }

  Future removeUsersProfileImage() async {

    HttpClientResponse removeUserProfileImageResponse = await new ApiHelper().removeUserProfilePicture();

    String responseModelString = await removeUserProfileImageResponse.transform(utf8.decoder).join();

    print(removeUserProfileImageResponse.toString());

      if (removeUserProfileImageResponse.statusCode == 200 ) {

      print("statusCode === 200 ooh");

      ResponseModel responseModel = ResponseModel.fromJson(json.decode(responseModelString));

      if(responseModel.isSuccessful){


      }

      print(responseModelString);

      if (responseModel.isSuccessful) {

        user = await sharedPreference.getUser();

        user.picture = "profile-picture";

        await sharedPreference.setUser(user);

        setState((){

        });

      }else{

        print("It was NOT sucessful ooh");

      }
    }else {

      print("statusCode NOT 200 ooh" + removeUserProfileImageResponse.statusCode.toString());

      print(responseModelString);
    }

  }

  Future uploadImageToServer(File imageFile) async{

    isUploadingImage = true;

    setState((){});

    http.StreamedResponse response = await new ApiHelper().uploadProfileImageToServer(imageFile);

    if(response.statusCode == 200 || response.statusCode == 201){

    }else if(response.statusCode == 500){


    }else if(response.statusCode == 401){

      //todo for unauthorized request
    }else if(response.statusCode == 403){


    }else{


    }

    response.stream.transform(utf8.decoder).listen((value) {
         print(value);
    });

    isUploadingImage = false;

    setState((){});

  }

  @override
  void initState() {
    super.initState();

    loadUsersDetails();

  }


  @override
  Widget build(BuildContext context) {

    return new Scaffold(

      body: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          image: DecorationImage(
            image: AssetImage('images/background_main.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: new Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[

                Stack(
                  children: <Widget>[


                    new Container(
                      width: 200.0,
                      height: 200.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100.0),
                          color: Colors.blue,

                        ),

                      child: Container(
                        padding: EdgeInsets.all(1.5),
                        child: CircleAvatar(
                          backgroundImage: user.picture == "profile-picture" || user.picture == ""  ?

                          AssetImage('images/profile_photo.jpg') : NetworkImage('http://40.87.11.131:3000/images/profile-pictures/${user.picture}',
                          ),
                          radius: 90.0,
                        ),
                      )
                    ),


                    new Positioned(
                        top: 80.0,
                        right: 80.0,
                        child: new Visibility(
                          child: new CircularProgressIndicator(
                            valueColor: new AlwaysStoppedAnimation(Colors.white),
                          ),
                          visible: isUploadingImage,
                        )
                    ) ,

                    new Positioned(
                      top: 20.0,
                      right: 15.0,
                      child: GestureDetector(
                        onTap: (){

                         removeUsersProfileImage();

                        },

                        child: Material(
                            type: MaterialType.circle,
                            elevation: 3.0,
                            color: Colors.redAccent,
                            child: Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Icon(Icons.clear,
                                  color: Colors.white,
                                  size: 12.0,)
                            )),
                      )
                    ),

                  ],
                ),

                SizedBox(height: 30.0,),

                new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new RaisedButton(
                      onPressed: (){

                        openGallery();

                        },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Icon(Icons.image, color: Colors.white,),

                          SizedBox(width: 5.0,),

                          Text("Gallery", style: TextStyle(color: Colors.white),),

                        ],
                      ),
                      color: Colors.blue,
                    ),
                    SizedBox(
                      width: 30.0,
                    ),
                    new RaisedButton(
                      onPressed: (){

                        openCamera();

                      },
                      child: new Row(
                        children: <Widget>[
                          Icon(Icons.camera_alt, color: Colors.white,),

                          SizedBox(width: 5.0,),

                          Text("Camera", style: TextStyle(color: Colors.white),),
                        ],
                      ),
                      color: Colors.blue,
                    )
                  ],
                )
              ],
            )
        ),
      ),
    );
  }
}
