import 'package:Gambeat/models/rest-models/response/FriendAndMessagesModel.dart';

class FriendsAndMessagesPaginationModel{

  FriendsAndMessagesPaginationModel();

  int totalElements;

  int totalPage;

  bool last;

  int size;

  int number;

  String sort;

  bool first;

  int numberOfElements;

  List<FriendAndMessagesModel> content;

  factory FriendsAndMessagesPaginationModel.fromJson(Map<String, dynamic> parsedJson) {

    FriendsAndMessagesPaginationModel friendsAndMessagesPaginationModel = new FriendsAndMessagesPaginationModel();

    friendsAndMessagesPaginationModel.totalElements = parsedJson['totalElements'];

    friendsAndMessagesPaginationModel.totalPage = parsedJson['totalPage'];

    friendsAndMessagesPaginationModel.last = parsedJson['last'];

    friendsAndMessagesPaginationModel.size = parsedJson['size'];

    friendsAndMessagesPaginationModel.number = int.tryParse(parsedJson['number']);

    friendsAndMessagesPaginationModel.sort = parsedJson['sort'];

    friendsAndMessagesPaginationModel.first = parsedJson['first'];

    friendsAndMessagesPaginationModel.numberOfElements = parsedJson['numberOfElements'];

    var list = parsedJson['content'] as List;

    friendsAndMessagesPaginationModel.content = list.map((i) => FriendAndMessagesModel.fromJson(i)).toList();

    return friendsAndMessagesPaginationModel;
  }
}
