import 'package:Gambeat/fragments/social/gamer_profile.dart';
import 'package:Gambeat/models/Competition.dart';
import 'package:Gambeat/models/User.dart';
import 'package:flutter/material.dart';

class Stages extends StatefulWidget {

  final Competition competition;

  Stages({this.competition});

  @override
  _StagesState createState() => _StagesState();
}

class _StagesState extends State<Stages> {

  void userOptionsPanel(User selectedUser) {
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return new Container(
            color: Colors.white,
            child: new Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(
                  height: 20.0,
                ),
                Text("SELECT AN OPTION:"),
                ListTile(
                  leading: Icon(Icons.account_circle),
                  title: Text("Profile"),
                  onTap: () {
                    var route = new MaterialPageRoute(
                      builder: (BuildContext context) =>
                      new GamerProfile(user: selectedUser),
                    );

                    Navigator.of(context).push(route);
                  },
                ),
                ListTile(
                  leading: Icon(Icons.message),
                  title: Text("Message"),
                ),
                ListTile(
                  leading: Icon(Icons.reply),
                  title: Text("Unfollow"),
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          title: Text("Stages"),
        ),
        body: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            image: DecorationImage(
              colorFilter: new ColorFilter.mode(
                  Colors.black.withOpacity(0.2), BlendMode.dstATop),
              image: AssetImage('images/games.jpg'),
              fit: BoxFit.cover,
            ),
          ),
          child: ListView(
            children: <Widget>[
              new GestureDetector(
                onTap: () {
                  //ShowMatchInfo();
                },
                child: new Card(
                    elevation: 5.0,
                    margin:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                    child: Column(
                      children: <Widget>[
                        new Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 10.0),
                          child: Column(
                            children: <Widget>[
                              Container(
                                width: double.infinity,
                                child: Card(
                                  color: Colors.lightBlue,
                                  child: Container(
                                    margin: EdgeInsets.all(5.0),
                                    child: Text(
                                      "Stage 1",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  new GestureDetector(
                                    onTap:(){
                                      userOptionsPanel(null);
                                    },
                                    child: new Column(children: <Widget>[
                                      Stack(
                                        children: <Widget>[
                                          new CircleAvatar(
                                            backgroundImage:
                                            AssetImage('images/chris.jpg'),
                                            minRadius: 40.0,
                                          ),
                                          new Positioned(
                                            left: 0.0,
                                            child: Image.asset(
                                              "images/xmark.png",
                                              height: 80.0,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 5.0,
                                      ),
                                      Text(
                                        "Mr Bassey",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ]),
                                  ),
                                  new Image.asset(
                                    'images/versus.png',
                                    width: 100.0,
                                    height: 100.0,
                                  ),
                                  new GestureDetector(
                                    onTap: (){
                                      userOptionsPanel(null);
                                    },
                                    child: new Column(children: <Widget>[
                                      Stack(
                                        children: <Widget>[
                                          new  CircleAvatar(
                                            backgroundImage:
                                            AssetImage('images/chris.jpg'),
                                            minRadius: 40.0,
                                          ),
                                          new Positioned(
                                            left: 0.0,
                                            child: Image.asset(
                                              "images/xmark.png",
                                              height: 80.0,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 5.0,
                                      ),
                                      Text(
                                        "Nsikan",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ]),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 15.0),
                          child: new Divider(
                            color: Colors.black26,
                          ),
                        ),
                      ],
                    )),
              )
            ],
          ),
        ));
  }
}
