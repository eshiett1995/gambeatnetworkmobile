import 'package:Gambeat/models/User.dart';

class UserPaginationModel{

  UserPaginationModel();

  int totalElements;

  int totalPage;

  bool last;

  int size;

  int number;

  String sort;

  bool first;

  int numberOfElements;

  List<User> content;

  factory UserPaginationModel.fromJson(Map<String, dynamic> parsedJson) {

    UserPaginationModel userPaginationModel = new UserPaginationModel();

    userPaginationModel.totalElements = parsedJson['totalElements'];

    userPaginationModel.totalPage = parsedJson['totalPages'];

    userPaginationModel.last = parsedJson['last'];

    userPaginationModel.size = parsedJson['size'];

    userPaginationModel.number = int.tryParse(parsedJson['number']);

    userPaginationModel.sort = parsedJson['sort'];

    userPaginationModel.first = parsedJson['first'];

    userPaginationModel.numberOfElements = parsedJson['numberOfElements'];

    var list = parsedJson['content'] as List;

    userPaginationModel.content = list.map((i) => User.fromJson(i)).toList();

    return userPaginationModel;
  }

}