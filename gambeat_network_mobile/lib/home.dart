import 'package:Gambeat/fragments/social/user_profile.dart';
import 'package:flutter/material.dart';

import './fragments/home_fragment.dart';
import './fragments/settings_fragment.dart';
import './fragments/messages/chat-list.dart';
import './fragments/deposit_fragment.dart';

import './fragments/social/friends.dart';

import 'package:badges/badges.dart';

class DrawerItem {
  String title;
  IconData icon;
  DrawerItem(this.title, this.icon);
}

class HomePage extends StatefulWidget {
  final drawerItems = [
    new DrawerItem("Home", Icons.dashboard),
    new DrawerItem("Profile", Icons.account_circle),
    new DrawerItem("Social", Icons.group),
    new DrawerItem("Wallet", Icons.credit_card),
    new DrawerItem("Messages", Icons.chat),
    new DrawerItem("Settings", Icons.settings)
  ];

  @override
  State<StatefulWidget> createState() {
    return new HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  int _selectedDrawerIndex = 0;

  _getDrawerItemWidget(int pos) {
    switch (pos) {
      case 0:
        return new HomeFragment();
      case 1:
        return new UserProfile();
      case 2:
        return new Friends();
      case 3:
        return new DepositFragment();

      case 4:
        return new ChatList();

      case 5:
        return new SettingsFragment();

      case 6:
        return new ChatList();

      default:
        return new Text("Error");
    }
  }

  _onSelectItem(int index) {
    setState(() => _selectedDrawerIndex = index);
    Navigator.of(context).pop(); // close the drawer
  }

  @override
  Widget build(BuildContext context) {
    var drawerOptions = <Widget>[];



    drawerOptions.add(ExpansionTile(
      leading: BadgeIconButton(
                itemCount: 1,
                badgeColor: Colors.blue,
                badgeTextColor: Colors.white,
                icon: new Icon(Icons.timer_off),
                onPressed: () {}),
      title: Text("Animal"),
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
            left: 30.0,
          ),
          child: ListTile(
            leading: new Icon(Icons.fast_forward),
            title: Text("dog"),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            left: 30.0,
          ),
          child: ListTile(
            leading: new Icon(Icons.fast_forward),
            title: Text("dog"),
          ),
        ),
      ],
    ));

    for (var i = 0; i < widget.drawerItems.length; i++) {
      var d = widget.drawerItems[i];
      drawerOptions.add(new ListTile(
        leading: new Icon(d.icon),
        title: new Text(d.title),
        selected: i == _selectedDrawerIndex,
        onTap: () => _onSelectItem(i),
      ));
    }

    drawerOptions.add(new Divider(
      color: Colors.blue,
    ));

   /** drawerOptions.add(new ListTile(
      leading: new Icon(Icons.settings),
      title: new Text("Settings"),
      selected: false,
      onTap: () => _onSelectItem(6),
    )); **/

    drawerOptions.add(new ListTile(
      leading: new Icon(Icons.settings_power),
      title: new Text("Logout"),
      selected: false,
      onTap: () => _onSelectItem(1),
    ));

    return new Scaffold(
      appBar: new AppBar(
        // here we display the title corresponding to the fragment
        // you can instead choose to have a static title
        title: new Text(widget.drawerItems[_selectedDrawerIndex].title),
      ),
      drawer: SizedBox(
          width: 240.0,
          child: new Drawer(
            child: new Column(
              children: <Widget>[
                new UserAccountsDrawerHeader(
                    accountName: new Text("John Doe"),
                    accountEmail: new Text("eshiett1995@gmail.com"),
                    currentAccountPicture: new GestureDetector(
                      child: new CircleAvatar(
                        backgroundImage: AssetImage('images/chris.jpg'),
                      ),
                      onTap: () => print("This is your current account."),
                    ),
                    decoration: new BoxDecoration(
                        image: new DecorationImage(
                            image: new NetworkImage(
                                "https://img00.deviantart.net/35f0/i/2015/018/2/6/low_poly_landscape__the_river_cut_by_bv_designs-d8eib00.jpg"),
                            fit: BoxFit.fill))),
                Expanded(
                  child: ListView(
                    children: drawerOptions,
                  ),
                )
              ],
            ),
          )),
      body: _getDrawerItemWidget(_selectedDrawerIndex),
    );
  }
}

class MyTile {
  String title;

  List<MyTile> children;

  MyTile(this.title, [this.children = const <MyTile>[]]);

  List<MyTile> listOfTiles = <MyTile>[
    new MyTile("Animals", <MyTile>[
      new MyTile("Dog"),
      new MyTile("Bird"),
      new MyTile("Cat"),
    ])
  ];
}
