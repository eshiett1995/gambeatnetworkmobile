import 'package:Gambeat/models/Message.dart';
import 'package:Gambeat/models/User.dart';

class FriendAndMessagesModel{

  FriendAndMessagesModel();

  User friend;

  List<Message> messages;

factory FriendAndMessagesModel.fromJson(Map<String, dynamic> parsedJson) {

    FriendAndMessagesModel friendAndMessagesModel = new FriendAndMessagesModel();

    friendAndMessagesModel.friend = User.fromJson(parsedJson['friend']);

    var list = parsedJson['messages'] as List;

    friendAndMessagesModel.messages = list.map((i) => Message.fromJson(i)).toList();

    return friendAndMessagesModel;
  }

}