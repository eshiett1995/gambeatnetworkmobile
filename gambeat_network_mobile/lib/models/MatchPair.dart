import 'package:Gambeat/models/DefaultEntity.dart';

class MatchPair extends DefaultEntity{

  MatchPair();

  String playerOne;

  String playerTwo;

  String winner;

  factory MatchPair.fromJson(Map<String, dynamic> parsedJson) {

    MatchPair matchPair = new MatchPair();

    matchPair.id = parsedJson['_id'];

    matchPair.dateCreated = parsedJson['dateCreated'];

    matchPair.playerOne = parsedJson['playerOne'];

    matchPair.playerTwo = parsedJson['playerTwo'];

    matchPair.winner = parsedJson['winner'];

    return matchPair;
  }

  Map<String, dynamic> toJson() => {

    '"_id"': id,

    '"dateCreated"': dateCreated,

    '"playerOne"': '"$playerOne"',

    '"playerTwo"': '"$playerTwo"',

    '"winner"': '"$winner"',
  };

}