import 'dart:convert';
import 'dart:io';

import 'package:Gambeat/models/PlayerStats.dart';
import 'package:Gambeat/models/rest-models/response/ResponseModel.dart';
import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_pickers.dart';
import 'package:Gambeat/custom-widgets/custom_radio_button_for_gender.dart';
import 'package:Gambeat/helper/SharedPreferenceHelper.dart';
import 'package:Gambeat/models/User.dart';
import 'package:Gambeat/network/api/ApiHelper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_sweet_alert/flutter_sweet_alert.dart';

import './models/rest-models/response/LoginModel.dart';

class LoginScreen3 extends StatefulWidget {
  @override
  _LoginScreen3State createState() => new _LoginScreen3State();
}

class _LoginScreen3State extends State<LoginScreen3>
    with TickerProviderStateMixin {
  bool isLoggingIn = false;

  bool isSigningUp = false;

  Country _selectedDialogCountry = CountryPickerUtils.getCountryByIsoCode('tr');

  TextEditingController loginEmailController = TextEditingController();

  TextEditingController loginPasswordTextController = TextEditingController();

  final String emailNotValidText = 'Email is not valid';
  final String passwordNotValidText =
      'Password must not be less than 8 characters';
  final String confirmPasswordNotValidText = 'Password does not match';
  final String fieldEmptyText = "Field Must not be empty";

  TextEditingController signUpFirstNameController = TextEditingController();
  TextEditingController signUpLastNameController = TextEditingController();
  TextEditingController signUpUserNameController = TextEditingController();
  TextEditingController signUpDOBController = TextEditingController();
  TextEditingController signUpEmailController = TextEditingController();
  TextEditingController signUpPhoneNumberController = TextEditingController();
  TextEditingController signUpPasswordController = TextEditingController();
  TextEditingController signUpConfirmPasswordController =
      TextEditingController();

  String genderString = "Non";
  void showAlertDialog(AlertType alertType, String title, String content) {
    SweetAlert.dialog(
        type: alertType,
        title: title,
        content: content,
        cancelButtonText: "Okay",
        confirmButtonText: "ok");
  }

  Future signUp() async {
    if (genderString != 'Non') {
      User user = _initSignUpDetails();

      user.playerStats = new PlayerStats();

      ApiHelper apiHelper = new ApiHelper();

      HttpClientResponse signUpResponse = await apiHelper.signUp(user);

      //var t = json.decode(c);
      String m = await signUpResponse.transform(utf8.decoder).join();
      print(signUpResponse.toString());

      setState(() {
        isSigningUp = false;
      });

      if (signUpResponse.statusCode == 200) {
        print("statusCode === 200 ooh");
        ResponseModel responseModel = ResponseModel.fromJson(json.decode(m));
        print(m);
        if (responseModel.isSuccessful) {
          showAlertDialog(AlertType.SUCCESS, "Signup Successful",
              "You have been signed up");
        } else {
          showAlertDialog(AlertType.ERROR, "Signup Unsuccessful",
              responseModel.responseMessage);
        }
      } else {
        showAlertDialog(AlertType.ERROR, "Signup Failed",
            "Oops! might be connection error");
      }
    } else {
      setState(() {
        isSigningUp = false;
      });

      _signUpScaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  Future login() async {
    setState(() {
      isLoggingIn = true;
    });

    User user = new User();

    user.userName = loginEmailController.text;

    user.password = loginPasswordTextController.text;

    ApiHelper apiHelper = new ApiHelper();

    HttpClientResponse loginResponse = await apiHelper.login(user);

    print(loginResponse.statusCode == 200);

    setState(() {
      isLoggingIn = false;
    });

    if (loginResponse.statusCode == 200) {
      String mm = await loginResponse.transform(utf8.decoder).join();

      print(mm);

      LoginModel loginModel = new LoginModel.fromJson(json.decode(mm));

      SharedPreferenceHelper sharedPreference = new SharedPreferenceHelper();

      await sharedPreference.setUser(loginModel.user);

      await sharedPreference.setAuthToken(loginModel.responseModel.jwtToken);

      await sharedPreference.setSessionId(loginModel.responseModel.sessionId);

      await sharedPreference.setIsLoggedInStatus(true);


      Navigator.of(context).pushReplacementNamed('/home');
    } else if (loginResponse.statusCode == 404) {
      showAlertDialog(
          AlertType.ERROR, "Login unsuccessful", "Invalid credentials");
    } else {
      showAlertDialog(
          AlertType.ERROR, "Login unsuccessful", "Ooop! connection error");
    }
  }

  String _countryCode = "+234";

  Widget _countryFlag = Image.asset("name");

  Country _selectedCupertinoCountry =
      CountryPickerUtils.getCountryByIsoCode('tr');

  Widget _buildDropdownItem(Country country) => Container(
        child: Row(
          children: <Widget>[
            CountryPickerUtils.getDefaultFlagImage(country),
            SizedBox(
              width: 8.0,
            ),
            Text("+${country.phoneCode}(${country.isoCode})"),
          ],
        ),
      );

  Widget _buildDialogItem(Country country) {
    _countryFlag = CountryPickerUtils.getDefaultFlagImage(country);

    _countryCode = "+${country.phoneCode}";

    return Row(
      children: <Widget>[
        CountryPickerUtils.getDefaultFlagImage(country),
        SizedBox(width: 8.0),
        SizedBox(width: 8.0),
        Flexible(child: Text(country.name))
      ],
    );
  }

  void _openCountryPickerDialog() => showDialog(
        context: context,
        builder: (context) => Theme(
            data: Theme.of(context).copyWith(primaryColor: Colors.pink),
            child: CountryPickerDialog(
                titlePadding: EdgeInsets.all(8.0),
                searchCursorColor: Colors.pinkAccent,
                searchInputDecoration: InputDecoration(hintText: 'Search...'),
                isSearchable: true,
                title: Text('Select your phone code'),
                onValuePicked: (Country country) =>
                    setState(() => _selectedDialogCountry = country),
                itemBuilder: _buildDialogItem)),
      );

  void _openCupertinoCountryPicker() => showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) {
        return CountryPickerCupertino(
          pickerSheetHeight: 200.0,
          onValuePicked: (Country country) =>
              setState(() => _selectedCupertinoCountry = country),
        );
      });

  Widget _buildCupertinoItem(Country country) => Row(
        children: <Widget>[
          CountryPickerUtils.getDefaultFlagImage(country),
          SizedBox(width: 8.0),
          Text("+${country.phoneCode}"),
          SizedBox(width: 8.0),
          Flexible(child: Text(country.name))
        ],
      );

  Widget _buildLoginButton() {
    if (!isLoggingIn) {
      return Expanded(
        child: RaisedButton(
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0),
          ),
          color: Colors.blue,
          onPressed: () {
            if (_formKeyLogin.currentState.validate()) {
              setState(() {
                login();
                //print(loginPasswordTextController.text);
              });
            }
          },
          child: new Container(
            padding: const EdgeInsets.symmetric(
              vertical: 20.0,
              horizontal: 20.0,
            ),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Expanded(
                  child: Text(
                    "LOGIN",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    } else {
      return Container(
        height: 48.0,
        width: 48.0,
        child: RaisedButton(
          padding: EdgeInsets.all(0.0),
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0),
          ),
          child: SizedBox(
            height: 30.0,
            width: 30.0,
            child: CircularProgressIndicator(
              value: null,
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
            ),
          ),
          disabledColor: Colors.blue,
          onPressed: null,
        ),
      );
    }
  }

  Widget _buildSignUpButton() {
    if (!isSigningUp) {
      return new Expanded(
        child: new FlatButton(
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0),
          ),
          color: Colors.blue,
          onPressed: () {
            if (_formKeySignUp.currentState.validate()) {
              setState(() {
                isSigningUp = true;

                signUp();
              });
            }
          },
          child: new Container(
            padding: const EdgeInsets.symmetric(
              vertical: 20.0,
              horizontal: 20.0,
            ),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Expanded(
                  child: Text(
                    "SIGN UP",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    } else {
      return Container(
        height: 48.0,
        width: 48.0,
        child: RaisedButton(
          padding: EdgeInsets.all(0.0),
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0),
          ),
          child: SizedBox(
            height: 30.0,
            width: 30.0,
            child: CircularProgressIndicator(
              value: null,
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
            ),
          ),
          disabledColor: Colors.blue,
          onPressed: null,
        ),
      );
    }
  }

  //The code is commented because instead of manual scrolling with animation,
  //Now PageView is being used

  /*double scrollPercent = 0.0;
  Offset startDrag;
  double startDragPercentScroll;
  double dragDirection; // -1 for left, +1 for right

  AnimationController controller_minus1To0;
  AnimationController controller_0To1;
  CurvedAnimation anim_minus1To0;
  CurvedAnimation anim_0To1;

  final numCards = 3;

  void _onHorizontalDragStart(DragStartDetails details) {
    startDrag = details.globalPosition;
    startDragPercentScroll = scrollPercent;
  }

  void _onHorizontalDragUpdate(DragUpdateDetails details) {
    final currDrag = details.globalPosition;
    final dragDistance = currDrag.dx - startDrag.dx;
    if (dragDistance > 0) {
      dragDirection = 1.0;
    } else {
      dragDirection = -1.0;
    }
    final singleCardDragPercent = dragDistance / context.size.width;

    setState(() {
      scrollPercent =
          (startDragPercentScroll + (-singleCardDragPercent / numCards))
              .clamp(0.0 - (1 / numCards), (1 / numCards));
      print(scrollPercent);
    });
  }

  void _onHorizontalDragEnd(DragEndDetails details) {
    if (scrollPercent > 0.1666) {
      print("FIRST CASE");
      controller_0To1.forward(from: scrollPercent * numCards);
    } else if (scrollPercent < 0.1666 &&
        scrollPercent > -0.1666 &&
        dragDirection == -1.0) {
      print("SECOND CASE");
      controller_0To1.reverse(from: scrollPercent * numCards);
    } else if (scrollPercent < 0.1666 &&
        scrollPercent > -0.1666 &&
        dragDirection == 1.0) {
      print("THIRD CASE");
      controller_minus1To0.forward(from: scrollPercent * numCards);
    } else if (scrollPercent < -0.1666) {
      print("LAST CASE");
      controller_minus1To0.reverse(from: scrollPercent * numCards);
    }

    setState(() {
      startDrag = null;
      startDragPercentScroll = null;
    });
  }
  */

  @override
  void initState() {
    super.initState();

    //The code is commented because instead of manual scrolling with animation,
    //Now PageView is being used

    /*
    controller_minus1To0 = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
      lowerBound: -1.0,
      upperBound: 0.0,
    );
    controller_0To1 = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
      lowerBound: 0.0,
      upperBound: 1.0,
    );

    anim_minus1To0 = new CurvedAnimation(
      parent: controller_minus1To0,
      curve: Interval(0.10, 0.90, curve: Curves.bounceInOut),
    );
    anim_0To1 = new CurvedAnimation(
      parent: controller_0To1,
      curve: Interval(0.10, 0.90, curve: Curves.bounceInOut),
    );

    anim_0To1.addListener(() {
      scrollPercent = controller_0To1.value / numCards;
//      print(scrollPercent);
      setState(() {});
    });

    anim_minus1To0.addListener(() {
      scrollPercent = controller_minus1To0.value / numCards;
//      print(scrollPercent);
      setState(() {});
    });
    */
  }

  Widget HomePage() {
    return Material(
      child: new ListView(
        children: <Widget>[
          new Container(
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              color: Colors.white,
              image: DecorationImage(
                colorFilter: new ColorFilter.mode(
                    Colors.black.withOpacity(0.3), BlendMode.dstATop),
                image: AssetImage('images/space.gif'),
                fit: BoxFit.cover,
              ),
            ),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 150.0, left: 40.0),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Start",
                        style: TextStyle(
                          decoration: TextDecoration.none,
                          color: Colors.black,
                          fontSize: 30.0,
                        ),
                      ),
                      Text(
                        "your",
                        style: TextStyle(
                          decoration: TextDecoration.none,
                          color: Colors.black,
                          fontSize: 30.0,
                        ),
                      ),
                      Text(
                        "adventure",
                        style: TextStyle(
                          decoration: TextDecoration.none,
                          color: Colors.black,
                          fontSize: 30.0,
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  width: MediaQuery.of(context).size.width,
                  margin: const EdgeInsets.only(
                      left: 30.0, right: 30.0, top: 100.0),
                  alignment: Alignment.center,
                  child: new Row(
                    children: <Widget>[
                      new Expanded(
                        child: new RaisedButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0)),
                          color: Colors.blue,
                          onPressed: () => gotoSignup(),
                          child: new Container(
                            padding: const EdgeInsets.symmetric(
                              vertical: 20.0,
                              horizontal: 20.0,
                            ),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Expanded(
                                  child: Text(
                                    "SIGN UP",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  width: MediaQuery.of(context).size.width,
                  margin:
                      const EdgeInsets.only(left: 30.0, right: 30.0, top: 30.0),
                  alignment: Alignment.center,
                  child: new Row(
                    children: <Widget>[
                      new Expanded(
                        child: new RaisedButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0)),
                          color: Colors.redAccent,
                          onPressed: () => gotoLogin(),
                          child: new Container(
                            padding: const EdgeInsets.symmetric(
                              vertical: 20.0,
                              horizontal: 20.0,
                            ),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Expanded(
                                  child: Text(
                                    "LOGIN",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  //String emailTextVal;
  String _emailValidator(String value) {
    //String value = email;
    if (value.isEmpty) {
      return "Email Field cannot be empty";
    }

    // This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      // So, the email is valid
      return null;
    }

    // The pattern of the email didn't match the regex above.
    return emailNotValidText;
  }

  String _passwordValidator(String value) {
    if (value.isEmpty) return fieldEmptyText;

    if (value.length < 8) return passwordNotValidText;
  }

  String password = "";

  String _confirmPasswordValidator(String value) {
    if (value.isEmpty) return fieldEmptyText;

    if (value != password) return confirmPasswordNotValidText;
  }

  String _phoneNumberValidator(String value) {
    if (value.isEmpty) return fieldEmptyText;
  }

  String _nameValidator(String value) {
    if (value.isEmpty) return fieldEmptyText;
  }

  final _formKeyLogin = GlobalKey<FormState>();
  final _formKeySignUp = GlobalKey<FormState>();
  final _signUpScaffoldKey = GlobalKey<ScaffoldState>();

  ///This method sets the controller listeners for Login page,
  /// so as to listen to changes made and update th validate error
  _setLoginControllersListeners() {
    loginEmailController.addListener(() {
      if (loginEmailController.text.length > 0) {
        setState(() {
          _formKeyLogin.currentState.validate();
        });
      }
    });

    loginPasswordTextController.addListener(() {
      if (loginEmailController.text.length > 0) {
        setState(() {
          _formKeyLogin.currentState.validate();
        });
      }
    });
  }

  ///This method sets the controller listeners for SignUp page,
  /// so as to listen to changes made and update th validate error
  _setSignUpControllersListeners() {
    signUpFirstNameController.addListener(() {
      if (_isTextLengthGreaterThanZero(signUpFirstNameController))
        setState(() {
          _formKeySignUp.currentState.validate();
        });
    });

    signUpLastNameController.addListener(() {
      if (_isTextLengthGreaterThanZero(signUpLastNameController))
        setState(() {
          _formKeySignUp.currentState.validate();
        });
    });
    signUpUserNameController.addListener(() {
      if (_isTextLengthGreaterThanZero(signUpUserNameController))
        setState(() {
          _formKeySignUp.currentState.validate();
        });
    });
    signUpDOBController.addListener(() {
      if (_isTextLengthGreaterThanZero(signUpDOBController))
        setState(() {
          _formKeySignUp.currentState.validate();
        });
    });
    signUpEmailController.addListener(() {
      if (_isTextLengthGreaterThanZero(signUpEmailController))
        setState(() {
          _formKeySignUp.currentState.validate();
        });
    });
    signUpPhoneNumberController.addListener(() {
      if (_isTextLengthGreaterThanZero(signUpPhoneNumberController))
        setState(() {
          _formKeySignUp.currentState.validate();
        });
    });
    signUpPasswordController.addListener(() {
      if (_isTextLengthGreaterThanZero(signUpPasswordController))
        setState(() {
          password = signUpPasswordController.text;
          _formKeySignUp.currentState.validate();
        });
    });
    signUpConfirmPasswordController.addListener(() {
      if (_isTextLengthGreaterThanZero(signUpConfirmPasswordController))
        setState(() {
          _formKeySignUp.currentState.validate();
        });
    });
  }

  bool _isTextLengthGreaterThanZero(TextEditingController textC) =>
      textC.text.length > 0;

  Widget LoginPage() {
    // _setLoginControllersListeners();
    return new Scaffold(
      resizeToAvoidBottomPadding: true,
      body: new Container(
          //height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            color: Colors.white,
            image: DecorationImage(
              image: AssetImage('images/background_main.png'),
              fit: BoxFit.cover,
            ),
          ),
          child: new ListView(
            children: <Widget>[
              new Form(
                key: _formKeyLogin,
                child: new Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(80.0),
                      child: Center(
                        child: new Image.asset("images/logo.png"),
                      ),
                    ),
                    new Row(
                      children: <Widget>[
                        new Expanded(
                          child: new Padding(
                            padding: const EdgeInsets.only(left: 40.0),
                            child: new Text(
                              "EMAIL",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.blue,
                                fontSize: 15.0,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    new Container(
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(
                          left: 30.0, right: 30.0, top: 10.0),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                      child: new Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Expanded(
                            child: new TextFormField(
                              //onChanged: (v)=>setState((){loginEmail=v;}),
                              controller: loginEmailController,
                              decoration: new InputDecoration(
                                  enabledBorder: new OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30.0),
                                    borderSide: BorderSide(
                                      color: Colors.blue,
                                      width: 1.0,
                                    ),
                                  ),
                                  prefixIcon: Icon(Icons.email),
                                  contentPadding: const EdgeInsets.only(
                                      left: 10.0, top: 10.0, bottom: 10.0),
                                  border: new OutlineInputBorder(
                                    borderSide: BorderSide(
                                      width: 1.0,
                                    ),
                                    gapPadding: 1.0,
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(30.0),
                                    ),
                                  ),
                                  filled: true,
                                  fillColor: Colors.white70),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                    new Row(
                      children: <Widget>[
                        new Expanded(
                          child: new Padding(
                            padding: const EdgeInsets.only(left: 40.0),
                            child: new Text(
                              "PASSWORD",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue,
                                  fontSize: 15.0,
                                  fontFamily: 'Montserrat'),
                            ),
                          ),
                        ),
                      ],
                    ),
                    new Container(
                      //width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(
                          left: 30.0, right: 30.0, top: 10.0),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                      child: new Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Expanded(
                            child: new TextFormField(
                              //keyboardType: TextInputType.,
                              validator: _passwordValidator,
                              obscureText: true,
                              controller: loginPasswordTextController,
                              decoration: new InputDecoration(
                                  enabledBorder: new OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30.0),
                                    borderSide: BorderSide(
                                      color: Colors.blue,
                                      width: 1.0,
                                    ),
                                  ),
                                  prefixIcon: Icon(Icons.security),
                                  contentPadding: const EdgeInsets.only(
                                      left: 10.0, top: 10.0, bottom: 10.0),
                                  border: new OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.red,
                                      width: 3.0,
                                    ),
                                    gapPadding: 1.0,
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(30.0),
                                    ),
                                  ),
                                  filled: true,
                                  fillColor: Colors.white70),
                            ),
                          ),
                        ],
                      ),
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right: 20.0),
                          child: new FlatButton(
                            child: new Text(
                              "Forgot Password?",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue,
                                  fontSize: 15.0,
                                  fontFamily: 'Montserrat'),
                              textAlign: TextAlign.end,
                            ),
                            onPressed: () => {},
                          ),
                        ),
                      ],
                    ),
                    new Container(
                      // width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(
                          left: 30.0, right: 30.0, top: 20.0),
                      alignment: Alignment.center,
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          _buildLoginButton(),
                        ],
                      ),
                    ),
                    new Container(
                      // width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(
                          left: 30.0, right: 30.0, top: 20.0),
                      alignment: Alignment.center,
                      child: Row(
                        children: <Widget>[
                          new Expanded(
                            child: new Container(
                              margin: EdgeInsets.all(8.0),
                              decoration: BoxDecoration(
                                  border: Border.all(width: 0.25)),
                            ),
                          ),
                          Text(
                            "OR CONNECT WITH",
                            style: TextStyle(
                              color: Colors.grey,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          new Expanded(
                            child: new Container(
                              margin: EdgeInsets.all(8.0),
                              decoration: BoxDecoration(
                                  border: Border.all(width: 0.25)),
                            ),
                          ),
                        ],
                      ),
                    ),
                    new Container(
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(
                          left: 30.0, right: 30.0, top: 20.0),
                      child: new Row(
                        children: <Widget>[
                          new Expanded(
                            child: new Container(
                              margin: EdgeInsets.only(right: 8.0),
                              alignment: Alignment.center,
                              child: new Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  new Expanded(
                                    child: new RaisedButton(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 15.0),
                                      shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(30.0),
                                      ),
                                      color: Color(0Xff3B5998),
                                      onPressed: () => {},
                                      child: new Container(
                                        child: new Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: <Widget>[
                                            Icon(
                                              FontAwesomeIcons.facebookF,
                                              color: Colors.white,
                                              size: 15.0,
                                            ),
                                            Text(
                                              "FACEBOOK",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          new Expanded(
                            child: new Container(
                              margin: EdgeInsets.only(left: 8.0),
                              alignment: Alignment.center,
                              child: new Row(
                                children: <Widget>[
                                  new Expanded(
                                    child: new RaisedButton(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 15.0),
                                      shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(30.0),
                                      ),
                                      color: Color(0Xffdb3236),
                                      onPressed: () => {},
                                      child: new Container(
                                        child: new Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: <Widget>[
                                            Icon(
                                              FontAwesomeIcons.google,
                                              color: Colors.white,
                                              size: 15.0,
                                            ),
                                            Text(
                                              "GOOGLE",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          )),
    );
  }

  var snackBar = SnackBar(
      backgroundColor: Colors.blue,
      content: new Text(
        "Please choose your gender!",
        style: new TextStyle(color: Colors.white),
      ));

  Widget SignupPage() {
    _setSignUpControllersListeners();
    return new Scaffold(
      key: _signUpScaffoldKey,
      body: new Container(
          decoration: BoxDecoration(
            color: Colors.white,
            image: DecorationImage(
              image: AssetImage('images/background_main.png'),
              fit: BoxFit.cover,
            ),
          ),
          child: ListView(
            children: <Widget>[
              new Form(
                key: _formKeySignUp,
                child: new Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(70.0),
                      child: Center(
                        child: Image.asset('images/logo.png'),
                      ),
                    ),
                    new Row(
                      children: <Widget>[
                        new Expanded(
                          child: new Padding(
                            padding: const EdgeInsets.only(left: 40.0),
                            child: new Text(
                              "First Name",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.blue,
                                fontSize: 15.0,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    new Container(
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(
                          left: 30.0, right: 30.0, top: 10.0, bottom: 20.0),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                      child: new Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Expanded(
                            child: new TextFormField(
                              controller: signUpFirstNameController,
                              validator: _nameValidator,
                              decoration: new InputDecoration(
                                  enabledBorder: new OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30.0),
                                    borderSide: BorderSide(
                                      color: Colors.blue,
                                      width: 1.0,
                                    ),
                                  ),
                                  prefixIcon: Icon(Icons.person),
                                  contentPadding: const EdgeInsets.only(
                                      left: 10.0, top: 10.0, bottom: 10.0),
                                  border: new OutlineInputBorder(
                                    borderSide: BorderSide(
                                      width: 1.0,
                                    ),
                                    gapPadding: 1.0,
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(30.0),
                                    ),
                                  ),
                                  filled: true,
                                  fillColor: Colors.white70),
                            ),
                          ),
                        ],
                      ),
                    ),
                    new Row(
                      children: <Widget>[
                        new Expanded(
                          child: new Padding(
                            padding: const EdgeInsets.only(left: 40.0),
                            child: new Text(
                              "Last Name",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.blue,
                                fontSize: 15.0,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    new Container(
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(
                          left: 30.0, right: 30.0, top: 10.0, bottom: 20.0),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                      child: new Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Expanded(
                            child: new TextFormField(
                              controller: signUpLastNameController,
                              validator: _nameValidator,
                              decoration: new InputDecoration(
                                  enabledBorder: new OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30.0),
                                    borderSide: BorderSide(
                                      color: Colors.blue,
                                      width: 1.0,
                                    ),
                                  ),
                                  prefixIcon: Icon(Icons.person),
                                  contentPadding: const EdgeInsets.only(
                                      left: 10.0, top: 10.0, bottom: 10.0),
                                  border: new OutlineInputBorder(
                                    borderSide: BorderSide(
                                      width: 1.0,
                                    ),
                                    gapPadding: 1.0,
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(30.0),
                                    ),
                                  ),
                                  filled: true,
                                  fillColor: Colors.white70),
                            ),
                          ),
                        ],
                      ),
                    ),
                    new Row(
                      children: <Widget>[
                        new Expanded(
                          child: new Padding(
                            padding: const EdgeInsets.only(left: 40.0),
                            child: new Text(
                              "User Name",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.blue,
                                fontSize: 15.0,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    new Container(
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(
                          left: 30.0, right: 30.0, top: 10.0, bottom: 20.0),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                      child: new Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Expanded(
                            child: new TextFormField(
                              validator: _nameValidator,
                              controller: signUpUserNameController,
                              decoration: new InputDecoration(
                                  enabledBorder: new OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30.0),
                                    borderSide: BorderSide(
                                      color: Colors.blue,
                                      width: 1.0,
                                    ),
                                  ),
                                  prefixIcon: Icon(Icons.person),
                                  contentPadding: const EdgeInsets.only(
                                      left: 10.0, top: 10.0, bottom: 10.0),
                                  border: new OutlineInputBorder(
                                    borderSide: BorderSide(
                                      width: 1.0,
                                    ),
                                    gapPadding: 1.0,
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(30.0),
                                    ),
                                  ),
                                  filled: true,
                                  fillColor: Colors.white70),
                            ),
                          ),
                        ],
                      ),
                    ),
                    new Row(
                      children: <Widget>[
                        new Expanded(
                          child: new Padding(
                            padding: const EdgeInsets.only(left: 40.0),
                            child: new Text(
                              "DOB",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.blue,
                                fontSize: 15.0,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    GestureDetector(
                      child: new Container(
                        width: MediaQuery.of(context).size.width,
                        margin: const EdgeInsets.only(
                            left: 30.0, right: 30.0, top: 10.0, bottom: 20.0),
                        alignment: Alignment.center,
                        padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                        child: new Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Expanded(
                              child: new TextFormField(
                                controller: signUpDOBController,
                                validator: _nameValidator,
                                decoration: new InputDecoration(
                                    enabledBorder: new OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                      borderSide: BorderSide(
                                        color: Colors.blue,
                                        width: 1.0,
                                      ),
                                    ),
                                    prefixIcon: Icon(Icons.calendar_today),
                                    contentPadding: const EdgeInsets.only(
                                        left: 10.0, top: 10.0, bottom: 10.0),
                                    border: new OutlineInputBorder(
                                      borderSide: BorderSide(
                                        width: 1.0,
                                      ),
                                      gapPadding: 1.0,
                                      borderRadius: const BorderRadius.all(
                                        const Radius.circular(30.0),
                                      ),
                                    ),
                                    filled: true,
                                    fillColor: Colors.white70),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    new Center(
                      child: Column(
                        children: <Widget>[
                          new Text(
                            "Gender",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blue,
                              fontSize: 18.0,
                            ),
                          ),
                          new CustomRadioButton(
                            values: <String>["Male", "Female"],
                            genderSelectionCallback: _setGender,
                          ),
                        ],
                      ),
                    ),
                    new Row(
                      children: <Widget>[
                        new Expanded(
                          child: new Padding(
                            padding: const EdgeInsets.only(left: 40.0),
                            child: new Text(
                              "Email",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.blue,
                                fontSize: 15.0,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    new Container(
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(
                          left: 30.0, right: 30.0, top: 10.0, bottom: 20.0),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                      child: new Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Expanded(
                            child: new TextFormField(
                              controller: signUpEmailController,
                              validator: _emailValidator,
                              decoration: new InputDecoration(
                                  enabledBorder: new OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30.0),
                                    borderSide: BorderSide(
                                      color: Colors.blue,
                                      width: 1.0,
                                    ),
                                  ),
                                  prefixIcon: Icon(Icons.alternate_email),
                                  contentPadding: const EdgeInsets.only(
                                      left: 10.0, top: 10.0, bottom: 10.0),
                                  border: new OutlineInputBorder(
                                    borderSide: BorderSide(
                                      width: 1.0,
                                    ),
                                    gapPadding: 1.0,
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(30.0),
                                    ),
                                  ),
                                  filled: true,
                                  fillColor: Colors.white70),
                            ),
                          ),
                        ],
                      ),
                    ),
                    new Row(
                      children: <Widget>[
                        new Expanded(
                          child: new Padding(
                            padding: const EdgeInsets.only(left: 40.0),
                            child: new Text(
                              "Country",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.blue,
                                fontSize: 15.0,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    new Container(
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(
                          left: 30.0, right: 30.0, top: 10.0, bottom: 20.0),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                      child: new Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Expanded(
                            child: new TextFormField(
                              decoration: new InputDecoration(
                                  enabledBorder: new OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30.0),
                                    borderSide: BorderSide(
                                      color: Colors.blue,
                                      width: 1.0,
                                    ),
                                  ),
                                  prefixIcon: ListTile(
                                    onTap: _openCountryPickerDialog,
                                    title: _buildDialogItem(
                                        _selectedDialogCountry),
                                  ),
                                  contentPadding: const EdgeInsets.only(
                                      left: 10.0, top: 10.0, bottom: 10.0),
                                  border: new OutlineInputBorder(
                                    borderSide: BorderSide(
                                      width: 1.0,
                                    ),
                                    gapPadding: 1.0,
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(30.0),
                                    ),
                                  ),
                                  filled: true,
                                  fillColor: Colors.white70),
                            ),
                          ),
                        ],
                      ),
                    ),
                    new Row(
                      children: <Widget>[
                        new Expanded(
                          child: new Padding(
                            padding: const EdgeInsets.only(left: 40.0),
                            child: new Text(
                              "Phone Number",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.blue,
                                fontSize: 15.0,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    new Container(
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(
                          left: 30.0, right: 30.0, top: 10.0, bottom: 20.0),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                      child: new Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Expanded(
                            child: new TextFormField(
                              controller: signUpPhoneNumberController,
                              validator: _phoneNumberValidator,
                              decoration: new InputDecoration(
                                  enabledBorder: new OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30.0),
                                    borderSide: BorderSide(
                                      color: Colors.blue,
                                      width: 1.0,
                                    ),
                                  ),
                                  prefixIcon: Padding(
                                    padding: EdgeInsets.only(
                                        left: 20.0, right: 10.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[Text(_countryCode)],
                                    ),
                                  ),
                                  contentPadding: const EdgeInsets.only(
                                      left: 10.0, top: 10.0, bottom: 10.0),
                                  border: new OutlineInputBorder(
                                    borderSide: BorderSide(
                                      width: 1.0,
                                    ),
                                    gapPadding: 1.0,
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(30.0),
                                    ),
                                  ),
                                  filled: true,
                                  fillColor: Colors.white70),
                            ),
                          ),
                        ],
                      ),
                    ),
                    new Row(
                      children: <Widget>[
                        new Expanded(
                          child: new Padding(
                            padding: const EdgeInsets.only(left: 40.0),
                            child: new Text(
                              "Password",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.blue,
                                fontSize: 15.0,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    new Container(
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(
                          left: 30.0, right: 30.0, top: 10.0, bottom: 20.0),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                      child: new Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Expanded(
                            child: new TextFormField(
                              controller: signUpPasswordController,
                              validator: _passwordValidator,
                              decoration: new InputDecoration(
                                  enabledBorder: new OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30.0),
                                    borderSide: BorderSide(
                                      color: Colors.blue,
                                      width: 1.0,
                                    ),
                                  ),
                                  prefixIcon: Icon(Icons.lock),
                                  contentPadding: const EdgeInsets.only(
                                      left: 10.0, top: 10.0, bottom: 10.0),
                                  border: new OutlineInputBorder(
                                    borderSide: BorderSide(
                                      width: 1.0,
                                    ),
                                    gapPadding: 1.0,
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(30.0),
                                    ),
                                  ),
                                  filled: true,
                                  fillColor: Colors.white70),
                            ),
                          ),
                        ],
                      ),
                    ),
                    new Row(
                      children: <Widget>[
                        new Expanded(
                          child: new Padding(
                            padding: const EdgeInsets.only(left: 40.0),
                            child: new Text(
                              "Confirm Password",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.blue,
                                fontSize: 15.0,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    new Container(
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(
                          left: 30.0, right: 30.0, top: 10.0, bottom: 20.0),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                      child: new Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Expanded(
                            child: new TextFormField(
                              controller: signUpConfirmPasswordController,
                              validator: _confirmPasswordValidator,
                              decoration: new InputDecoration(
                                  enabledBorder: new OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30.0),
                                    borderSide: BorderSide(
                                      color: Colors.blue,
                                      width: 1.0,
                                    ),
                                  ),
                                  prefixIcon: Icon(Icons.lock),
                                  contentPadding: const EdgeInsets.only(
                                      left: 10.0, top: 10.0, bottom: 10.0),
                                  border: new OutlineInputBorder(
                                    borderSide: BorderSide(
                                      width: 1.0,
                                    ),
                                    gapPadding: 1.0,
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(30.0),
                                    ),
                                  ),
                                  filled: true,
                                  fillColor: Colors.white70),
                            ),
                          ),
                        ],
                      ),
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right: 20.0),
                          child: new FlatButton(
                            child: new Text(
                              "Already have an account?",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.blue,
                                fontSize: 15.0,
                              ),
                              textAlign: TextAlign.end,
                            ),
                            onPressed: () => gotoLogin(),
                          ),
                        ),
                      ],
                    ),
                    new Container(
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(
                          left: 30.0, right: 30.0, top: 50.0),
                      alignment: Alignment.center,
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          _buildSignUpButton(),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }

  ///A callback method that is called when gender is selected
  _setGender(String gender) {
    genderString = gender;
  }

  _initSignUpDetails() {
    User user = new User();
    user.firstName = signUpFirstNameController.text;
    user.lastName = signUpLastNameController.text;
    user.userName = signUpUserNameController.text;
    user.phoneNumber = signUpPhoneNumberController.text;
    user.email = signUpEmailController.text;
    user.password = signUpPasswordController.text;
    user.country = _selectedDialogCountry.name;
    user.gender = genderString;
    return user;
  }

  _signUpValidateDetails() {
    _initSignUpDetails();
  }

  gotoLogin() {
    //controller_0To1.forward(from: 0.0);
    _controller.animateToPage(
      0,
      duration: Duration(milliseconds: 800),
      curve: Curves.bounceOut,
    );
  }

  gotoSignup() {
    //controller_minus1To0.reverse(from: 0.0);
    _controller.animateToPage(
      2,
      duration: Duration(milliseconds: 800),
      curve: Curves.bounceOut,
    );
  }

  PageController _controller =
      new PageController(initialPage: 1, viewportFraction: 1.0);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height,
//      child: new GestureDetector(
//        onHorizontalDragStart: _onHorizontalDragStart,
//        onHorizontalDragUpdate: _onHorizontalDragUpdate,
//        onHorizontalDragEnd: _onHorizontalDragEnd,
//        behavior: HitTestBehavior.translucent,
//        child: Stack(
//          children: <Widget>[
//            new FractionalTranslation(
//              translation: Offset(-1 - (scrollPercent / (1 / numCards)), 0.0),
//              child: SignupPage(),
//            ),
//            new FractionalTranslation(
//              translation: Offset(0 - (scrollPercent / (1 / numCards)), 0.0),
//              child: HomePage(),
//            ),
//            new FractionalTranslation(
//              translation: Offset(1 - (scrollPercent / (1 / numCards)), 0.0),
//              child: LoginPage(),
//            ),
//          ],
//        ),
//      ),
        child: PageView(
          controller: _controller,
          physics: new AlwaysScrollableScrollPhysics(),
          children: <Widget>[LoginPage(), HomePage(), SignupPage()],
          scrollDirection: Axis.horizontal,
        ));
  }
}
