import 'package:Gambeat/helper/SharedPreferenceHelper.dart';
import 'package:Gambeat/models/User.dart';
import 'package:flutter/material.dart';

class OverviewFragment extends StatefulWidget {
  @override
  _OverviewFragmentState createState() => _OverviewFragmentState();
}

class _OverviewFragmentState extends State<OverviewFragment> {
  SharedPreferenceHelper sharedPreference = new SharedPreferenceHelper();

  User user;

  void loadUsersDetails() async {

    User foundUser = await sharedPreference.getUser();

    user = foundUser;

    setState(() {

    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    loadUsersDetails();
  }

  @override
  Widget build(BuildContext context) {
    return Container(

      decoration: BoxDecoration(
        color: Colors.white,
        image: DecorationImage(
          colorFilter: new ColorFilter.mode(
              Colors.black.withOpacity(0.15), BlendMode.dstATop),
          image: AssetImage('images/games.jpg'),
          fit: BoxFit.cover,
        ),
      ),

      child: Center(
          child: Container(
            width: 300.0,
            child: Card(
                elevation: 8.0,
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 15.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        "YOUR BALANCE",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 15.0,
                      ),

                      Text(user == null? "N0" : "N ${user.playerStats.wallet.toString()}"),
                      SizedBox(
                        height: 20.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "View Transactions:",
                            style: TextStyle(color: Colors.grey,fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            width: 5.0,
                          ),
                          OutlineButton(
                            highlightedBorderColor: Colors.amber,

                            color: Colors.blue,
                            borderSide: BorderSide(color: Colors.blue),
                            shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0),
                            ),
                            child: Container(
                              child: Text(
                                "transactions",
                                style: TextStyle(color: Colors.blue, fontSize: 10.0),
                              ),
                            ),
                            onPressed: () {},
                          )
                        ],
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Divider(
                        color: Colors.black45,
                        height: 10.0,
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                user == null ? "N0" : "N${user.playerStats.profit.toString()}",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Text("Profit"),
                              SizedBox(
                                height: 10.0,
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(100.0),
                                ),
                                constraints: BoxConstraints(
                                  maxWidth: 100.0,
                                  maxHeight: 3.0,
                                ),
                                child: new LinearProgressIndicator(
                                  value: user == null ? 0.0 :
                                  user.playerStats.expenses == 0.0 && user.playerStats.profit + user.playerStats.expenses == 0.0 ? 0.0 :
                                  user.playerStats.profit/(user.playerStats.profit + user.playerStats.expenses),
                                  valueColor:
                                  new AlwaysStoppedAnimation<Color>(Colors.blue),
                                  backgroundColor: Colors.black12,
                                ),
                              )
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                user == null ? "N0" : "N${user.playerStats.expenses.toString()}",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Text("Expenses"),
                              SizedBox(
                                height: 10.0,
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(100.0),
                                ),
                                constraints: BoxConstraints(
                                  maxWidth: 100.0,
                                  maxHeight: 3.0,
                                ),
                                child: new LinearProgressIndicator(
                                  value: user == null ? 0.0 :
                                  user.playerStats.expenses == 0.0 && user.playerStats.profit + user.playerStats.expenses == 0.0 ? 0.0 :
                                  user.playerStats.expenses/(user.playerStats.profit + user.playerStats.expenses),
                                  valueColor:
                                  new AlwaysStoppedAnimation<Color>(Colors.red),
                                  backgroundColor: Colors.black12,
                                ),
                              )
                            ],
                          ),
                        ],
                      )
                    ],
                  ),
                )),
          )),
    );
  }
}
