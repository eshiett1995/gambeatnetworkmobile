import 'dart:async';

import 'package:Gambeat/helper/SharedPreferenceHelper.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => new _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  startTime() async {
    var _duration = new Duration(seconds: 5);

    return new Timer(_duration, navigationPage);
  }

  void navigationPage() async {
    if (await new SharedPreferenceHelper().getIsLoggedInStatus()) {
      
      Navigator.of(context).pushReplacementNamed('/home');
    } else {
      Navigator.of(context).pushReplacementNamed('/login');
    }
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new Image.asset('images/google_play.png'),
      ),
    );
  }
}
