import 'dart:convert';
import 'dart:io';

import 'package:Gambeat/fragments/social/gamer_profile.dart';
import 'package:Gambeat/models/User.dart';
import 'package:Gambeat/models/rest-models/response/UserPaginationModel.dart';
import 'package:Gambeat/network/api/ApiHelper.dart';
import 'package:flutter/material.dart';

class UsersFragment extends StatefulWidget {
  @override
  _UsersState createState() => _UsersState();
}

class _UsersState extends State<UsersFragment> {
  UserPaginationModel userPaginationModel;

  List<User> usersList = new List();

  getUsers() async {
    HttpClientResponse response = await new ApiHelper().getAllUsersByPage(1);

    String userPaginationModelResponse =
        await response.transform(utf8.decoder).join();

    var jsonString = json.decode(userPaginationModelResponse);

    if (response.statusCode == 200) {
      UserPaginationModel userPaginationModel =
          UserPaginationModel.fromJson(jsonString);

      userPaginationModel.content.forEach((user) {
        usersList.add(user);
      });

      setState(() {});
    } else {
      Scaffold.of(context).showSnackBar(new SnackBar(
        content: new Text("Error in fetching users"),
      ));
    }
  }

  @override
  void initState() {
    super.initState();
    getUsers();
  }

  Widget userCard(User user) {
    return new Container(
      padding: EdgeInsets.only(top: 20.0),
      child: new Column(
        children: <Widget>[
          new Column(
            children: <Widget>[
              new Stack(
                children: <Widget>[
                  new Container(
                    height: 100.0,
                    width: 100.0,
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black26,
                            blurRadius:
                                8.0, // has the effect of softening the shadow
                            spreadRadius:
                                2.0, // has the effect of extending the shadow
                            offset: Offset(
                              2.0, // horizontal, move right 10
                              1.0, // vertical, move down 10
                            ),
                          )
                        ],
                        borderRadius: BorderRadius.circular(62.5),
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage('images/chris.jpg'))),
                  ),
                  new Positioned(
                    top: 10.0,
                    right: 8.5,
                    child: new Material(
                        type: MaterialType.circle,
                        elevation: 2.0,
                        color: Colors.lightGreenAccent,
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                        )),
                  ),
                ],
              ),
              new SizedBox(
                height: 10.0,
              ),
              new Text(
                user.userName,
                style: TextStyle(
                    color: Colors.blue,
                    fontSize: 14.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Montserrat',
                    letterSpacing: 2.0),
              ),
              new Text(
                "120XP",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.blue,
                    fontSize: 12.0,
                    fontFamily: 'Montserrat',
                    letterSpacing: 2.0),
              )
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: RefreshIndicator(
        onRefresh: (){},
        child: new GridView.count(
          crossAxisCount: 2,
          children: new List.generate(usersList.length, (index) {
            return GestureDetector(
              child: userCard(usersList.elementAt(index)),
              onTap: () {
                Navigator.of(context).push(new MaterialPageRoute(
                    builder: (BuildContext context) =>
                        new GamerProfile(user: usersList.elementAt(index))));
              },
              onLongPress: () {},
            );
          }),
        ),
      ),
    );
  }
}
