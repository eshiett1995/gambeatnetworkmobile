
import 'dart:convert';
import 'dart:io';

import 'package:country_pickers/countries.dart';
import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_pickers.dart';
import 'package:Gambeat/custom-widgets/custom_radio_butto_for_message_sender.dart';
import 'package:Gambeat/custom-widgets/custom_radio_button_for_gender.dart';
import 'package:Gambeat/helper/SharedPreferenceHelper.dart';
import 'package:Gambeat/models/User.dart';
import 'package:Gambeat/models/rest-models/response/ResponseModel.dart';
import 'package:Gambeat/network/api/ApiHelper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sweet_alert/flutter_sweet_alert.dart';


class BasicInfo extends StatefulWidget {
  @override
  _BasicInfoState createState() => _BasicInfoState();
}

class _BasicInfoState extends State<BasicInfo> {

  bool updatingBasicInfo = false;

  String genderString = "Non";

  final _countries =
  countriesList.map((item) => Country.fromMap(item)).toList();

  Country _selectedDialogCountry = CountryPickerUtils.getCountryByIsoCode('tr');

  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController DOBController = TextEditingController();
  TextEditingController BioController = TextEditingController();

  User user;

  void showAlertDialog(AlertType alertType, String title, String content) {
    SweetAlert.dialog(
        type: alertType,
        title: title,
        content: content,
        cancelButtonText: "Okay",
        confirmButtonText: "ok");
  }


  void updateUserBasicInfo() async{

    updatingBasicInfo = true;

    setState(() {

    });

    User tempUser = user;

    tempUser.firstName = firstNameController.text;

    tempUser.lastName = lastNameController.text;

    tempUser.bio = BioController.text;

    print(genderString);

    tempUser.gender = genderString;

    HttpClientResponse basicUpdateResponse = await new ApiHelper().updateUserBasicInfo(tempUser);

    if (basicUpdateResponse.statusCode == 200) {
      String basicUpdateResponseJsonString = await basicUpdateResponse
          .transform(utf8.decoder).join();

      ResponseModel responseModel = new ResponseModel.fromJson(
          json.decode(basicUpdateResponseJsonString));

      if (responseModel.isSuccessful) {
        SharedPreferenceHelper sharedPreference = new SharedPreferenceHelper();

        user = tempUser;

        await sharedPreference.setUser(user);

        showAlertDialog(
            AlertType.SUCCESS, "Update Info", responseModel.responseMessage);
      } else {
        showAlertDialog(
            AlertType.ERROR, "Update Info", responseModel.responseMessage);
      }

      updatingBasicInfo = false;

    } else {
      showAlertDialog(
          AlertType.ERROR, "Update Info", "Ooop! connection error");
    }

    updatingBasicInfo = false;

    setState(() {

    });


  }

  void loadUsersDetails() async {



    user = await new SharedPreferenceHelper().getUser();

    firstNameController.text = user.firstName;

    lastNameController.text = user.lastName;

    DOBController.text = "3/6/1995";

    var country =_countries.firstWhere((country) => country.name.startsWith(user.country), orElse: () => null);

    _selectedDialogCountry = CountryPickerUtils.getCountryByIsoCode(country.isoCode);

    BioController.text = user.bio;

    setState(() {

    });
  }


  @override
  void initState()  {
    loadUsersDetails();
  }




  String _countryCode = "+234";

  Widget _countryFlag = Image.asset("name");

  final _formKeySignUp = GlobalKey<FormState>();

  final String emailNotValidText = 'Email is not valid';
  final String passwordNotValidText =
      'Password must not be less than 8 characters';
  final String confirmPasswordNotValidText = 'Password does not match';
  final String fieldEmptyText = "Field Must not be empty";

  String _nameValidator(String value) {
    if (value.isEmpty) return fieldEmptyText;
  }




  Widget _buildDialogItem(Country country) {
    _countryFlag = CountryPickerUtils.getDefaultFlagImage(country);

    _countryCode = "+${country.phoneCode}";

    return Row(
      children: <Widget>[
        CountryPickerUtils.getDefaultFlagImage(country),
        SizedBox(width: 8.0),
        SizedBox(width: 8.0),
        Flexible(child: Text(country.name))
      ],
    );
  }

  void _openCountryPickerDialog() => showDialog(
    context: context,
    builder: (context) => Theme(
        data: Theme.of(context).copyWith(primaryColor: Colors.pink),
        child: CountryPickerDialog(
            titlePadding: EdgeInsets.all(8.0),
            searchCursorColor: Colors.pinkAccent,
            searchInputDecoration: InputDecoration(hintText: 'Search...'),
            isSearchable: true,
            title: Text('Select your country'),
            onValuePicked: (Country country) =>
                setState(() => _selectedDialogCountry = country),
            itemBuilder: _buildDialogItem)),
  );













  Widget _buildUpdateProfileButton() {

    if (!updatingBasicInfo) {

      return new Expanded(
        child: new RaisedButton(
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(10.0),
          ),
          color: Colors.blue,
          onPressed: () {

            updateUserBasicInfo();

              setState(() {

              });

          },
          child: new Container(
            padding: const EdgeInsets.symmetric(
              vertical: 15.0,
              horizontal: 10.0,
            ),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[

                Text(
                  "SAVE CHANGES",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),

                SizedBox(width: 15.0,),

                Icon(Icons.save, color: Colors.white,),
              ],
            ),
          ),
        ),
      );

    }else{

      return Container(
        height: 48.0,
        width: 48.0,
        child: RaisedButton(
          padding: EdgeInsets.all(0.0),
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0),
          ),
          child: SizedBox(
            height: 30.0,
            width: 30.0,
            child: Stack(
              children: <Widget>[
                CircularProgressIndicator(
                  value: null,
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                ),
                Positioned(
                    child: Icon(Icons.save, size: 18.0, color: Colors.white,),
                  left: 0.0,
                  right: 0.0,
                  top: 0.0,
                  bottom: 0.0,
                )
              ],
            )
          ),
          disabledColor: Colors.blue,
          onPressed: null,
        ),
      );
      
    }
    
  }













  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          image: DecorationImage(
            image: AssetImage('images/background_main.png'),
            fit: BoxFit.cover,
          ),
        ),

      child: ListView(
        children: <Widget>[
          new Container(
            margin: EdgeInsets.only(bottom: 20.0),

            child: new Form(
              key: _formKeySignUp,
              child: new Column(
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    padding: EdgeInsets.only(top:50.0, bottom: 50.0, left: 40.0),

                      child: Text("Basic Info",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: Colors.blue,
                        fontSize: 50.0
                      )
                    ),
                  ),
                  new Row(
                    children: <Widget>[
                      new Expanded(
                        child: new Padding(
                          padding: const EdgeInsets.only(left: 40.0),
                          child: new Text(
                            "First Name",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blue,
                              fontSize: 15.0,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  new Container(
                    width: MediaQuery.of(context).size.width,
                    margin: const EdgeInsets.only(
                        left: 30.0, right: 30.0, top: 10.0, bottom: 20.0),
                    alignment: Alignment.center,
                    padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                    child: new Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        new Expanded(
                          child: new TextFormField(
                            controller: firstNameController,
                            validator: _nameValidator,
                            decoration: new InputDecoration(
                                enabledBorder: new OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: BorderSide(
                                    color: Colors.blue,
                                    width: 1.0,
                                  ),
                                ),
                                prefixIcon: Icon(Icons.person),
                                contentPadding: const EdgeInsets.only(
                                    left: 10.0, top: 10.0, bottom: 10.0),
                                border: new OutlineInputBorder(
                                  borderSide: BorderSide(
                                    width: 1.0,
                                  ),
                                  gapPadding: 1.0,
                                  borderRadius: const BorderRadius.all(
                                    const Radius.circular(10.0),
                                  ),
                                ),
                                filled: true,
                                fillColor: Colors.white70),
                          ),
                        ),
                      ],
                    ),
                  ),
                  new Row(
                    children: <Widget>[
                      new Expanded(
                        child: new Padding(
                          padding: const EdgeInsets.only(left: 40.0),
                          child: new Text(
                            "Last Name",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blue,
                              fontSize: 15.0,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  new Container(
                    width: MediaQuery.of(context).size.width,
                    margin: const EdgeInsets.only(
                        left: 30.0, right: 30.0, top: 10.0, bottom: 20.0),
                    alignment: Alignment.center,
                    padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                    child: new Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        new Expanded(
                          child: new TextFormField(
                            controller: lastNameController,
                            validator: _nameValidator,
                            decoration: new InputDecoration(
                                enabledBorder: new OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: BorderSide(
                                    color: Colors.blue,
                                    width: 1.0,
                                  ),
                                ),
                                prefixIcon: Icon(Icons.person),
                                contentPadding: const EdgeInsets.only(
                                    left: 10.0, top: 10.0, bottom: 10.0),
                                border: new OutlineInputBorder(
                                  borderSide: BorderSide(
                                    width: 1.0,
                                  ),
                                  gapPadding: 1.0,
                                  borderRadius: const BorderRadius.all(
                                    const Radius.circular(10.0),
                                  ),
                                ),
                                filled: true,
                                fillColor: Colors.white70),
                          ),
                        ),
                      ],
                    ),
                  ),

                  new Row(
                    children: <Widget>[
                      new Expanded(
                        child: new Padding(
                          padding: const EdgeInsets.only(left: 40.0),
                          child: new Text(
                            "DOB",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blue,
                              fontSize: 15.0,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  GestureDetector(
                    child: new Container(
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(
                          left: 30.0, right: 30.0, top: 10.0, bottom: 20.0),
                      alignment: Alignment.center,
                      padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                      child: new Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Expanded(
                            child: new TextFormField(
                              controller: DOBController,
                              validator: _nameValidator,
                              decoration: new InputDecoration(
                                  enabledBorder: new OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                    borderSide: BorderSide(
                                      color: Colors.blue,
                                      width: 1.0,
                                    ),
                                  ),
                                  prefixIcon: Icon(Icons.calendar_today),
                                  contentPadding: const EdgeInsets.only(
                                      left: 10.0, top: 10.0, bottom: 10.0),
                                  border: new OutlineInputBorder(
                                    borderSide: BorderSide(
                                      width: 1.0,
                                    ),
                                    gapPadding: 1.0,
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(10.0),
                                    ),
                                  ),
                                  filled: true,
                                  fillColor: Colors.white70),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  new Row(
                    children: <Widget>[
                      new Expanded(
                        child: new Padding(
                          padding: const EdgeInsets.only(left: 40.0),
                          child: new Text(
                            "Country",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blue,
                              fontSize: 15.0,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  new Container(
                    width: MediaQuery.of(context).size.width,
                    margin: const EdgeInsets.only(
                        left: 30.0, right: 30.0, top: 10.0, bottom: 20.0),
                    alignment: Alignment.center,
                    padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                    child: new Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        new Expanded(
                          child: new TextFormField(
                            decoration: new InputDecoration(
                                enabledBorder: new OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: BorderSide(
                                    color: Colors.blue,
                                    width: 1.0,
                                  ),
                                ),
                                prefixIcon: ListTile(
                                  onTap: _openCountryPickerDialog,
                                  title: _buildDialogItem(_selectedDialogCountry),
                                ),
                                contentPadding: const EdgeInsets.only(
                                    left: 10.0, top: 10.0, bottom: 10.0),
                                border: new OutlineInputBorder(
                                  borderSide: BorderSide(
                                    width: 1.0,
                                  ),
                                  gapPadding: 1.0,
                                  borderRadius: const BorderRadius.all(
                                    const Radius.circular(10.0),
                                  ),
                                ),
                                filled: true,
                                fillColor: Colors.white70),
                          ),
                        ),
                      ],
                    ),
                  ),

                  new Row(
                    children: <Widget>[
                      new Expanded(
                        child: new Padding(
                          padding: const EdgeInsets.only(left: 40.0),
                          child: new Text(
                            "Gender",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blue,
                              fontSize: 15.0,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),


                  new CustomRadioButton(
                    radioVal: "Non",
                    values: <String>["Male", "Female"],
                    genderSelectionCallback: (gender){

                      genderString = gender;

                    },
                  ),


                  new Row(
                    children: <Widget>[
                      new Expanded(
                        child: new Padding(
                          padding: const EdgeInsets.only(left: 40.0),
                          child: new Text(
                            "Bio",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.blue,
                              fontSize: 15.0,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  new Container(
                    width: MediaQuery.of(context).size.width,
                    margin: const EdgeInsets.only(
                        left: 30.0, right: 30.0, top: 10.0, bottom: 20.0),
                    alignment: Alignment.center,
                    padding: const EdgeInsets.only(left: 0.0, right: 0.0),
                    child: new Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        new Expanded(
                          child: new TextFormField(
                            keyboardType: TextInputType.multiline,
                            maxLines: 5,
                            controller: BioController,
                            validator: _nameValidator,
                            decoration: new InputDecoration(
                                enabledBorder: new OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: BorderSide(
                                    color: Colors.blue,
                                    width: 1.0,
                                  ),
                                ),

                                contentPadding: const EdgeInsets.only(
                                    left: 10.0, top: 10.0, bottom: 10.0),
                                border: new OutlineInputBorder(
                                  borderSide: BorderSide(
                                    width: 1.0,
                                  ),
                                  gapPadding: 1.0,
                                  borderRadius: const BorderRadius.all(
                                    const Radius.circular(10.0),
                                  ),
                                ),
                                filled: true,
                                fillColor: Colors.white70),
                          ),
                        ),
                      ],
                    ),
                  ),



                  new Container(
                    width: MediaQuery.of(context).size.width,
                    margin:
                    const EdgeInsets.only(left: 30.0, right: 30.0, top: 30.0),
                    alignment: Alignment.center,
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        _buildUpdateProfileButton(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      )
    );
  }
}
