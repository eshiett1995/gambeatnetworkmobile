

import 'package:Gambeat/models/DefaultEntity.dart';

class Platform extends DefaultEntity{

  Platform();

  String name;

  factory Platform.fromJson(Map<String, dynamic> parsedJson) {

    Platform platform = new Platform();

    platform.id = parsedJson['_id'];

    platform.name = parsedJson['name'];

    return platform;

  }


  Map<String, dynamic> toJson() => {

    '"_id"': '"$id"',

    '"name"': '"$name"',

  };

}