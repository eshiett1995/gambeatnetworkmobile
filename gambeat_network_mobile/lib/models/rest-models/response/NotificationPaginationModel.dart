import 'package:Gambeat/models/GeneralNotification.dart';
import 'package:Gambeat/models/User.dart';

class NotificationPaginationModel{

  NotificationPaginationModel();

  int totalElements;

  int totalPage;

  bool last;

  int size;

  int number;

  String sort;

  bool first;

  int numberOfElements;

  List<GeneralNotification> content;

  factory NotificationPaginationModel.fromJson(Map<String, dynamic> parsedJson) {

    NotificationPaginationModel notificationPaginationModel = new NotificationPaginationModel();

    notificationPaginationModel.totalElements = parsedJson['totalElements'];

    notificationPaginationModel.totalPage = parsedJson['totalPages'];

    notificationPaginationModel.last = parsedJson['last'];

    notificationPaginationModel.size = parsedJson['size'];

    notificationPaginationModel.number = int.tryParse(parsedJson['number']);

    notificationPaginationModel.sort = parsedJson['sort'];

    notificationPaginationModel.first = parsedJson['first'];

    notificationPaginationModel.numberOfElements = parsedJson['numberOfElements'];

    var list = parsedJson['content'] as List;

    notificationPaginationModel.content = list.map((i) => GeneralNotification.fromJson(i)).toList();

    return notificationPaginationModel;
  }

}