import 'package:Gambeat/models/rest-models/response/FriendsAndMessagesPaginationModel.dart';
import 'package:Gambeat/models/rest-models/response/ResponseModel.dart';

class MessageModel {
  MessageModel();

  int totalNumberOfUnreadMessages;

  FriendsAndMessagesPaginationModel friendsAndMessagesModelPage;

  ResponseModel responseModel;

  factory MessageModel.fromJson(Map<String, dynamic> parsedJson) {
    MessageModel messageModel = new MessageModel();

    messageModel.totalNumberOfUnreadMessages =
        parsedJson['totalNumberOfUnreadMessages'];

    messageModel.friendsAndMessagesModelPage =
        FriendsAndMessagesPaginationModel.fromJson(
            parsedJson['friendsAndMessagesModelPage']);

    messageModel.responseModel =
        ResponseModel.fromJson(parsedJson['responseModel']);

    return messageModel;
  }
}
