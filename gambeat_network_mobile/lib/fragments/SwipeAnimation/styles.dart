import 'package:flutter/material.dart';

DecorationImage image1 = new DecorationImage(
  image: new ExactAssetImage('images/chris.jpg'),
  fit: BoxFit.cover,
);
DecorationImage image2 = new DecorationImage(
  image: new ExactAssetImage('images/sms.png'),
  fit: BoxFit.cover,
);

DecorationImage image3 = new DecorationImage(
  image: new ExactAssetImage('images/versus.png'),
  fit: BoxFit.cover,
);
DecorationImage image4 = new DecorationImage(
  image: new ExactAssetImage('images/competition-started.png'),
  fit: BoxFit.cover,
);
DecorationImage image5 = new DecorationImage(
  image: new ExactAssetImage('images/email.png'),
  fit: BoxFit.cover,
);

ImageProvider avatar1 = new ExactAssetImage('assets/avatars/avatar-1.jpg');
ImageProvider avatar2 = new ExactAssetImage('assets/avatars/avatar-2.jpg');
ImageProvider avatar3 = new ExactAssetImage('assets/avatars/avatar-3.jpg');
ImageProvider avatar4 = new ExactAssetImage('assets/avatars/avatar-4.jpg');
ImageProvider avatar5 = new ExactAssetImage('assets/avatars/avatar-5.jpg');
ImageProvider avatar6 = new ExactAssetImage('assets/avatars/avatar-6.jpg');
