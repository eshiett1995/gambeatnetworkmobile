
import 'package:Gambeat/models/DefaultEntity.dart';
import 'package:Gambeat/models/Platform.dart';

class DownloadLink{

  DownloadLink();

  Platform platform;

  String url;

  factory DownloadLink.fromJson(Map<String, dynamic> parsedJson) {

    DownloadLink downloadLink = new DownloadLink();

    downloadLink.platform = Platform.fromJson(parsedJson['platform']);

    downloadLink.url = parsedJson['url'];

    return downloadLink;
  }


  Map<String, dynamic> toJson() => {

    '"platform"': platform == null ? null : platform.toJson(),

    '"url"': '"$url"',

  };

}