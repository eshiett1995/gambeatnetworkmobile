import 'package:Gambeat/models/Game.dart';

class GamePageModel{

  GamePageModel();

  int totalElements;

  int totalPage;

  bool last;

  int size;

  int number;

  String sort;

  bool first;

  int numberOfElements;

  List<Game> content;

  factory GamePageModel.fromJson(Map<String, dynamic> parsedJson) {

    GamePageModel gamePageModel = new GamePageModel();

    gamePageModel.totalElements = parsedJson['totalElements'];

    gamePageModel.totalPage = parsedJson['totalPage'];

    gamePageModel.last = parsedJson['last'];

    gamePageModel.size = parsedJson['size'];

    gamePageModel.number = int.tryParse(parsedJson['number']);

    gamePageModel.sort = parsedJson['sort'];

    gamePageModel.first = parsedJson['first'];

    gamePageModel.numberOfElements = parsedJson['numberOfElements'];

    var list = parsedJson['content'] as List;

    gamePageModel.content = list.map((i) => Game.fromJson(i)).toList();

    return gamePageModel;
  }

}