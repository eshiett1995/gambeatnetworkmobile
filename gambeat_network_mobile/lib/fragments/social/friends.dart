import 'dart:convert';
import 'dart:io';

import 'package:Gambeat/fragments/social/gamer_profile.dart';
import 'package:Gambeat/models/User.dart';
import 'package:Gambeat/models/rest-models/response/UserPaginationModel.dart';
import 'package:Gambeat/network/api/ApiHelper.dart';
import 'package:flutter/material.dart';
import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

class Friends extends StatefulWidget {
  var homePageState;

  Friends({this.homePageState});

  @override
  State<StatefulWidget> createState() {
    return new _Friends();
  }
}

class _Friends extends State<Friends> {
  List<User> friendList = [];

  SqfliteAdapter _adapter;
  int currentPageNumber = 0;
  bool isLastPage = false;
  int endOfPageCalled;
  int currentLength = 0;
  bool _isInitialListLoading = false;
  int totalNumberOfPages = 0;

  bool _isSubsequentListLoading = false;

  bool _getFriendsFromServerCalled = false;

  var _isCrossCheckListMethodCalled = 0;

  void _lazyLoading() {
    endOfPageCalled = endOfPageCalled + 1;
    print("===========================================================");
    print("LazyMethod Called!! --- end of Page");
    print("===========================================================");
    if (!(endOfPageCalled > 1)) //This is so to prevent multiple calling
    if (!(currentPageNumber >= totalNumberOfPages)) {
      print(
          "currentPageNumber NOT greater than or equal to totalNumberOfPages -- ");
      currentPageNumber = currentPageNumber + 1;
      _loadMoreFriendsFromServer(
          pageNum: currentPageNumber, calledForDbCrossCheck: false);
    }
  }

  ///-------------------------------------------------------------------------
  ///This method sets state setting _initialListLoading bool var to false
  ///so as to control the progressbar indicating initial list items are being
  ///fetched from the api
  ///-------------------------------------------------------------------------
  _setInitialListLoadingFalse() {
    setState(() {
      _isInitialListLoading = false;
    });
  }

  ///This method sets state setting _initialListLoading bool var to true
  ///so as to control the progressbar indicating initial list items are being
  ///fetched from the api
  _setInitialListLoadingTrue() {
    setState(() {
      _isInitialListLoading = true;
    });
  }


  ///This method handles checking the list from server and comparing with the list
  ///from database and adding to the list in the database if any item does not exist
  _crossCheckListFromDbWithListFromServer() async {
    /** print("crossCHeck called ---- ");
    final userBean = UserBean(_adapter);
    print("totalNumber of Pages -- " + totalNumberOfPages.toString());
    //When it gets the totalNumberOfPages it loops through it getting the friends
    //from other pages
    for (int i = 1; i < totalNumberOfPages; i++) {
      print("forLoop entered count --- " + i.toString());
      //This line of code is getting the list from server and adding it to the list meant for
      //database crosscheck
      _loadMoreFriendsFromServer(pageNum: i, calledForDbCrossCheck: true);
    }
    print(friendsDbCrossCheckList.toString() + "Crosscheck list -- ");
    friendsDbCrossCheckList.forEach((user) async {
      UserDAO userDAO = await userBean.find(user.id);
      if (userDAO == null) {
        setState(() {
          friendList.add(user);
          print("user added to friend list ---- ");
        });
        userBean.insert(user);
        print("user not in database ------ ");
      } else
        print("user model already in database ---- ");
    });  **/
  }

  List<User> friendsDbCrossCheckList = [];

  void getFriendsFromServer() async {
    _getFriendsFromServerCalled = true;
    print("got ete");

    ApiHelper apiHelper = new ApiHelper();

    HttpClientResponse c = await apiHelper.getFriendsByPage(1);

    //var mm = json.decode(response.body);

    String mm = await c.transform(utf8.decoder).join();

    print("this is mm");

    print(mm);

    var jsonString = json.decode(mm);

    if (c.statusCode == 200) {
      print("\n*********************************************");
      print("StatusCode == 200\n");
      _setInitialListLoadingFalse();
      UserPaginationModel userPaginationModel =
          UserPaginationModel.fromJson(jsonString);

      totalNumberOfPages = userPaginationModel.totalPage;
      setState(() {
        userPaginationModel.content.forEach((user) {
          user.isFriend = "ACCEPTED";
          friendList.add(user);
        });
      });

      //final userBean = UserBean(_adapter);

      //userBean.insertMany(friendList);
    } else {
      print("\n*********************************************");
      print("StatusCode != 200\n");
      _setInitialListLoadingFalse();
    }
  }

  ///This is the method that handles loading more items from the server
  ///passing in the page number... but if this method is called to crosscheck the db list
  ///then the true is passed as the bool for calledForDbCrossCheck param
  void _loadMoreFriendsFromServer(
      {int pageNum, @required bool calledForDbCrossCheck}) async {
    if (!calledForDbCrossCheck) _setSubsequentListLoadingTrue();
    print("got ete");

    ApiHelper apiHelper = new ApiHelper();

    HttpClientResponse c = await apiHelper.getFriendsByPage(pageNum);

    //var mm = json.decode(response.body);

    String mm = await c.transform(utf8.decoder).join();

    var jsonString = json.decode(mm);

    if (c.statusCode == 200) {
      UserPaginationModel userPaginationModel =
          UserPaginationModel.fromJson(jsonString);

      ///if this method is called to cross check list items in db and add missing ones
      ///den it should skip the normal task for loading more data and go to task to
      ///perform for crosschecking items in db
      if (!calledForDbCrossCheck) {
        setState(() {
          endOfPageCalled = 0;
          userPaginationModel.content.forEach((user) {
            user.isFriend = "ACCEPTED";
            friendList.add(user);
          });
        });
        _setSubsequentListLoadingFalse();


      } else {
        totalNumberOfPages = userPaginationModel.totalPage;
        setState(() {
          userPaginationModel.content.forEach((user) {
            user.isFriend = "ACCEPTED";
            friendsDbCrossCheckList.add(user);
          });
          //_isCrossCheckListMethodCalled field ------
          // This is to prevent this method from calling multiple times
          //since the method itself calls the _loadMoreFriendsFromServer() in a loop
          _isCrossCheckListMethodCalled = _isCrossCheckListMethodCalled + 1;
          if (!(_isCrossCheckListMethodCalled > 1))
            _crossCheckListFromDbWithListFromServer();
        });
      }
    } else{ //TODO: Display a toast or snackbar that getting the friends from server failed
      _setSubsequentListLoadingFalse();
      Scaffold.of(context).showSnackBar(new SnackBar(content: new Text("Error in getting friend list"),));
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.homePageState.initSearchCallback((String searchText) {

    });

  }

  int searchCalled = 0;

  ///This method takes care of searching through the database for the required text


  @override
  Widget build(BuildContext context) {
    void friendPanelTapped(User selectedUser) {
      showModalBottomSheet(
          context: context,
          builder: (builder) {
            return new Container(
              color: Colors.white,
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    height: 20.0,
                  ),
                  Text("SELECT AN OPTION:"),
                  ListTile(
                    leading: Icon(Icons.account_circle),
                    title: Text("Profile"),
                    onTap: () {
                      var route = new MaterialPageRoute(
                        builder: (BuildContext context) =>
                            new GamerProfile(user: selectedUser),
                      );

                      Navigator.of(context).push(route);
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.message),
                    title: Text("Message"),
                  ),
                  ListTile(
                    leading: Icon(Icons.reply),
                    title: Text("Unfollow"),
                  ),
                ],
              ),
            );
          });
    }

    return new Container(
      child: RefreshIndicator(
        onRefresh: (){
          print("not refreshing");
        },
        child: new Column(
          children: <Widget>[
            new Expanded(
                child: _isInitialListLoading
                    ? Container(
//                color:Colors.blueAccent,
                  height: MediaQuery
                      .of(context)
                      .size
                      .height,

                  child: Center(child: CircularProgressIndicator()),
                )
                    : LazyLoadScrollView(
                  child: ListView.builder(
                      physics: const AlwaysScrollableScrollPhysics(),
                      itemCount: friendList == null ? 0 : friendList.length,
                      itemBuilder: (BuildContext context, int index) {
                        return new GestureDetector(
                          onTap: () {
                            friendPanelTapped(friendList[index]);
                          },
                          child: Card(
                            margin: EdgeInsets.symmetric(
                                vertical: 15.0, horizontal: 10.0),
                            elevation: 3.0,
                            child: Padding(
                                padding: EdgeInsets.all(15.0),
                                child: Row(
                                  children: <Widget>[
                                    new CircleAvatar(
                                      radius: 35.0,
                                      backgroundImage:
                                      AssetImage('images/chris.jpg'),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(right: 10.0),
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          "${friendList[index].firstName} ${friendList[index].lastName}",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 5.0,
                                        ),
                                        Text(
                                          friendList[index].userName,
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              color: Colors.black54),
                                        ),
                                        SizedBox(
                                          height: 5.0,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.start,
                                          children: <Widget>[
                                            Icon(
                                              Icons.location_on,
                                              color: Colors.blue,
                                            ),
                                            Text(
                                              friendList[index].country,
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                  color: Colors.black54),
                                            ),
                                          ],
                                        )
                                      ],
                                    )
                                  ],
                                )),
                          ),
                        );
                      }),
                  onEndOfPage:
                  _getFriendsFromServerCalled ? _lazyLoading : () {},
                )),
            _isSubsequentListLoading ? _getBottomProgressBar() : Center()
          ],
        ),
      ),
    );
  }

  void _setSubsequentListLoadingTrue() {
    setState(() {
      _isSubsequentListLoading = true;
    });
  }

  void _setSubsequentListLoadingFalse() {
    setState(() {
      _isSubsequentListLoading = false;
    });
  }

  Widget _getBottomProgressBar() {
    return new LinearProgressIndicator();
  }
}
