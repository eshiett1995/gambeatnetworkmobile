import 'package:Gambeat/models/Transactions.dart';

class TransactionPaginationModel {

TransactionPaginationModel();

  int totalElements;

  int totalPage;

  bool last;

  int size;

  int number;

  String sort;

  bool first;

  int numberOfElements;

  List<Transactions> content;

  factory TransactionPaginationModel.fromJson(Map<String, dynamic> parsedJson) {

    TransactionPaginationModel transactionsPaginationModel = new TransactionPaginationModel();

    transactionsPaginationModel.totalElements = parsedJson['totalElements'];

    transactionsPaginationModel.totalPage = parsedJson['totalPage'];

    transactionsPaginationModel.last = parsedJson['last'];

    transactionsPaginationModel.size = parsedJson['size'];

    transactionsPaginationModel.number = parsedJson['number'];

    transactionsPaginationModel.sort = parsedJson['sort'];

    transactionsPaginationModel.first = parsedJson['first'];

    transactionsPaginationModel.numberOfElements = parsedJson['numberOfElements'];

    var list = parsedJson['content'] as List;

    transactionsPaginationModel.content = list.map((i) => Transactions.fromJson(i)).toList();

    return transactionsPaginationModel;
  }


}