

import 'package:Gambeat/models/DefaultEntity.dart';

class Genre extends DefaultEntity{

  Genre();

  String name;

  factory Genre.fromJson(Map<String, dynamic> parsedJson) {
    Genre platform = new Genre();

    platform.id = parsedJson['_id'];

    platform.name = parsedJson['name'];

    return platform;
  }


  Map<String, dynamic> toJson() => {

    '"_id"': '"$id"',

    '"name"': '"$name"',

  };

}