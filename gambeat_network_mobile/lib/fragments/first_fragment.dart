import 'package:flutter/material.dart';

import '../FirstPage.dart' as first;
import '../SecondPage.dart' as second;
import '../ThirdPage.dart' as third;


class FirstFragment extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
   return new _FirstFragment();
  }
  
}

class _FirstFragment extends State<FirstFragment> with SingleTickerProviderStateMixin {

  TabController controller;

  @override
  void initState() {
    super.initState();
    controller = new TabController(vsync: this, length: 3);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
    
      bottomNavigationBar: new Material(
        color: Colors.deepOrange,
        child: new TabBar(
          controller: controller,
          tabs: <Tab>[
            new Tab(icon: new Icon(Icons.perm_identity)),
            new Tab(icon: new Icon(Icons.arrow_downward)),
            new Tab(icon: new Icon(Icons.arrow_back)),
          ]
        )
      ),
      body: new TabBarView(
        controller: controller,
        children: <Widget>[
          
          new second.Second(),
          new third.Third()
        ]
      )
    );
  }

}