import 'package:Gambeat/Callbacks.dart';
import 'package:flutter/material.dart';

class SelectPriceCutomRadioButton extends StatefulWidget {
  @override
  SelectPriceCutomRadioButtonState createState() =>
      SelectPriceCutomRadioButtonState();
}

class SelectPriceCutomRadioButtonState
    extends State<SelectPriceCutomRadioButton> {
  List<RadioModel> radioButtonItems = [
    new RadioModel(false, "₦ 500"),
    new RadioModel(false, "₦ 1000")
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 32.0),
      child: ListView.builder(
        itemCount: radioButtonItems.length,
        itemBuilder: (BuildContext content, int index) {
          return new InkWell(
            onTap: () {
              print("Taaapped ooh ");
              setState(() {
                radioButtonItems
                    .forEach((element) => element.isSelected = false);
                radioButtonItems[index].isSelected = true;
//                _onCompetitionPriceButtonClicked(
//                    radioButtonItems[index],
//                    radioButtonItems[index].buttonText == "#500" ? 0 : 1,
//                    radioButtonItems[index].isSelected);
              });
            },
            child: new RadioItem(radioButtonItems[index]),
          );
        },
      ),
    );
  }
}

class RadioItem extends StatelessWidget {
  final RadioModel radioModel;

  RadioItem(this.radioModel);

  @override
  Widget build(BuildContext context) {
    print("IsPressed ----- " + radioModel.isSelected.toString());
    return Container(
      height: 50.0,
      margin: EdgeInsets.only(left: 32.0, right: 32.0, top: 4.0, bottom: 16.0),
      padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 4.0, bottom: 4.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
          color:
              radioModel.isSelected ? Colors.orangeAccent : Colors.blueAccent,
          boxShadow: <BoxShadow>[
            new BoxShadow(
              color: Colors.black,
              blurRadius: radioModel.isSelected ? 3.0 : 20.0,
            )
          ]),
      child: new Center(
        child: new Text(radioModel.buttonText,
            style: new TextStyle(color: Colors.white, fontSize: 24.0,)),
      ),
    );
  }

  _onPress() {}
}

class RadioModel {
  bool isSelected;
  final String buttonText;

  RadioModel(this.isSelected, this.buttonText);
}
