import 'dart:async';

import 'package:Gambeat/models/Message.dart';
import 'package:Gambeat/models/socketIO-models/SocketIOLogin.dart';
import 'package:flutter/services.dart';
import 'package:Gambeat/models/Enum.dart';

class MethodChannelUtility {
  static const MethodChannel _channel = const MethodChannel('sexy');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<bool> connectToChatRelayServer()async{
    bool isSuccessful;
    try {
      isSuccessful = await _channel.invokeMethod('connectToChatRelayServer');
    }catch(e){
      print(e);
    }

    return isSuccessful;
  }

  static Future<bool>  sendMessage(Message message) async {
    print("sendMessage --- method_channel_utility");
    print(message.toJson());
    final bool isSuccessful = await _channel.invokeMethod('sendingMessage', message.toJson());
    return isSuccessful;
  }

  static Future<bool>  readMessage(Message message) async {
    var message1 = new Message();
    message1.id = message.id;
    message1.isRead = true;
    message1.message = message.message;
    message1.messageStatus = MessageStatus.READ;
    message1.receiver = message.receiver;
    message1.sender = message.sender;
    message1.dateCreated = message.dateCreated;
    // message1.isRead = true;
    final bool isSuccessful = await _channel.invokeMethod('readMessage', message1.toJson());

    return isSuccessful;

  }


  static Future<bool>  deliveredMessage(Message message) async {
var message1 = new Message();
    message1.id = message.id;
    message1.isRead = message.isRead;
    message1.message = message.message;
    message1.messageStatus = MessageStatus.DELIVERED;
    message1.receiver = message.receiver;
    message1.sender = message.sender;
    message1.dateCreated = message.dateCreated;

    final bool isSuccessful = await _channel.invokeMethod('deliveredMessage', message1.toJson());

    return isSuccessful;

  }

  static Future<bool>  loginToSocketIOServer(SocketIOLogin login) async {

    final bool isSuccessful = await _channel.invokeMethod('joinChatServer', login.toJson());

    return isSuccessful;

  }

  static void nativePlatformPortal(Future<dynamic> callback(MethodCall call)) =>
      _channel.setMethodCallHandler(callback);
}