class ResponseModel {
  ResponseModel();
  ResponseModel.make(
      this.jwtToken, this.sessionId, this.isSuccessful, this.responseMessage);

  String jwtToken;

  String sessionId;

  bool isSuccessful;

  String responseMessage;

  factory ResponseModel.fromJson(Map<String, dynamic> parsedJson) {
    ResponseModel responseModel = new ResponseModel();

    responseModel.jwtToken = parsedJson['jwtToken'];

    responseModel.sessionId = parsedJson['sessionId'];

    responseModel.isSuccessful = parsedJson['isSuccessful'];

    responseModel.responseMessage = parsedJson['responseMessage'];

    return responseModel;
  }
}
