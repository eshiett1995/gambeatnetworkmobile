import 'package:Gambeat/models/DefaultEntity.dart';
import 'package:Gambeat/models/Enum.dart';

class TemporaryStore {

  TemporaryStore();

  String id;

  DateTime dateCreated;

   String userName;

   String token;

   RequestedUpdate requestedUpdate;

   String value;

   String confirmValue;

   String presentValue;

   MessageSender sendVia;

   int attempts;

   factory TemporaryStore.fromJson(Map<String, dynamic> parsedJson) {
     TemporaryStore temporaryStore = new TemporaryStore();

     temporaryStore.id = parsedJson['id'];

     temporaryStore.dateCreated = DateTime.fromMicrosecondsSinceEpoch(parsedJson['dateCreated']);

     temporaryStore.userName = parsedJson['userName'];

     temporaryStore.token = parsedJson['userName'];

     temporaryStore.requestedUpdate = getRequestedUpdate(parsedJson['requestedUpdate']);

     temporaryStore.value = parsedJson['value'];

     temporaryStore.confirmValue = parsedJson['confirmValue'];

     temporaryStore.presentValue = parsedJson['presentValue'];

     temporaryStore.sendVia = getSendVia(parsedJson['sendVia']);

     temporaryStore.attempts = parsedJson['attempts'];

     return temporaryStore;
   }


  static RequestedUpdate getRequestedUpdate(String messageStat) {
    switch (messageStat) {
      case "EMAIL":
        return RequestedUpdate.EMAIL;
      case "PASSWORD":
        return RequestedUpdate.PASSWORD;
      case "PHONENUMBER":
        return RequestedUpdate.PHONENUMBER;
      case "USERNAME":
        return RequestedUpdate.USERNAME;
      default:
        return RequestedUpdate.EMAIL;
    }
  }

  static String getRequestedUpdateAsString(RequestedUpdate requestedUpdate) {
    switch (requestedUpdate) {
      case RequestedUpdate.EMAIL:
        return "EMAIL";
      case RequestedUpdate.PASSWORD:
        return "PASSWORD";
      case RequestedUpdate.PHONENUMBER:
        return "PHONENUMBER";
      case RequestedUpdate.USERNAME:
        return "USERNAME";
      default:
        return "EMAIL";
    }
  }

  static MessageSender getSendVia(String messageStat) {
    switch (messageStat) {
      case "EMAIL":
        return MessageSender.EMAIL;
      case "SMS":
        return MessageSender.SMS;
      default:
        return MessageSender.EMAIL;
    }
  }

  static String getSendViaAsString(MessageSender sendVia) {
    switch (sendVia) {
      case MessageSender.EMAIL:
        return "EMAIL";
      case MessageSender.SMS:
        return "SMS";
      default:
        return "EMAIL";
    }
  }


  Map<String, dynamic> toJson() => {

    '"id"': '"$id"',

    '"dateCreated"': dateCreated,

    '"userName"': '"$userName"',

    '"token"': '"$token"',

    '"requestedUpdate"': '"${getRequestedUpdateAsString(requestedUpdate)}"',

    '"value"': '"$value"',

    '"confirmValue"': '"$confirmValue"',

    '"presentValue"': '"$presentValue"',

    '"sendVia"': '"${getSendViaAsString(sendVia)}"',

    '"attempts"': '"$attempts"',
  };


}
