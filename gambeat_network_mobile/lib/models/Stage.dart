
//08106080688
import 'package:Gambeat/models/DefaultEntity.dart';
import 'package:Gambeat/models/MatchPair.dart';

class Stage {

  Stage();

  int round;

  List<MatchPair> pairs = new List();

  String immunity;

  factory Stage.fromJson(Map<String, dynamic> parsedJson) {

    Stage stage = new Stage();

    stage.round = parsedJson['round'];

    stage.pairs = jsonToListMatchPair(parsedJson);

    stage.immunity = parsedJson['immunity'];

    return stage;
  }

  static List<MatchPair> jsonToListMatchPair(Map<String, dynamic> parsedJson){

    var list = parsedJson['pairs'] as List;

    print(list.runtimeType);

    List<MatchPair> punterList = list.map((i) => MatchPair.fromJson(i)).toList();

    return punterList;
  }


  Map<String, dynamic> toJson() => {

    '"round"': '"$round"',

    '"pairs"': pairs.toString(),

    '"immunity"': '"$immunity"',
  };



}