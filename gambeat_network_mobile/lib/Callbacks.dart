import 'package:Gambeat/custom-widgets/competion_custom_radio_button_dialog.dart';
import 'package:Gambeat/home3.dart';
import 'package:flutter/material.dart';

typedef SearchCallback = void Function(String searchText);

typedef CompetitionOptionDropDownMenuCallback = void Function(String button);

typedef OnCompetitionPriceButtonClicked = void Function(RadioModel radioModel, int button, bool isSelected);

typedef NavigationDrawerCallback = void Function();

typedef Widget ZoomScaffoldBuilder(
    BuildContext context,
    MenuController menuController
    );

const String CompetionDropDownMenuSearchValue = "Search";

const String CompetionDropDownMenuCreateValue = "Create a competition";
