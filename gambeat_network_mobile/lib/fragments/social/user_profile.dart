import 'package:Gambeat/helper/SharedPreferenceHelper.dart';
import 'package:Gambeat/models/User.dart';
import 'package:flutter/material.dart';

class UserProfile extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _UserProfile();
  }
}

class _UserProfile extends State<UserProfile> {

  SharedPreferenceHelper sharedPreference = new SharedPreferenceHelper();

  User user = new User();

  String userName = "";

  void loadUsersDetails() async {
    user = await sharedPreference.getUser();

    print(user.firstName);

    setState(() {

    });
  }

  @override
  void initState() {

    super.initState();

    loadUsersDetails();

  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,

      child: ListView(
        children: <Widget>[
          Container(

            margin: EdgeInsets.symmetric(horizontal: 15.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 25.0,
                ),
                Column(
                  children: <Widget>[
                    Hero(
                      tag: 'profile-pic',
                      child: Container(
                        height: 125.0,
                        width: 125.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(62.5),
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: AssetImage('images/chris.jpg'))),
                      ),
                    ),
                    SizedBox(
                      height: 25.0,
                    ),
                    Text(
                      user.firstName == null ? " " : "${user.firstName} " " ${user.lastName}",
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.location_on,
                          color: Colors.blue,
                        ),
                        Text(
                          user.country == null ? " " : user.country,
                          style: TextStyle(
                              //fontFamily: 'Montserrat',
                              color: Colors.grey),
                        ),
                      ],
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 30.0, horizontal: 20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                user.playerStats == null ? "0" : user.playerStats.friends.toString(),
                                style: TextStyle(
                                  fontSize: 20.0,
                                  //fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Text(
                                'FRIENDS',
                                style: TextStyle(
                                  fontSize: 15.0,
                                  //fontFamily: 'Montserrat',
                                ),
                              ),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                user.playerStats == null ? "0" : user.playerStats.wins.toString(),
                                style: TextStyle(
                                  fontSize: 20.0,
                                  //fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Text(
                                'WINS',
                                style: TextStyle(
                                  fontSize: 15.0,
                                  //fontFamily: 'Montserrat',
                                ),
                              ),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                user.playerStats == null ? "0" : user.playerStats.loss.toString(),
                                style: TextStyle(
                                  fontSize: 20.0,
                                  //fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Text(
                                'LOSS',
                                style: TextStyle(
                                  fontSize: 15.0,
                                  //fontFamily: 'Montserrat',
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Expanded(
                            child: new Container(
                              margin: EdgeInsets.only(
                                right: 8.0,
                              ),
                              alignment: Alignment.center,
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  new Expanded(
                                    child: new RaisedButton(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 15.0),
                                      shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(30.0),
                                      ),
                                      color: Colors.blue,
                                      onPressed: () => {},
                                      child: new Container(
                                        child: new Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: <Widget>[
                                            Icon(
                                              Icons.people,
                                              color: Colors.white,
                                              size: 15.0,
                                            ),
                                            Text(
                                              "Follow",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          new Expanded(
                            child: new Container(
                              margin: EdgeInsets.only(left: 8.0),
                              alignment: Alignment.center,
                              child: new Row(
                                children: <Widget>[
                                  new Expanded(
                                    child: new RaisedButton(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 15.0),
                                      shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(30.0),
                                      ),
                                      color: Colors.orangeAccent,
                                      onPressed: () => {},
                                      child: new Container(
                                        child: new Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: <Widget>[
                                            Icon(
                                              Icons.message,
                                              size: 15.0,
                                              color: Colors.white,
                                            ),
                                            Text(
                                              "Message",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 25.0,
                    )
                  ],
                ),
                SizedBox(
                  height: 25.0,
                ),
                Card(
                  child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 20.0,
                        vertical: 20.0,
                      ),
                      child: Column(
                        children: <Widget>[
                          Text(
                            user.bio == null ? "" : user.bio,
                            textAlign: TextAlign.center,
                            style: TextStyle(height: 2.0),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Text(
                                "- ${user.userName}",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    height: 2.0,
                                    color: Colors.black54,
                                    fontStyle: FontStyle.italic),
                              ),
                            ],
                          )
                        ],
                      )),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Card(
                  child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 20.0,
                        vertical: 20.0,
                      ),
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text("First Name"),
                              Spacer(),
                              Text(user.firstName == null ? "" : user.firstName,),
                              SizedBox(
                                width: 10.0,
                              ),
                              Icon(
                                Icons.keyboard_arrow_right,
                                color: Colors.grey,
                              )
                            ],
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Divider(
                            indent: 0.5,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Row(
                            children: <Widget>[
                              Text("Last Name"),
                              Spacer(),
                              Text(user.lastName == null ? "" : user.lastName,),
                              SizedBox(
                                width: 10.0,
                              ),
                              Icon(
                                Icons.keyboard_arrow_right,
                                color: Colors.grey,
                              )
                            ],
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Divider(
                            color: Colors.grey,
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Row(
                            children: <Widget>[
                              Text("User Name"),
                              Spacer(),
                              Text(user.userName == null ? "" : user.userName,),
                              SizedBox(
                                width: 10.0,
                              ),
                              Icon(
                                Icons.keyboard_arrow_right,
                                color: Colors.grey,
                              )
                            ],
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Divider(
                            color: Colors.grey,
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Row(
                            children: <Widget>[
                              Text("Gender"),
                              Spacer(),
                              Text(user.gender == null ? "" : user.gender,),
                              SizedBox(
                                width: 10.0,
                              ),
                              Icon(
                                Icons.keyboard_arrow_right,
                                color: Colors.grey,
                              )
                            ],
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Divider(
                            color: Colors.grey,
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Row(
                            children: <Widget>[
                              Text("Age"),
                              Spacer(),
                              Text("21"),
                              SizedBox(
                                width: 10.0,
                              ),
                              Icon(
                                Icons.keyboard_arrow_right,
                                color: Colors.grey,
                              )
                            ],
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Divider(
                            color: Colors.grey,
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Row(
                            children: <Widget>[
                              Text("Country"),
                              Spacer(),
                              Text(user.country == null ? "" : user.country,),
                              SizedBox(
                                width: 10.0,
                              ),
                              Icon(
                                Icons.keyboard_arrow_right,
                                color: Colors.grey,
                              )
                            ],
                          ),
                        ],
                      )),
                ),
                SizedBox(
                  height: 20.0,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
