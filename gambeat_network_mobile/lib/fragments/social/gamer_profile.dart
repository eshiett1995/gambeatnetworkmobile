import 'dart:convert';
import 'dart:io';

import 'package:Gambeat/chatpage.dart';
import 'package:Gambeat/helper/SharedPreferenceHelper.dart';
import 'package:Gambeat/models/Enum.dart';
import 'package:Gambeat/models/FriendRequest.dart';
import 'package:Gambeat/models/Message.dart';
import 'package:Gambeat/models/User.dart';
import 'package:Gambeat/network/api/ApiHelper.dart';
import 'package:flutter/material.dart';

class GamerProfile extends StatefulWidget {
  final User user;

  GamerProfile({Key key, @required this.user}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new _GamerProfile();
  }
}

class _GamerProfile extends State<GamerProfile> {

  bool isFollowing = false;

  @override
  Widget build(BuildContext context) {

    void goToChatPage(User friend) async{

      User currentUser = await new SharedPreferenceHelper().getUser();

      //if (currentUser == null) return;
      Navigator.of(context).push(new MaterialPageRoute(
          builder: (BuildContext context) => new ChatScreen(
            currentUser: currentUser,
            friend: friend,
            messages: [],
          )));
    }

    Future sendFriendRequest() async {

      isFollowing = true;

      setState(() {});

      FriendRequest friendRequest = new FriendRequest();

      friendRequest.sender = await new SharedPreferenceHelper().getUser();

      friendRequest.friendRequestStatus = FriendRequestStatus.PENDING;

      friendRequest.recipient = widget.user;

      HttpClientResponse friendRequestResponseModel = await new ApiHelper().sendFriendRequest(friendRequest);

      String response = await friendRequestResponseModel.transform(utf8.decoder).join();

      if (friendRequestResponseModel.statusCode == 200) {

      } else {

      }

      isFollowing = false;

      setState(() {});


    }

    Widget _buildFollowButton(){

      if(!isFollowing){

        return  new Container(
          margin: EdgeInsets.only(left: 8.0),
          alignment: Alignment.center,
          child: new Row(
            children: <Widget>[
              new Expanded(
                child:new RaisedButton(
                  padding:
                  EdgeInsets.symmetric(vertical: 15.0),
                  shape: new RoundedRectangleBorder(
                    borderRadius:
                    new BorderRadius.circular(30.0),
                  ),
                  color: Colors.blue,
                  onPressed: () => sendFriendRequest(),
                  child: new Container(
                    child: new Row(
                      mainAxisAlignment:
                      MainAxisAlignment.spaceEvenly,
                      children: <Widget>[


                        Icon(
                          Icons.message,
                          size: 15.0,
                          color: Colors.white,
                        ),
                        Text(
                          "Follow",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ),
              )],),);

      }else{

        return Container(
          height: 48.0,
          width: 48.0,
          child: RaisedButton(
            padding: EdgeInsets.all(0.0),
            shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(30.0),
            ),
            child: SizedBox(
                height: 30.0,
                width: 30.0,
                child: Stack(
                  children: <Widget>[
                    CircularProgressIndicator(
                      value: null,
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                    ),
                    Positioned(
                      child: Icon(Icons.people, size: 18.0, color: Colors.white,),
                      left: 0.0,
                      right: 0.0,
                      top: 0.0,
                      bottom: 0.0,
                    )
                  ],
                )
            ),
            disabledColor: Colors.blue,
            onPressed: null,
          ),
        );

      }
    }


    return new Scaffold(
      body: ListView(
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(horizontal: 15.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 25.0,
                ),
                Column(
                  children: <Widget>[
                    Hero(
                      tag: 'profile-pic',
                      child: Container(
                        height: 125.0,
                        width: 125.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(62.5),
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: AssetImage('images/chris.jpg'))),
                      ),
                    ),
                    SizedBox(
                      height: 25.0,
                    ),
                    Text(
                      '${widget.user.firstName} ${widget.user.lastName}',
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.location_on,
                          color: Colors.blue,
                        ),
                        Text(
                          widget.user.country,
                          style: TextStyle(
                              //fontFamily: 'Montserrat',
                              color: Colors.grey),
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: 30.0, horizontal: 20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                widget.user.playerStats.friends.toString(),
                                style: TextStyle(
                                  fontSize: 20.0,
                                  //fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Text(
                                'FRIENDS',
                                style: TextStyle(
                                  fontSize: 15.0,
                                  //fontFamily: 'Montserrat',
                                ),
                              ),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                widget.user.playerStats.wins.toString(),
                                style: TextStyle(
                                  fontSize: 20.0,
                                  //fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Text(
                                'WINS',
                                style: TextStyle(
                                  fontSize: 15.0,
                                  //fontFamily: 'Montserrat',
                                ),
                              ),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                widget.user.playerStats.loss.toString(),
                                style: TextStyle(
                                  fontSize: 20.0,
                                  //fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Text(
                                'LOSS',
                                style: TextStyle(
                                  fontSize: 15.0,
                                  //fontFamily: 'Montserrat',
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Expanded(
                            child: new Container(
                              margin: EdgeInsets.only(
                                right: 8.0,
                              ),
                              alignment: Alignment.center,
                              child: new Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  new Expanded(
                                    child: _buildFollowButton(),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          new Expanded(
                            child: new Container(
                              margin: EdgeInsets.only(left: 8.0),
                              alignment: Alignment.center,
                              child: new Row(
                                children: <Widget>[
                                  new Expanded(
                                    child: new RaisedButton(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 15.0),
                                      shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(30.0),
                                      ),
                                      color: Colors.orangeAccent,
                                      onPressed: () => goToChatPage(widget.user),
                                      child: new Container(
                                        child: new Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: <Widget>[


                                            Icon(
                                              Icons.message,
                                              size: 15.0,
                                              color: Colors.white,
                                            ),
                                            Text(
                                              "Message",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 25.0,
                    )
                  ],
                ),
                SizedBox(
                  height: 25.0,
                ),
                Card(
                  child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 20.0,
                        vertical: 20.0,
                      ),
                      child: Column(
                        children: <Widget>[
                          Text(
                            widget.user.bio,
                            textAlign: TextAlign.center,
                            style: TextStyle(height: 2.0),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Text(
                                widget.user.userName,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    height: 2.0,
                                    color: Colors.black54,
                                    fontStyle: FontStyle.italic),
                              ),
                            ],
                          )
                        ],
                      )),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Card(
                  child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 20.0,
                        vertical: 20.0,
                      ),
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text("First Name"),
                              Spacer(),
                              Text(widget.user.firstName),
                              SizedBox(
                                width: 10.0,
                              ),
                              Icon(
                                Icons.keyboard_arrow_right,
                                color: Colors.grey,
                              )
                            ],
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Divider(
                            indent: 0.5,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Row(
                            children: <Widget>[
                              Text("Last Name"),
                              Spacer(),
                              Text(widget.user.lastName),
                              SizedBox(
                                width: 10.0,
                              ),
                              Icon(
                                Icons.keyboard_arrow_right,
                                color: Colors.grey,
                              )
                            ],
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Divider(
                            color: Colors.grey,
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Row(
                            children: <Widget>[
                              Text("User Name"),
                              Spacer(),
                              Text(widget.user.userName),
                              SizedBox(
                                width: 10.0,
                              ),
                              Icon(
                                Icons.keyboard_arrow_right,
                                color: Colors.grey,
                              )
                            ],
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Divider(
                            color: Colors.grey,
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Row(
                            children: <Widget>[
                              Text("Gender"),
                              Spacer(),
                              Text(widget.user.gender),
                              SizedBox(
                                width: 10.0,
                              ),
                              Icon(
                                Icons.keyboard_arrow_right,
                                color: Colors.grey,
                              )
                            ],
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Divider(
                            color: Colors.grey,
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Row(
                            children: <Widget>[
                              Text("Age"),
                              Spacer(),
                              Text("21"),
                              SizedBox(
                                width: 10.0,
                              ),
                              Icon(
                                Icons.keyboard_arrow_right,
                                color: Colors.grey,
                              )
                            ],
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Divider(
                            color: Colors.grey,
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Row(
                            children: <Widget>[
                              Text("Country"),
                              Spacer(),
                              Text(widget.user.country),
                              SizedBox(
                                width: 10.0,
                              ),
                              Icon(
                                Icons.keyboard_arrow_right,
                                color: Colors.grey,
                              )
                            ],
                          ),
                        ],
                      )),
                ),
                SizedBox(
                  height: 20.0,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
