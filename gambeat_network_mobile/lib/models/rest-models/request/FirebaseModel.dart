

import 'package:Gambeat/models/rest-models/response/ResponseModel.dart';

class FirebaseModel{

  FirebaseModel();

  String token;

  ResponseModel responseModel;


  Map<String, dynamic> toJson() => {

    '"token"': '"$token"',

    //'"responseModel"': responseModel == null ? null : ResponseModel.toJson(),

  };



  factory FirebaseModel.fromJson(Map<String, dynamic> parsedJson) {

    FirebaseModel firebaseModel = new FirebaseModel();

    firebaseModel.token =  parsedJson['token'];

    firebaseModel.responseModel = ResponseModel.fromJson(parsedJson['responseModel']);

    return firebaseModel;

  }


}