import 'package:Gambeat/helper/GetTimeAgo.dart';
import 'package:Gambeat/models/Enum.dart';
import 'package:Gambeat/models/Message.dart';
import 'package:Gambeat/models/User.dart';
import 'package:flutter/material.dart';

var currentUserEmail;

class ChatMessageListItem extends StatefulWidget {
  final Message messageSnapshot;
  final Animation animation;
  final User currentuser;
  final bool useSentLayout;
  final bool isRead;

  ChatMessageListItem(
      {this.messageSnapshot,
      this.animation,
      this.currentuser,
      this.useSentLayout,
      this.isRead});

  @override
  ChatMessageListItemState createState() => ChatMessageListItemState();
}

class ChatMessageListItemState extends State<ChatMessageListItem>
    with TickerProviderStateMixin {
  String timeAgo = "";

  @override
  Widget build(BuildContext context) {
    timeAgo = getTimeAgo(new DateTime.fromMillisecondsSinceEpoch(widget.messageSnapshot.dateCreated * 1000));
    return new Container(
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      child: new Column(
        crossAxisAlignment: widget.useSentLayout
            ? CrossAxisAlignment.end
            : CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: widget.useSentLayout
            ? getSentMessageLayout()
            : getReceivedMessageLayout(),
      ),
//      ),
    );
  }

  _getMessageStatusIcon(Message message){

    if(message.messageStatus == MessageStatus.UNDELIVERED){
      return new Icon(
          Icons.done,
          size: 18.0,
          color: Colors.grey,);
    }else if(message.messageStatus == MessageStatus.DELIVERED){
      return new Icon(
          Icons.done_all,
          size: 18.0,
          color: Colors.grey,);
    }else if(message.messageStatus == MessageStatus.READ){
      return new Icon(
          Icons.done_all,
          size: 18.0,
          color: Colors.blueAccent,);
    }else {
      return new Icon(
          Icons.done,
          size: 18.0,
          color: Colors.grey,);
    }
  }

  List<Widget> getSentMessageLayout() {
    return <Widget>[
      Card(
          child: Container(
            decoration: new BoxDecoration(borderRadius: BorderRadiusDirectional.only(topStart: Radius.circular(15.0)),),
              constraints: new BoxConstraints(
                maxWidth: 250.0,
              ),
              padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
              child: IntrinsicWidth(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Flexible(
                        child: IntrinsicWidth(
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          new Text(widget.messageSnapshot.sender.userName,
                              style: new TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold)),
                          new Container(
                            margin: const EdgeInsets.only(top: 5.0),
                            child: widget.messageSnapshot.sender.picture == null
                                ? new Image.network(
                                    widget.messageSnapshot.sender.picture,
                                    width: 250.0,
                                  )
                                : new Text(widget.messageSnapshot.message),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                Text(
                                  timeAgo,
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                      color: Colors.grey, fontSize: 10.0),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )),
                    new Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        new Align(
                          alignment: Alignment.topCenter,
                          child: new Container(
                              margin: const EdgeInsets.only(left: 8.0),
                              child: new CircleAvatar(
                                backgroundImage: new NetworkImage(
                                    widget.messageSnapshot.sender.picture),
                              )),
                        ),
                        new Align(
                            alignment: Alignment.bottomCenter,
                            child: _getMessageStatusIcon(widget.messageSnapshot)),
                      ],
                    ),
                  ],
                ),
              )))
//        ],
//      ),
    ];
  }

  timeAgoCallBack(String newTimeAgo) async {
    if (this.mounted) {
      setState(() {
        timeAgo = newTimeAgo;
      });
    }
  }

  List<Widget> getReceivedMessageLayout() {
    return <Widget>[
      Card(
          child: Container(
            decoration: new BoxDecoration(borderRadius: BorderRadiusDirectional.only(topStart: Radius.circular(15.0)),),
        constraints: new BoxConstraints(
          maxWidth: 250.0,
        ),
        padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                    margin: const EdgeInsets.only(right: 8.0),
                    child: new CircleAvatar(
                      backgroundImage: new NetworkImage(
                          widget.messageSnapshot.receiver.picture),
                    )),
              ],
            ),
            new Flexible(
                child: IntrinsicWidth(
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(widget.messageSnapshot.receiver.userName,
                      style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                          fontWeight: FontWeight.bold)),
                  new Container(
                    margin: const EdgeInsets.only(top: 5.0),
                    child: widget.messageSnapshot.receiver.picture !=
                            null //TODO: This should me a message imageUrl if the message is an image file
                        ? new Text(
                            widget.messageSnapshot.message,
                            textAlign: TextAlign.justify,
                            overflow: TextOverflow.fade,
                          )
                        : new Text(widget.messageSnapshot.message),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Text(
                          timeAgo,
                          textAlign: TextAlign.right,
                          style: TextStyle(color: Colors.grey, fontSize: 10.0),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )),
          ],
        ),
      ))
    ];
  }
}
