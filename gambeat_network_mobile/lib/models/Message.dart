import 'package:Gambeat/models/DefaultEntity.dart';
import 'package:Gambeat/models/Enum.dart';
import 'package:Gambeat/models/User.dart';

class Message extends DefaultEntity {

  Message();

  User sender;

  User receiver;

  String message;

  MessageStatus messageStatus;

  bool isRead;

  factory Message.fromJson(Map<String, dynamic> parsedJson) {
    Message message = new Message();

    message.id = parsedJson['_id'];

    message.dateCreated = DateTime.parse(parsedJson['dateCreated']).millisecondsSinceEpoch;

    message.sender = User.fromJson(parsedJson['sender']);

    message.receiver = User.fromJson(parsedJson['receiver']);

    message.message = parsedJson['message'];

    message.messageStatus = getMessageStatus(parsedJson['messageStatus']);

    message.isRead = parsedJson['isRead'];

    return message;
  }

  ///Returns a MessageStatus corresponding to the String value gotten from the Api
  static MessageStatus getMessageStatus(String messageStat) {
    switch (messageStat) {
      case "DELIVERED":
        return MessageStatus.DELIVERED;
      case "UNDELIVERED":
        return MessageStatus.UNDELIVERED;
      case "FAILED":
        return MessageStatus.FAILED;
      case "UNREAD":
        return MessageStatus.UNREAD;
      default:
        return MessageStatus.FAILED;
    }
  }

  Map<String, dynamic> toJson() => {

        '"_id"': '"$id"',

        '"dateCreated"': dateCreated,

        '"sender"': sender == null ? null : sender.toJson(),

        '"receiver"': receiver == null ? null : receiver.toJson(),

        '"message"': '"$message"',

        '"messageStatus"': '$messageStatus',

        '"isRead"': isRead,
      };
}