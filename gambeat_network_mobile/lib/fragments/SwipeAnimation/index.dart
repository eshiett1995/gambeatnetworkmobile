import 'dart:async';
import 'dart:convert';
import 'dart:io';

//import 'package:animation_exp/PageReveal/page_main.dart';
import 'package:Gambeat/fragments/SwipeAnimation/activeCard.dart';
import 'package:Gambeat/fragments/SwipeAnimation/dummyCard.dart';
import 'package:Gambeat/fragments/SwipeAnimation/styles.dart';
import 'package:Gambeat/models/FriendRequest.dart';
import 'package:Gambeat/models/User.dart';
import 'package:Gambeat/models/rest-models/response/FriendRequestResponseModel.dart';
import 'package:Gambeat/models/rest-models/response/ResponseModel.dart';
import 'package:Gambeat/models/rest-models/response/UserPaginationModel.dart';
import 'package:Gambeat/network/api/ApiHelper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart' show timeDilation;

class CardDemo extends StatefulWidget {
  @override
  CardDemoState createState() => new CardDemoState();
}

class CardDemoState extends State<CardDemo> with TickerProviderStateMixin {
  AnimationController _buttonController;
  Animation<double> rotate;
  Animation<double> right;
  Animation<double> bottom;
  Animation<double> width;
  int flag = 0;

  //List data = [image5, image3, image4, image2, image1];
  ApiHelper apiHelper = new ApiHelper();

  List selectedData = [];
  void initState() {
    super.initState();

    _getListOfFriendRequests();
    _buttonController = new AnimationController(
        duration: new Duration(milliseconds: 1000), vsync: this);

    rotate = new Tween<double>(
      begin: -0.0,
      end: -40.0,
    ).animate(
      new CurvedAnimation(
        parent: _buttonController,
        curve: Curves.ease,
      ),
    );
    rotate.addListener(() {
      setState(() {
        if (rotate.isCompleted) {
          setState(() {
            var f = friendRequests.removeLast();
          });
          //data.insert(0, i);

          _buttonController.reset();
        }
      });
    });

    right = new Tween<double>(
      begin: 0.0,
      end: 400.0,
    ).animate(
      new CurvedAnimation(
        parent: _buttonController,
        curve: Curves.ease,
      ),
    );
    bottom = new Tween<double>(
      begin: 15.0,
      end: 100.0,
    ).animate(
      new CurvedAnimation(
        parent: _buttonController,
        curve: Curves.ease,
      ),
    );
    width = new Tween<double>(
      begin: 20.0,
      end: 25.0,
    ).animate(
      new CurvedAnimation(
        parent: _buttonController,
        curve: Curves.bounceOut,
      ),
    );
  }

  @override
  void dispose() {
    _buttonController.dispose();
    super.dispose();
  }

  Future<Null> _swipeAnimation() async {
    try {
      await _buttonController.forward();
    } on TickerCanceled {}
  }

  dismissUser(FriendRequest friendRequest) {
    setState(() {
      acceptOrRejectFriendRequest(friendRequest, false);
      friendRequests.remove(friendRequest);
    });
  }

  addUser(FriendRequest friendRequest) async {
    setState(() {
      acceptOrRejectFriendRequest(friendRequest, true);
      friendRequests.remove(friendRequest);
      selectedData.add(friendRequest);
    });
  }

  acceptOrRejectFriendRequest(FriendRequest friendRequest, bool isAccept) async {

    print(friendRequest.iid);

    HttpClientResponse response =
        await apiHelper.acceptOrRejectFriendRequest(friendRequest, isAccept);

    String mm = await response.transform(utf8.decoder).join();
    var parsedJson = json.decode(mm);
    if (response.statusCode == 200) {
      ResponseModel responseModel = ResponseModel.fromJson(parsedJson);
      if (responseModel.isSuccessful) {
        setState(() {
          Scaffold.of(context).showSnackBar(new SnackBar(
              content:
                  new Text(isAccept ? "Friend Accepted" : "Friend Rejected")));
        });
      }
    } else
      setState(() {
        print("Accept Or Reject statusCode --- " + response.statusCode.toString());
        Scaffold.of(context)
            .showSnackBar(new SnackBar(content: new Text("Action Failed")));
      });
  }

  swipeRight() {
    if (flag == 0)
      setState(() {
        flag = 1;
      });
    _swipeAnimation();
  }

  swipeLeft() {
    if (flag == 1)
      setState(() {
        flag = 0;
      });
    _swipeAnimation();
  }

  List<FriendRequest> friendRequests = new List<FriendRequest>();

  _getListOfFriendRequests() async {
    ApiHelper apiHelper = new ApiHelper();

    HttpClientResponse httpClientResponse =
        await apiHelper.getAllFriendRequest();

    String jsonStr = await httpClientResponse.transform(utf8.decoder).join();

    var jsonD = json.decode(jsonStr);

    if (httpClientResponse.statusCode == 200) {
      FriendRequestResponseModel friendRequestResponseModel =
          FriendRequestResponseModel.fromJson(jsonD);

      setState(() {

        friendRequests = friendRequestResponseModel.content;

        friendRequestResponseModel.content.forEach((f) =>

          print(f.iid)

        );

      });
    }
  }

  @override
  Widget build(BuildContext context) {
    timeDilation = 0.4;

    double initialBottom = 15.0;
    var dataLength = friendRequests.length;

    double backCardPosition = initialBottom + (dataLength - 1) * 10 + 10;
    double backCardWidth = -10.0;
    return (new Scaffold(
        appBar: new AppBar(
          elevation: 0.0,
          backgroundColor: new Color.fromRGBO(106, 94, 175, 1.0),
          centerTitle: true,
          title: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text(
                "REQUESTS",
                style: new TextStyle(
                    fontSize: 12.0,
                    letterSpacing: 3.5,
                    fontWeight: FontWeight.bold),
              ),
              new Container(
                width: 15.0,
                height: 15.0,
                margin: new EdgeInsets.only(bottom: 20.0),
                alignment: Alignment.center,
                child: new Text(
                  dataLength.toString(),
                  style: new TextStyle(fontSize: 10.0),
                ),
                decoration: new BoxDecoration(
                    color: Colors.green, shape: BoxShape.circle),
              )
            ],
          ),
        ),
        body: new Container(
          color: new Color.fromRGBO(106, 94, 175, 1.0),
          alignment: Alignment.center,
          child: dataLength > 0
              ? new Stack(
                  alignment: AlignmentDirectional.center,
                  children: friendRequests.map((item) {
                    if (friendRequests.indexOf(item) == dataLength - 1) {
                      return cardDemo(
                          item,
                          bottom.value,
                          right.value,
                          0.0,
                          backCardWidth + 10,
                          rotate.value,
                          rotate.value < -10 ? 0.1 : 0.0,
                          context,
                          dismissUser,
                          flag,
                          addUser,
                          swipeRight,
                          swipeLeft);
                    } else {
                      backCardPosition = backCardPosition - 10;
                      backCardWidth = backCardWidth + 10;

                      return cardDemoDummy(item.sender, backCardPosition, 0.0, 0.0,
                          backCardWidth, 0.0, 0.0, context);
                    }
                  }).toList())
              : new Text("No Friend Request Left",
                  style: new TextStyle(color: Colors.white, fontSize: 35.0)),
        )));

  }
}
