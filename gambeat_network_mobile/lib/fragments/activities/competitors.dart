import 'package:Gambeat/fragments/social/gamer_profile.dart';
import 'package:Gambeat/models/Competition.dart';
import 'package:Gambeat/models/User.dart';
import 'package:flutter/material.dart';

class Competitors extends StatefulWidget {

  final Competition competition;

  Competitors({this.competition});

  @override
  State<StatefulWidget> createState() {
    return new _Competitors();
  }
}

class _Competitors extends State<Competitors> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    void competitorOptionPanel(User selectedUser) {
      showModalBottomSheet(
          context: context,
          builder: (builder) {
            return new Container(
              color: Colors.white,
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    height: 20.0,
                  ),
                  Text("SELECT AN OPTION:"),
                  ListTile(
                    leading: Icon(Icons.account_circle),
                    title: Text("Profile"),
                    onTap: () {
                      var route = new MaterialPageRoute(
                        builder: (BuildContext context) =>
                            new GamerProfile(user: selectedUser),
                      );

                      Navigator.of(context).push(route);
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.message),
                    title: Text("Message"),
                  ),
                  ListTile(
                    leading: Icon(Icons.reply),
                    title: Text("Unfollow"),
                  ),
                ],
              ),
            );
          });
    }

    return new Scaffold(
        appBar: AppBar(title: Text("Tic-Tac-Toe-Pisces Competitors"),),
        body: new Container(
          decoration: new BoxDecoration(
              image: new DecorationImage(
            colorFilter: new ColorFilter.mode(
                Colors.black.withOpacity(0.2), BlendMode.dstATop),
            image: new AssetImage('images/games.jpg'),
            fit: BoxFit.cover,
          )),
          child: new Column(
            children: <Widget>[
              new GestureDetector(
                onTap: () {
                  competitorOptionPanel(widget.competition.punters[0].player);
                },
                child: new Stack(
                  children: <Widget>[
                    new Card(
                      margin:
                      EdgeInsets.symmetric(vertical: 15.0, horizontal: 10.0),
                      elevation: 3.0,
                      child: Padding(
                          padding: EdgeInsets.all(15.0),
                          child: Row(
                            children: <Widget>[
                              new CircleAvatar(
                                radius: 35.0,
                                backgroundImage: AssetImage('images/chris.jpg'),
                              ),
                              Padding(
                                padding: EdgeInsets.only(right: 10.0),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Eshiett Oto-bong",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    height: 5.0,
                                  ),
                                  Text(
                                    "eshiett1995",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(color: Colors.black54),
                                  ),
                                  SizedBox(
                                    height: 5.0,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Icon(
                                        Icons.location_on,
                                        color: Colors.blue,
                                      ),
                                      Text(
                                        "Nigeria",
                                        textAlign: TextAlign.left,
                                        style: TextStyle(color: Colors.black54),
                                      ),
                                    ],
                                  )
                                ],
                              )
                            ],
                          )),
                    ),
                    new Positioned(
                      right: 10.0,
                      top: 15.0,
                      child: Container(
                        width: 10.0,
                        height: 100.0,
                        color: Colors.greenAccent,
                      ),
                    ),
                  ],
                )
              ),
            ],
          ),
        ));
  }
}
