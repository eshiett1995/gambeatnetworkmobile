import 'dart:math';

import 'package:Gambeat/models/FriendRequest.dart';
import 'package:Gambeat/models/User.dart';
import 'package:flutter/material.dart';

Positioned cardDemo(
    FriendRequest friendRequest,
    double bottom,
    double right,
    double left,
    double cardWidth,
    double rotation,
    double skew,
    BuildContext context,
    Function dismissUser,
    int flag,
    Function addUser,
    Function swipeRight,
    Function swipeLeft) {
  Size screenSize = MediaQuery.of(context).size;
  // print("Card");
  return new Positioned(
    bottom: 100.0 + bottom,
    right: flag == 0 ? right != 0.0 ? right : null : null,
    left: flag == 1 ? right != 0.0 ? right : null : null,
    child: new Dismissible(
      key: new Key(new Random().toString()),
      crossAxisEndOffset: -0.3,
      onResize: () {
        //print("here");
        // setState(() {
        //   var i = data.removeLast();

        //   data.insert(0, i);
        // });
      },
      onDismissed: (DismissDirection direction) {
//          _swipeAnimation();
        if (direction == DismissDirection.endToStart) {
          print("it got here");
          dismissUser(friendRequest);
        }else {
          addUser(friendRequest);
        }
      },
      child: new Transform(
        alignment: flag == 0 ? Alignment.bottomRight : Alignment.bottomLeft,
        //transform: null,
        transform: new Matrix4.skewX(skew),
        //..rotateX(-math.pi / rotation),
        child: new RotationTransition(
          turns: new AlwaysStoppedAnimation(
              flag == 0 ? rotation / 360 : -rotation / 360),
          child: new Hero(
            tag: "img",
            child: new GestureDetector(
              onTap: () {
                // Navigator.push(
                //     context,
                //     new MaterialPageRoute(
                //         builder: (context) => new DetailPage(type: img)));
                //Navigator.of(context).push(new PageRouteBuilder(
                  //pageBuilder: (_, __, ___) => new DetailPage(type: img),
                //));
              },
              child: new Card(
                color: Colors.transparent,
                elevation: 4.0,
                child: new Container(
                  alignment: Alignment.center,
                  width: screenSize.width / 1.2 + cardWidth,
                  height: screenSize.height / 1.7,
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    borderRadius: new BorderRadius.circular(8.0),
                  ),
                  child: new Column(
                    children: <Widget>[
                      new Container(
                        width: screenSize.width / 1.2 + cardWidth,
                        height: screenSize.height / 2.2,
                        
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 15.0,
                            ),
                            Hero(
                              tag: 'profile-pic',
                              child: Container(
                                height: 125.0,
                                width: 125.0,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(62.5),
                                    image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: AssetImage('images/chris.jpg'))),
                              ),
                            ),
                            SizedBox(
                              height: 25.0,
                            ),
                            Text(
                              friendRequest.sender.userName,
                              style: TextStyle(
                                fontSize: 20.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.location_on,
                                  color: Colors.blue,
                                ),
                                Text(
                                  friendRequest.sender.country,
                                  style: TextStyle(
                                      //fontFamily: 'Montserrat',
                                      color: Colors.grey),
                                ),
                              ],
                            ),

                            Padding(
                    padding:
                        EdgeInsets.only( left: 30.0, right: 30.0, top: 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              friendRequest.sender.playerStats.friends.toString(),
                              style: TextStyle(
                                fontSize: 20.0,
                                //fontFamily: 'Montserrat',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: 5.0,
                            ),
                            Text(
                              'FRIENDS',
                              style: TextStyle(
                                fontSize: 15.0,
                                //fontFamily: 'Montserrat',
                              ),
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              friendRequest.sender.playerStats.wins.toString(),
                              style: TextStyle(
                                fontSize: 20.0,
                                //fontFamily: 'Montserrat',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: 5.0,
                            ),
                            Text(
                              'WINS',
                              style: TextStyle(
                                fontSize: 15.0,
                                //fontFamily: 'Montserrat',
                              ),
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              friendRequest.sender.playerStats.loss.toString(),
                              style: TextStyle(
                                fontSize: 20.0,
                                //fontFamily: 'Montserrat',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: 5.0,
                            ),
                            Text(
                              'LOSS',
                              style: TextStyle(
                                fontSize: 15.0,
                                //fontFamily: 'Montserrat',
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                          ],
                        ),
                      ),
                      new Container(
                          width: screenSize.width / 1.2 + cardWidth,
                          height:
                              screenSize.height / 1.7 - screenSize.height / 2.2,
                          alignment: Alignment.center,
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              new FlatButton(
                                  padding: new EdgeInsets.all(0.0),
                                  onPressed: () {
                                    swipeLeft();


                                  },
                                  child: new Container(
                                      height: 50.0,
                                      width: 130.0,
                                      alignment: Alignment.center,
                                      decoration: new BoxDecoration(
                                        color: Colors.red,
                                        borderRadius:
                                            new BorderRadius.circular(60.0),
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          Icon(
                                            Icons.thumb_down,
                                            color: Colors.white,
                                          ),
                                          new Text(
                                            "Reject",
                                            style: new TextStyle(
                                                color: Colors.white),
                                          ),
                                        ],
                                      ))),
                              new FlatButton(
                                  padding: new EdgeInsets.all(0.0),
                                  onPressed: () {
                                    swipeRight();

                                  },
                                  child: new Container(
                                      height: 50.0,
                                      width: 130.0,
                                      alignment: Alignment.center,
                                      decoration: new BoxDecoration(
                                        color: Colors.blue,
                                        borderRadius:
                                            new BorderRadius.circular(60.0),
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          Icon(
                                            Icons.thumb_up,
                                            color: Colors.white,
                                          ),
                                          new Text(
                                            "Accept",
                                            style: new TextStyle(
                                                color: Colors.white),
                                          ),
                                        ],
                                      )))
                            ],
                          ))
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  );
}
