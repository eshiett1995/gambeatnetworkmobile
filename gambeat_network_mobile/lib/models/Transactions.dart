import 'package:Gambeat/models/DefaultEntity.dart';
import 'package:Gambeat/models/User.dart';

class Transactions  {

  Transactions();

  String id;

  DateTime dateCreated;

  User player;

  int amount;

  String transactiontype;

  String referenceID;

  String detail;

  String token;


  factory Transactions.fromJson(Map<String, dynamic> parsedJson) {
    
    Transactions transactions = new Transactions();

    transactions.id = parsedJson['id'];

    //transactions.dateCreated = parsedJson['dateCreated'];

    transactions.player = User.fromJson(parsedJson['player']);

    transactions.amount = int.parse(parsedJson['amount']);

    transactions.transactiontype = parsedJson['transactiontype'];

    transactions.referenceID = parsedJson['referenceID'];

    transactions.detail = parsedJson['detail'];
    
    transactions.token = parsedJson['token'];

    return transactions;

  }

  Map<String, dynamic> toJson() => {
    '"id"': '"$id"',
    '"dateCreated"': dateCreated,
    '"amount"': '"$amount"',
    '"transactiontype"': '"$transactiontype"',
    '"referenceID"': '"$referenceID"',
    '"detail"': '"$detail"',
    '"token"': '"$token"',
  };
}
