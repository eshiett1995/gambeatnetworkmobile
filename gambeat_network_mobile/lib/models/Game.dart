
import 'package:Gambeat/models/DefaultEntity.dart';
import 'package:Gambeat/models/DownloadLink.dart';
import 'package:Gambeat/models/Enum.dart';
import 'package:Gambeat/models/Genre.dart';
import 'package:Gambeat/models/Platform.dart';
import 'package:Gambeat/models/PlatformLink.dart';

class Game extends DefaultEntity{

  Game();

  String name;

  String image;

  List<Genre> genres = new List();

  CompetitionType competitionType;

  String description;

  List<Platform> platforms = new List();

  List<DownloadLink> downloadLinks = new List();

  List<PlatformLink> platformLinks = new List();


  factory Game.fromJson(Map<String, dynamic> parsedJson) {

    Game game = new Game();

    game.id = parsedJson['_id'];

    game.image = parsedJson['image'];

    game.name = parsedJson['name'];

    game.dateCreated = parsedJson['dateCreated'];

    game.genres = jsonToListOfGenres(parsedJson);

    game.competitionType = getCompetitionType(parsedJson['competitionType']);

    game.description = parsedJson['description'];

    game.platforms = jsonToListOfPlatforms(parsedJson);

    game.downloadLinks = jsonToListOfDownloadLink(parsedJson);

    game.platformLinks = jsonToListOfPlatformLink(parsedJson);

    return game;
  }

  static List<Genre> jsonToListOfGenres(Map<String, dynamic> parsedJson){

    var list = parsedJson['genres'] as List;

    print(list.runtimeType);

    List<Genre> genreList = list.map((i) => Genre.fromJson(i)).toList();

    return genreList;
  }

  static List<Platform> jsonToListOfPlatforms(Map<String, dynamic> parsedJson){

    var list = parsedJson['platforms'] as List;

    print(list.runtimeType);

    List<Platform> platformList = list.map((i) => Platform.fromJson(i)).toList();

    return platformList;
  }



  static List<PlatformLink> jsonToListOfPlatformLink(Map<String, dynamic> parsedJson){

    var list = parsedJson['platformLinks'] as List;

    print(list.runtimeType);

    List<PlatformLink> platformLinkList = list.map((i) => PlatformLink.fromJson(i)).toList();

    return platformLinkList;
  }


  static List<DownloadLink> jsonToListOfDownloadLink(Map<String, dynamic> parsedJson){

    var list = parsedJson['downloadLinks'] as List;

    print(list.runtimeType);

    List<DownloadLink> downloadLinkList = list.map((i) => DownloadLink.fromJson(i)).toList();

    return downloadLinkList;
  }



  ///Returns a MessageStatus corresponding to the String value gotten from the Api
  static CompetitionType getCompetitionType(String competitionType) {
    switch (competitionType) {
      case "ELIMINATION":
        return CompetitionType.ELIMINATION;
      case "FREEFORALL":
        return CompetitionType.FREEFORALL;
      case "ROYALRUMBLE":
        return CompetitionType.ROYALRUMBLE;
      default:
        return CompetitionType.ELIMINATION;
    }
  }

  Map<String, dynamic> toJson() => {

    '"_id"': '"$id"',

    '"dateCreated"': dateCreated,

    '"name"': '"$name"',

    '"image"': '"$image"',

    '"genre"': '"$genres"',

    '"competitionType"': '"$competitionType"',

    '"description"': '"$description"',

    '"platforms"': platforms == null ? null : platforms.toString(),

    '"downloadLinks"': downloadLinks == null ? null : downloadLinks.toString(),

    '"platformLinks"': platformLinks == null ? null : platformLinks.toString(),

  };


}