import 'package:Gambeat/helper/SharedPreferenceHelper.dart';
import 'package:Gambeat/models/User.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomeFragment extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _HomeFragment();
  }
}

class _HomeFragment extends State<HomeFragment> {
  SharedPreferenceHelper sharedPreference = new SharedPreferenceHelper();

  User user = new User();

  String userName = "";

  void loadUsersDetails() async {
    user = await sharedPreference.getUser();

    print(user.playerStats.expenses);

    setState(() {
      userName = user.userName;
    });
  }

  @override
  void initState() {
    super.initState();

    loadUsersDetails();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: Container(
      decoration: BoxDecoration(
        color: Colors.white,
        image: DecorationImage(
          image: AssetImage('images/background_main.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: new ListView(
        children: <Widget>[
          new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                _getListItemWidget(
                    cardText: "MATCH PLAYED",
                    iconData: FontAwesomeIcons.gamepad,
                    cardNumber: user.playerStats == null
                        ? 0
                        : user.playerStats.matchPlayed,
                    cardProgressText: "TOTAL MATCH PLAYED %",
                    progressbarValue: 1.0),

                _getListItemWidget(
                  cardText: "WINS",
                  iconData: FontAwesomeIcons.gamepad,
                  cardNumber: user.playerStats == null
                      ? 0
                      : user.playerStats.wins,
                  cardProgressText: "TOTAL WIN %",
                  progressbarValue: user.playerStats == null
                      ? 0.0
                      : user.playerStats.wins == 0 &&  user.playerStats.matchPlayed == 0 ?
                      0 : user.playerStats.wins / user.playerStats.matchPlayed,
                ),

                _getListItemWidget(
                  cardText: "LOSS",
                  iconData: FontAwesomeIcons.gamepad,
                  cardNumber: user.playerStats == null
                      ? 0
                      : user.playerStats.loss,
                  cardProgressText: "TOTAL LOSS %",
                  progressbarValue: user.playerStats == null
                      ? 0.0
                      : user.playerStats.loss == 0 &&  user.playerStats.matchPlayed == 0 ?
                      0 : user.playerStats.loss / user.playerStats.matchPlayed,
                ),

                _getListItemWidget(
                  cardText: "WALLET",
                  iconData: FontAwesomeIcons.moneyBillAlt,
                  cardNumber: user.money == null ? 0 : user.money,
                  cardProgressText: "GAMBEAT WALLET  %",
                  progressbarValue: 1.0,
                ),

                _getListItemWidget(
                  cardText: "PROFIT",
                  iconData: FontAwesomeIcons.moneyBillAlt,
                  cardNumber:
                      user.playerStats == null ? 0 : user.playerStats.profit,
                  cardProgressText: "CUMMULATIVE MONEY WON  %",
                  progressbarValue: user.playerStats == null ? 0.0 :
                  user.playerStats.expenses == 0.0 && user.playerStats.profit + user.playerStats.expenses == 0.0 ? 0.0 :
                  user.playerStats.profit/(user.playerStats.profit + user.playerStats.expenses),
                ),

                _getListItemWidget(
                  cardText: "EXPENSES",
                  iconData: FontAwesomeIcons.moneyBillAlt,
                  cardNumber:
                      user.playerStats == null ? 0 : user.playerStats.expenses,
                  cardProgressText: "CUMMULATIVE MONEY LOST  %",
                  progressbarValue: user.playerStats == null ? 0.0 :
                  user.playerStats.expenses == 0.0 && user.playerStats.profit + user.playerStats.expenses == 0.0 ? 0.0 :
                  user.playerStats.expenses/(user.playerStats.profit + user.playerStats.expenses),
                ),

                _getListItemWidget(
                  cardText: "FRIENDS",
                  iconData: FontAwesomeIcons.user,
                  cardNumber:
                      user.playerStats == null ? 0 : user.playerStats.friends,
                  cardProgressText: "CUMMULATIVE FRIENDS %",
                  progressbarValue: 1.0,
                )
              ])
        ],
      ),
    ));
  }

  _getListItemWidget(
      {@required IconData iconData,
      @required String cardText,
      @required var cardNumber,
      @required String cardProgressText,
      @required double progressbarValue}) {
    return new Container(
      decoration: new BoxDecoration(
          boxShadow: [
            new BoxShadow(
              color: Colors.black26,
              blurRadius: 8.0, // has the effect of softening the shadow
              spreadRadius: 2.0, // has the effect of extending the shadow
              offset: Offset(
                2.0, // horizontal, move right 10
                1.0, // vertical, move down 10
              ),
            )
          ],
          borderRadius: BorderRadius.circular(10.5),
          color: Colors.blueGrey,

          //borderRadius: BorderRadius.circular(20.5),
          image: new DecorationImage(
            image: new AssetImage('images/background_main.png'),
            fit: BoxFit.cover,
          )),
      margin: EdgeInsets.only(left: 20.0, right: 20.0, top: 40.0),
      padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
      height: 140.0,
      child: new Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[

          Stack(
            children: <Widget>[
              new Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[

                  new Align(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        new Align(
                          child: new Text(
                            cardText,
                            style: new TextStyle(
                                color: Colors.white, fontSize: 17.0,  fontFamily: 'Montserrat', letterSpacing: 0.5),
                          ),
                          alignment: Alignment.topRight,
                        ),
                        new SizedBox(
                          height: 10.0,
                        ),
                        new Align(
                          child: new Text(
                            cardNumber.toString(),
                            style: new TextStyle(
                                color: Colors.white, fontSize: 30.0,  fontFamily: 'Montserrat', letterSpacing: 1.0),
                          ),
                          alignment: Alignment.topRight,
                        ),
                        new SizedBox(
                          height: 10.0,
                        ),
                        new Align(
                          child: new Text(
                            cardProgressText,
                            style: new TextStyle(color: Colors.white,  fontFamily: 'Montserrat', letterSpacing: 0.5),
                          ),
                          alignment: Alignment.topRight,
                        ),
                      ],
                    ),
                    alignment: Alignment.topRight,
                  ),


                ],
              ),
              new Transform(
                transform: new Matrix4.translationValues(0.0, -5.0, 0.0),
                child: new Image.asset(
                  "images/pad.png",
                  height: 90.0,
                ),
              ),
            ],
          ),

          new Padding(
            padding: EdgeInsets.only(bottom: 5.0),
          ),
          new Container(
            height: 3.0,
            child: new LinearProgressIndicator(
              value: progressbarValue,
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),
              backgroundColor: Colors.white,
            ),
          )
        ],
      ),
    );
  }
}
