

import 'package:Gambeat/models/DefaultEntity.dart';
import 'package:Gambeat/models/Platform.dart';

class PlatformLink extends DefaultEntity{

  PlatformLink();

  Platform platform;

  String url;

  factory PlatformLink.fromJson(Map<String, dynamic> parsedJson) {

    PlatformLink platformLink = new PlatformLink();

    //platformLink.id = parsedJson['_id'];

    //platformLink.dateCreated = parsedJson['dateCreated'];

    platformLink.platform = Platform.fromJson(parsedJson['platform']);

    platformLink.url = parsedJson['url'];

    return platformLink;
  }


  Map<String, dynamic> toJson() => {

    '"_id"': '"$id"',

    '"dateCreated"': dateCreated,

    '"platform"': platform == null ? null : platform.toJson(),

    '"url"': '"$url"',

  };
}