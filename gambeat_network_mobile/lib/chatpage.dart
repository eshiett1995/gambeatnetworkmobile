import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:Gambeat/helper/method_channel_utility.dart';
import 'package:Gambeat/models/Message.dart';
import 'package:Gambeat/models/User.dart';
import 'package:Gambeat/models/rest-models/response/MessagePaginationModel.dart';
import 'package:Gambeat/network/api/ApiHelper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

import './chatmessagelistitem.dart';

var currentUserEmail;
var _scaffoldContext;

class ChatScreen extends StatefulWidget {
  final User friend;
  final List<Message> messages;
  final User currentUser;

  ChatScreen(
      {Key key,
      @required this.currentUser,
      @required this.friend,
      @required this.messages})
      : super(key: key);

  @override
  ChatScreenState createState() {
    return new ChatScreenState();
  }
}

class ChatScreenState extends State<ChatScreen> with TickerProviderStateMixin {
  final TextEditingController _textEditingController =
      new TextEditingController();
  bool _isComposingMessage = false;
  User currentUser;
  User friendUser;
  List<Message> messageList;

  var endOfPageCalled = 0;

  var currentPageNumber = 0;

  int totalNumberOfPages = 0;

  bool _isSubsequentListLoading = false;

  bool _isInitialListLoading = false;

  bool _getFriendsFromServerCalled = false;

  _initFields() {
    currentUser = widget.currentUser;
    friendUser = widget.friend;
    friendUser == null
        ? print("_initFields -- friend Users is null -- ")
        : print("friendUser not null");
    messageList = widget.messages;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _initFields();
    _getMessagesFromDatabase();
    MethodChannelUtility.nativePlatformPortal((MethodCall call) {
      // you get the message from the receivingMessage channel (there are other channels too)
      //e.g friend request, match etc
      if (call.method == "receivingMessage") {
        _setInitialListLoadingTrue();
        // here you get the json string from the message comvert it to a messaging.class model
        // push it to the messageList and set state on it
        String cc = call.arguments;

        Message message = Message.fromJson(json.decode(cc));
        MethodChannelUtility.deliveredMessage(message);

        if (message != null)
          setState(() {
            _setInitialListLoadingFalse();
            //TODO: Check if this line of code is valid,
            //TODO: Instead of doing this compare the DateTime if possible
            messageList.insert(messageList.length - 1, message);
          });
      }else if(call.method == "onMessageSeen"){
        String cc = call.arguments;

        Message message = Message.fromJson(json.decode(cc));
        print("Message seeen ooooh --- ");
        if(message != null){
          ///Start checking from bottom to up to ease tins up
          for(int i = messageList.length - 1; i >=0 ; i--){
            if(messageList[i].id == message.id){
              setState(() {
                int index = messageList.indexOf(message);
                messageList.removeAt(index);
                messageList.insert(index, message);
              });
            }
          }
        }
      } else if(call.method == "onMessageDelivered"){
        String cc = call.arguments;
        print("Message Delivered ooooh --- ");
        Message message = Message.fromJson(json.decode(cc));
        if(message != null){
          messageList.forEach((m){
            if(m.id == message.id){
              setState(() {
                int index = messageList.indexOf(message);
                messageList.removeAt(index);
                messageList.insert(index, message);
              });
            }
          });
        }
//        setState(() {
//
//        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(friendUser != null ? friendUser.userName : ""),
          elevation:
              Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
          actions: <Widget>[
            new IconButton(icon: new Icon(Icons.exit_to_app), onPressed: null)
          ],
        ),
        body: new Container(
          child: new Column(
            //mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _isInitialListLoading ? _getInitialProgressBarWidget() : Center(),
              new Expanded(
//                    alignment: Alignment.center,
                  child: new Align(
                alignment: Alignment.center,
                child: LazyLoadScrollView(
                  onEndOfPage:
                      _getFriendsFromServerCalled ? _lazyLoading : () {},
                  child: ListView.builder(
                    itemCount: messageList.length,
                    //comparing timestamp of messages to check which one would appear first
                    itemBuilder: (context, int index) {
                      if(_isSenderCurrentUser(
                          messageList[index].sender,
                          messageList[index].receiver)){
                        print("sender of Message is current user");
                        if(!messageList[index].isRead){
                          //Send a request to the server
                          MethodChannelUtility.readMessage(messageList[index]);
                          print("Message not read --- ");
                        }
                      }
                      return new ChatMessageListItem(
                        messageSnapshot: messageList[index],
                        currentuser: currentUser,
                        useSentLayout: _isSenderCurrentUser(
                            messageList[index].sender,
                            messageList[index].receiver),
                        isRead: false,
                      );
                    },
                  ),
                ),
              )),
              _isSubsequentListLoading ? _getBottomProgressbar() : Center(),
              new Divider(height: 1.0),
              new Container(
                decoration:
                    new BoxDecoration(color: Theme.of(context).cardColor),
                child: _buildTextComposer(),
              ),
              new Builder(builder: (BuildContext context) {
                _scaffoldContext = context;
                return new Container(width: 0.0, height: 0.0);
              })
            ],
          ),
          decoration: Theme.of(context).platform == TargetPlatform.iOS
              ? new BoxDecoration(
              image: new DecorationImage(
                colorFilter: new ColorFilter.mode(
                    Colors.black.withOpacity(0.2), BlendMode.dstATop),
                image: new AssetImage('images/games.jpg'),
                fit: BoxFit.cover,
              ),
              border: new Border(
                  top: new BorderSide(
                    color: Colors.grey[200],
                  )))
              : new BoxDecoration(
              image: new DecorationImage(
                colorFilter: new ColorFilter.mode(
                    Colors.black.withOpacity(0.2), BlendMode.dstATop),
                image: new AssetImage('images/games.jpg'),
                fit: BoxFit.cover,
              )),
        ));
  }

  _getInitialProgressBarWidget() {
    return new Container(
      color: Colors.black54,
      padding: EdgeInsets.only(left: 32.0, right: 16.0),
      height: 25.0,
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          new Text(
            "Loading.... ",
            style: new TextStyle(color: Colors.white, fontSize: 18.0),
          ),
          new SizedBox.fromSize(
            size: new Size(15.0, 15.0),
            child: new CircularProgressIndicator(
              strokeWidth: 2.5,
            ),
          )
        ],
      ),
    );
  }

  CupertinoButton getIOSSendButton() {
    return new CupertinoButton(
      child: new Text("Send"),
      onPressed: _isComposingMessage
          ? () => _textMessageSubmitted(_textEditingController.text)
          : null,
    );
  }

  IconButton getDefaultSendButton() {
    return new IconButton(
      icon: new Icon(Icons.send),
      onPressed: _isComposingMessage
          ? () => _textMessageSubmitted(_textEditingController.text)
          : null,
    );
  }

  Widget _buildTextComposer() {
    return new IconTheme(
        data: new IconThemeData(
          color: _isComposingMessage
              ? Theme.of(context).accentColor
              : Theme.of(context).disabledColor,
        ),
        child: new Container(
          margin: const EdgeInsets.symmetric(horizontal: 8.0),
          child: new Row(
            children: <Widget>[
              new Container(
                margin: new EdgeInsets.symmetric(horizontal: 4.0),
                child: new IconButton(
                    icon: new Icon(
                      Icons.photo_camera,
                      color: Theme.of(context).accentColor,
                    ),
                    onPressed: () async {
                      /** await _ensureLoggedIn();
                      File imageFile = await ImagePicker.pickImage();
                      int timestamp = new DateTime.now().millisecondsSinceEpoch;
                      StorageReference storageReference = FirebaseStorage
                          .instance
                          .ref()
                          .child("img_" + timestamp.toString() + ".jpg");
                      StorageUploadTask uploadTask =
                          storageReference.put(imageFile);
                      Uri downloadUrl = (await uploadTask.future).downloadUrl;
                      _sendMessage(
                          messageText: null, imageUrl: downloadUrl.toString()); **/
                    }),
              ),
              new Flexible(
                child: new TextField(
                  controller: _textEditingController,
                  onChanged: (String messageText) {
                    setState(() {
                      _isComposingMessage = messageText.length > 0;
                    });
                  },
                  onSubmitted: _textMessageSubmitted,
                  decoration:
                      new InputDecoration.collapsed(hintText: "Send a message"),
                ),
              ),
              new Container(
                margin: const EdgeInsets.symmetric(horizontal: 4.0),
                child: Theme.of(context).platform == TargetPlatform.iOS
                    ? getIOSSendButton()
                    : getDefaultSendButton(),
              ),
            ],
          ),
        ));
  }

  Future<Null> _textMessageSubmitted(String text) async {
    Message message = new Message();
    //message.dateCreated = DateTime.now();
    message.sender = currentUser;
    message.receiver = friendUser;
    message.isRead = false;
    //TODO: Make sure users cant send empty messages
    message.message = _textEditingController.text.trim();
    message.dateCreated = DateTime.now().millisecondsSinceEpoch;
    _textEditingController.clear();

    setState(() {
      messageList.add(message);
      _isComposingMessage = false;
    });

//await _ensureLoggedIn();


    _sendMessage(message);

  }

  void _sendMessage(Message message) async {
    print("_sendMessage() Called --------------------- ");
    bool isSuccessful = await MethodChannelUtility.sendMessage(message);

    print("_sendMessage ---------*******------- " + isSuccessful.toString());
  }

  ///-------------------------------------------------------------------------
  ///This method checks to see if the current logged in account is the sender of
  ///the last message, returns true or is the receiver of the last message and returns false
  ///-------------------------------------------------------------------------
  bool _isSenderCurrentUser(User sender, User receiver) {
    if (sender.id == currentUser.id) return true;
    if (receiver.id == currentUser.id) return false;
    return false;
  }

  bool _isReceiverCurrentUser(User sender, User receiver) {
    if (receiver.id == currentUser.id) return true;
    if (sender.id == currentUser.id) return false;
  }

  Widget _getBottomProgressbar() {
    return new LinearProgressIndicator();
  }

  ///-------------------------------------------------------------------------
  ///This method sets state setting _initialListLoading bool var to false
  ///so as to control the progressbar indicating initial list items are being
  ///fetched from the api
  ///-------------------------------------------------------------------------
  _setInitialListLoadingFalse() {
    setState(() {
      _isInitialListLoading = false;
    });
  }

  ///This method sets state setting _initialListLoading bool var to true
  ///so as to control the progressbar indicating initial list items are being
  ///fetched from the api
  _setInitialListLoadingTrue() {
    setState(() {
      _isInitialListLoading = true;
    });
  }

  void _setSubsequentListLoadingTrue() {
    setState(() {
      _isSubsequentListLoading = true;
    });
  }

  void _setSubsequentListLoadingFalse() {
    setState(() {
      _isSubsequentListLoading = false;
    });
  }

  void _lazyLoading() {
    endOfPageCalled = endOfPageCalled + 1;
    print("===========================================================");
    print("LazyMethod Called!! --- end of Page");
    print("===========================================================");
    if (!(endOfPageCalled > 1)) //This is so to prevent multiple calling
    if (!(currentPageNumber >= totalNumberOfPages)) {
      print(
          "currentPageNumber NOT greater than or equal to totalNumberOfPages -- ");
      currentPageNumber = currentPageNumber + 1;
      _getSubsequentMessagesFromServer(pageNum: currentPageNumber);
    }
  }

  _getMessagesFromDatabase() {
    //TODO: If messages gotten from database == 0,
    _getInitialMessagesFromServer();
  }

  ///This method gets all friends list from Db while getting the message list from
  ///the server... then crosschecks to ensure all messages from server are int the
  ///database if not it adds the missing message in the db
  _crossCheckMessageListInDbAddMissing() {}

  _getInitialMessagesFromServer() async {
    setState(() {
      _getFriendsFromServerCalled = true;
    });
    _setInitialListLoadingTrue();

    ApiHelper apiHelper = new ApiHelper();
    if (friendUser != null) {
      //_setInitialListLoadingFalse(); //TODO: Test purpose only
      print("friendUser != null -- ");
      HttpClientResponse response =
          await apiHelper.getMessageModel(friendUser.id, 1);

      //var mm = json.decode(response.body);

      String mm = await response.transform(utf8.decoder).join();
      print(mm);
      //if (response.statusCode == 200) {
      print("statusCode == 200");
      _setInitialListLoadingFalse();
      final jsonString = json.decode(mm);
      MessagePaginationModel messagePaginationModel =
          MessagePaginationModel.fromJson(jsonString);
      totalNumberOfPages = messagePaginationModel.totalPage;
      setState(() {
        messageList.clear();
        messagePaginationModel.content.forEach((messages) {
          messageList.add(messages);
        });
      });

      //final userBean = UserBean(_adapter);

      //userBean.insertMany(friendList);
//      } else
//        _setInitialListLoadingFalse();
    } else
      print("friendUser == null -- ");
  }

  _getSubsequentMessagesFromServer({int pageNum}) async {
    _setSubsequentListLoadingTrue();

    ApiHelper apiHelper = new ApiHelper();
    if (friendUser != null) {
      HttpClientResponse response =
          await apiHelper.getMessageModel(friendUser.id, pageNum);

      //var mm = json.decode(response.body);

      String mm = await response.transform(utf8.decoder).join();

      var jsonString = json.decode(mm);
      if (response.statusCode == 200) {
        _setInitialListLoadingFalse();
        MessagePaginationModel messagePaginationModel =
            MessagePaginationModel.fromJson(jsonString);
        totalNumberOfPages = messagePaginationModel.totalPage;
        setState(() {
          messagePaginationModel.content.forEach((messages) {
            messageList.add(messages);
          });
        });

        //final userBean = UserBean(_adapter);

        //userBean.insertMany(friendList);
      } else
        _setSubsequentListLoadingFalse();
    }
  }
}
