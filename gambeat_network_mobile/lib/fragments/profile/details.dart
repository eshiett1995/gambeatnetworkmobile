import 'package:flutter/material.dart';

import '../../custom-widgets/custom_gender_radio_buttons.dart';

class Details extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(10.0),
          child: Form(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(
                  height: 20.0,
                ),
                Container(
                  constraints: BoxConstraints(maxHeight: 55.0),
                  decoration: BoxDecoration(
                    color: Colors.white70,
                    border: Border.all(
                      color: Colors.blue.withOpacity(0.7),
                      width: 1.3,
                    ),
                    borderRadius: BorderRadius.circular(1.0),
                    
                  ),
                  margin: const EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 20.0),
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: 60.0,
                        width: 45.0,
                        color: Colors.blue.withOpacity(0.7),
                        child: Icon(
                          Icons.account_circle,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      new Expanded(
                        child: TextField(
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              labelStyle: TextStyle(),
                              labelText: "First Name"),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white70,
                    border: Border.all(
                      color: Colors.blue.withOpacity(0.7),
                      width: 1.3,
                    ),
                    borderRadius: BorderRadius.circular(1.0),
                  ),
                  margin: const EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 20.0),
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: 60.0,
                        width: 45.0,
                        color: Colors.blue.withOpacity(0.7),
                        child: Icon(
                          Icons.email,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      new Expanded(
                        child: TextField(
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              labelStyle: TextStyle(),
                              labelText: "First Name"),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
