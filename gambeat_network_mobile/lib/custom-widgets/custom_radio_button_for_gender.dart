import 'package:custom_radio/custom_radio.dart';
import 'package:flutter/material.dart';

class CustomRadioButton extends StatefulWidget {
  final GenderSelectionCallback genderSelectionCallback;
  List<String> values = [];
  List<Widget> buttonWidgets = [];

  CustomRadioButton(
      {Key key,
      this.genderSelectionCallback,
      this.radioVal,
      this.values,
      this.buttonWidgets})
      : super(key: key);
  String radioVal = 'Non';
  @override
  _CustomRadioButtonState createState() => _CustomRadioButtonState();
}

class _CustomRadioButtonState extends State<CustomRadioButton>
    with SingleTickerProviderStateMixin {
  RadioBuilder<String, dynamic> dynamicBuilder;
  AnimationController _controller;
  Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
        vsync: this, duration: new Duration(milliseconds: 100));

    _animation = CurvedAnimation(parent: _controller, curve: Curves.ease);

    _controller.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  _CustomRadioButtonState() {
    dynamicBuilder = (BuildContext context, List<dynamic> animValues,
        Function updateState, String value) {
      if (widget.buttonWidgets == null && value == "Male" ||
          value == "Female") {
        return GestureDetector(
          onTap: () {
            setState(() {
              widget.radioVal = value;
              widget.genderSelectionCallback(value);
              print("RadioButton Tapped --- Value " + value);
            });
          },
          child: Container(
            alignment: Alignment.center,
            //margin: EdgeInsets.symmetric(horizontal: 32.0, vertical: 6.0),
            margin: EdgeInsets.only(
                left: value == widget.values[0] ? 0.0 : 32.0,
                top: 8.0,
                bottom: 8.0),
            padding: EdgeInsets.all(10.0 + animValues[0] * 15.0),
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: animValues[1],
                border: Border.all(color: animValues[2], width: 2.0)),
            child: value == widget.values[0]
                ? new Image.asset(
                    "images/masculine.png",
                    color: animValues[2],
                    height: 45.0,
                    width: 45.0,
                  )
                : value == widget.values[1]
                    ? new Image.asset(
                        "images/femenine.png",
                        color: animValues[2],
                        height: 45.0,
                        width: 45.0,
                      )
                    : Icons.print,
          ),
        );
      } else {
        return GestureDetector(
          onTap: () {
            setState(() {
              widget.radioVal = value;
              widget.genderSelectionCallback(value);
              print("RadioButton Tapped --- Value " + value);
            });
          },
          child: Container(
            alignment: Alignment.center,
            //margin: EdgeInsets.symmetric(horizontal: 32.0, vertical: 6.0),
            margin: EdgeInsets.only(
                left: value == widget.values[0] ? 0.0 : 32.0,
                top: 8.0,
                bottom: 8.0),
            padding: EdgeInsets.all(18.0 + animValues[0] * 10.0),
            decoration: BoxDecoration(
//              borderRadius: BorderRadius.circular(10.0),
                shape: BoxShape.circle,
                color: animValues[1],
                border: Border.all(color: animValues[2], width: 2.0)),
            child: value == widget.values[0]
                ? new Center(

                    child: new Text(
                      "₦ 500",
                      style:
                          new TextStyle(color: animValues[2], fontSize: 18.0),
                    ))
                : value == widget.values[1]
                    ? new Center(

                        child: new Text(
                          "₦ 1000",
                          style: new TextStyle(
                              color: animValues[2], fontSize: 18.0),
                        ))
                    : Icons.print,
          ),
        );
      }
    };
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          CustomRadio<String, dynamic>(
            key: ValueKey(widget.values[0]),
            value: widget.values[0],
            groupValue: widget.radioVal,
            //duration: Duration(milliseconds: 800),
            animsBuilder: (AnimationController controller) => [
                  CurvedAnimation(
                    parent: controller,
                    curve: Curves.easeInOut,
                  ),
                  ColorTween(
                          begin: Colors.white,
                          end: Colors.blueAccent.withAlpha(200))
                      .animate(controller),
                  ColorTween(
                          begin: Colors.blueAccent.withAlpha(200),
                          end: Colors.white)
                      .animate(controller),
                ],
            builder: dynamicBuilder,
          ),
          CustomRadio<String, dynamic>(
            key: ValueKey(widget.values[1]),
            value: widget.values[1],
            groupValue: widget.radioVal,
            //duration: Duration(milliseconds: 800),
            animsBuilder: (AnimationController controller) => [
                  CurvedAnimation(
                    parent: controller,
                    curve: Curves.easeInOut,
                  ),
                  ColorTween(
                          begin: Colors.white,
                          end: Colors.blueAccent.withAlpha(200))
                      .animate(controller),
                  ColorTween(
                          begin: Colors.blueAccent.withAlpha(200),
                          end: Colors.white)
                      .animate(controller),
                ],
            builder: dynamicBuilder,
          ),
        ],
      ),
    );
  }
}

typedef GenderSelectionCallback = void Function(String gender);
