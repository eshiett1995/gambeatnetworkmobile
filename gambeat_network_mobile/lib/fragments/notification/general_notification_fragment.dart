
import 'dart:convert';
import 'dart:io';

import 'package:Gambeat/models/Enum.dart';
import 'package:Gambeat/models/rest-models/response/NotificationPaginationModel.dart';
import 'package:Gambeat/models/GeneralNotification.dart';
import 'package:Gambeat/network/api/ApiHelper.dart';
import 'package:flutter/material.dart';

class GeneralNotificationFragment extends StatefulWidget {

  @override
  _GeneralNotificationFragmentState createState() => _GeneralNotificationFragmentState();

}

class _GeneralNotificationFragmentState extends State<GeneralNotificationFragment> {

  NotificationPaginationModel notificationPaginationModel;

  void getNotificationsFromServer() async{

    HttpClientResponse notificationModelResponse = await new ApiHelper().getNotificationsByPage(1);

    String response = await notificationModelResponse.transform(utf8.decoder).join();

    if (notificationModelResponse.statusCode == 200) {

      final responseJson = json.decode(response);

      notificationPaginationModel = NotificationPaginationModel.fromJson(json.decode(responseJson));

    } else {

    }

  }

  @override
  void initState(){

     super.initState();

     getNotificationsFromServer();

  }

  void showNotificationInfo(GeneralNotification generalNotification) {
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return new Container(
              padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
              color: Colors.white,
              child: ListView(

                children: <Widget>[
                  new Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(
                        height: 20.0,
                      ),

                      Row(
                        children: <Widget>[
                          Text("Hello ${generalNotification.user.userName}", style: TextStyle(fontWeight: FontWeight.bold),),

                          Spacer(),

                          new Image.asset('images/scroll.gif', width: 50.0, height: 50.0,),

                        ],
                      ),

                      SizedBox(height: 20.0,),

                      Text(generalNotification.notificationMessage,

                        textAlign: TextAlign.justify,
                        style: TextStyle(
                            height: 1.3,
                            color: Colors.black87
                        ),
                      ),

                      SizedBox(height: 30.0,),

                      new Visibility(
                        child: new RaisedButton(
                            onPressed: (){},
                            color: Colors.blue,
                            child: Container(
                              width: double.infinity,
                              height: 50.0,
                              child: Center(
                                child: Text("${generalNotification.reference == null ? "" : generalNotification.reference.userName} PROFILE", style: TextStyle(color: Colors.white),),
                              ),
                            )
                        ),
                        visible: generalNotification.reference != null,
                      ),

                      SizedBox(height: 30.0,),

                      new Visibility(
                        child: new RaisedButton(
                            onPressed: (){},
                            color: Colors.blue,
                            child: Container(
                              width: double.infinity,
                              height: 50.0,
                              child: Center(
                                child: Text("VIEW COMPETITION", style: TextStyle(color: Colors.white),),
                              ),
                            )
                        ),
                        visible: generalNotification.competition != null,
                      ),




                    ],
                  ),
                ],

              )
          );
        });
  }


  Widget getNotificationPanel(GeneralNotification generalNotification){

    return GestureDetector(
      onTap: (){showNotificationInfo(generalNotification);},

      child: new Container(
        padding: EdgeInsets.only(right: 10.0),
        color: Colors.white,
        child: new Row(
          children: <Widget>[

            new Container(
              padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 15.0),
              child: generalNotification.notificationType == NotificationType.FRIENDELIMINATED ?
                   new Image.asset('images/competition-started.png', width: 130.0, height: 30.0,) :
              generalNotification.notificationType == NotificationType.TOURNAMENTENDED ?
                   new Image.asset('images/competition-ended.png', width: 130.0, height: 30.0,) :
              generalNotification.notificationType == NotificationType.TOURNAMENTWINNER ?
                   new Image.asset('images/winner.png', width: 130.0, height: 30.0,) :
              generalNotification.notificationType == NotificationType.FRIENDHASMATCH ?
                   new Image.asset('images/competition-started.png', width: 130.0, height: 30.0,) :
              generalNotification.notificationType == NotificationType.TOURNAMENTSTARTED ?
                   new Image.asset('images/competition-started.png', width: 130.0, height: 30.0,) :
              generalNotification.notificationType == NotificationType.PLAYERWINSMATCH ?
                   new Image.asset('images/competition-started.png', width: 130.0, height: 30.0,) :
              generalNotification.notificationType == NotificationType.PLAYERKNOCKEDOUT ?
                   new Image.asset('images/player-knocked-out.png', width: 130.0, height: 30.0,) :
              generalNotification.notificationType == NotificationType.FRIENDKNOCKEDOUT ?
                   new Image.asset('images/player-knocked-out.png', width: 130.0, height: 30.0,) :
              generalNotification.notificationType == NotificationType.FRIENDREQUESTACCEPTED ?
                   new Image.network(generalNotification.reference.picture, width: 130.0, height: 30.0,) :
              generalNotification.notificationType == NotificationType.TRANSACTION ?
                   new Image.asset('images/competition-started.png', width: 130.0, height: 30.0,) :
              generalNotification.notificationType == NotificationType.FRIENDWINSTOURNAMENT ?
                   new Image.asset('images/same-feather.png', width: 130.0, height: 30.0,) :
                   new Image.asset('images/competition-started.png', width: 130.0, height: 30.0,),
              width: 100.0,
              height: 100.0,
              margin: EdgeInsets.only(right: 5.0),

            ),


            new Expanded(child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                Text(
                  generalNotification.notificationHeader,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),

                SizedBox(height: 8.0,),

                Text(
                  generalNotification.notificationMessage,
                  style: TextStyle(
                      color: Colors.black26
                  ),
                ),

                SizedBox(height: 5.0,),

                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(generalNotification.dateCreated.toString())
                  ],

                ),
              ],
            ))
          ],
        ),
      ),
    );
  }



  @override
  Widget build(BuildContext context) {

      return new Container(
      decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage('images/background_image.jpeg'),
            fit: BoxFit.cover,)),
      child: ListView(

        children: <Widget>[


          new Container(

            color: Colors.white,

            padding: EdgeInsets.only(right: 10.0),

            child: Row(
              children: <Widget>[

                Container(
                  padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 15.0),
                  child: new Image.asset('images/chris.jpg', width: 130.0, height: 30.0,),
                  width: 100.0,
                  height: 100.0,
                  margin: EdgeInsets.only(right: 5.0),

                ),


                Expanded(child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[

                    Text(
                      "Friendship",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),

                    SizedBox(height: 8.0,),

                    Text(
                      "Hello Bassey, you friend Imabong just won a competition, now i am sure birds of the same feather, flock together. ",
                      style: TextStyle(
                          color: Colors.black26
                      ),
                    ),

                    SizedBox(height: 5.0,),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text("wed 12 2018")
                      ],

                    ),
                  ],
                ))
              ],
            ),
          ),
        ],
      ),
    );

  }

}
