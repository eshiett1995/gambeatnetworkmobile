import 'dart:core';

import 'package:Gambeat/models/DefaultEntity.dart';

class PlayerStats {
  PlayerStats();

  int matchPlayed;

  int wins;

  int loss;

  double wallet;

  double profit;

  double expenses;

  int totalMessage;

  int unreadMessage;

  int readMessage;

  int referrer;

  int friends;

  int referrals;

  factory PlayerStats.fromJson(Map<String, dynamic> parsedJson) {

    PlayerStats user = new PlayerStats();

    user.matchPlayed = parsedJson['matchPlayed'];

    user.wins = parsedJson['wins'];

    user.loss = parsedJson['loss'];

    user.wallet = parsedJson['wallet'].toDouble();

    user.profit = parsedJson['profit'].toDouble();

    user.expenses = parsedJson['expenses'].toDouble();

    user.totalMessage = parsedJson['totalMessage'];

    user.unreadMessage = parsedJson['unreadMessage'];

    user.readMessage = parsedJson['readMessage'];

    user.friends = parsedJson['friends'];


    return user;
  }

  Map<String, dynamic> toJson() => {

        '"matchPlayed"': matchPlayed,

        '"wins"': wins,

        '"loss"': loss,

        '"wallet"': wallet,

        '"profit"': profit,

        '"expenses"': expenses,

        '"totalMessage"': totalMessage,

        '"unreadMessage"': unreadMessage,

        '"readMessage"': readMessage,

        '"referrer"': referrer,

        '"friends"': friends,

        '"referrals"': referrals,

      };
}
