import 'package:badge/badge.dart';
import 'package:Gambeat/helper/GetTimeAgo.dart';
import 'package:Gambeat/models/Message.dart';
import 'package:Gambeat/models/User.dart';
import 'package:flutter/material.dart';

class ChatListItem extends StatefulWidget {
  Map<User, List<Message>> map;
  User friend;
  AnimationController controller;
  User currentUser;

  var itemTappedFunc;

  ChatListItem(
      {this.map,
      this.friend,
      this.controller,
      this.currentUser,
      this.itemTappedFunc});

  @override
  _ChatListItemState createState() => _ChatListItemState();
}

Message message;

class _ChatListItemState extends State<ChatListItem>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    var map = widget.map;
    var friend = widget.friend;
    message = map[friend][map[friend].length - 1];
    return _getListItemWidget();
  }

  String time = "";

  Widget _getListItemWidget() {
    var map = widget.map;
    var friend = widget.friend;
    //String messageCount = message.;
    String messageStr = message.message;
    time = getTimeAgo(new DateTime.fromMillisecondsSinceEpoch(message.dateCreated));
//    var  time = getTimeAgoF.getTimeAgo();
    String unreadMsgCount;
    int unreadMsgCountInt;
    User user = new User();
    bool isLastMsgSenderTheCurrentUser =
        _getSenderIsCurrentUser(message.sender, message.receiver);
    if (isLastMsgSenderTheCurrentUser) {
      user = message.sender;
      unreadMsgCount = null;
    } else {
      user = message.receiver;
      unreadMsgCount = user.unreadMessagesCount.toString();
      unreadMsgCountInt = user.unreadMessagesCount;
    }
    return GestureDetector(
      onTap: () {
        widget.itemTappedFunc();
      },
      child: Card(
        margin: EdgeInsets.symmetric(vertical: 15.0, horizontal: 10.0),
        elevation: 3.0,
        child: Padding(
            padding: EdgeInsets.all(15.0),
            child: Row(
              children: <Widget>[
                //If unreadMsgCount is null (no unread message(s)) hide the badge
                //else show it
                _getBadge(unreadMsgCount != null && unreadMsgCountInt != 0,
                    unreadMsgCount),
                Padding(
                  padding: EdgeInsets.only(right: 10.0),
                ),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Stack(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  time,
                                  textDirection: TextDirection.ltr,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(fontFamily: "Montserrat", fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Text(
                                  friend.userName,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(fontFamily: "Montserrat", fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 7.0,
                      ),
                      Text(
                        messageStr,
                        textAlign: TextAlign.left,
                        style: TextStyle(color: Colors.black54),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                )
              ],
            )),
      ),
    );
  }

  ///When there's no unread message this simply returns a widget showing the
  ///profile if unread message count != 0 it returns a badge widget showing the
  /// unread message count
  _getBadge(bool hasUnreadMsg, String unreadMsgCount) {
    if (hasUnreadMsg)
      return new Badge(
        color: Colors.blue,
        borderSize: 1.0,
        positionTop: 0.0,
        textStyle: TextStyle(
          color: Colors.white,
        ),
        positionRight: -3.0,
        borderColor: Colors.black,
        value: unreadMsgCount,
        // value to show inside the badge
        child: new CircleAvatar(
          radius: 35.0,
          backgroundImage: AssetImage('images/chris.jpg'),
        ), // text to append (required)
      );
    else
      return new CircleAvatar(
        radius: 35.0,
        backgroundImage: AssetImage('images/chris.jpg'),
      );
  }

  ///-------------------------------------------------------------------------
  ///This method checks to see if the current logged in account is the sender of
  ///the last message, returns true or is the receiver of the last message and returns false
  ///-------------------------------------------------------------------------
  bool _getSenderIsCurrentUser(User sender, User receiver) {
    if (sender.id == widget.currentUser.id) return true;
    if (receiver.id == widget.currentUser.id) return false;
    return false;
  }
}
