import 'package:Gambeat/models/FriendRequest.dart';
import 'package:Gambeat/models/rest-models/response/ResponseModel.dart';

class FriendRequestResponseModel {

  List<FriendRequest> content;

  ResponseModel responseModel;

  FriendRequestResponseModel();

  factory FriendRequestResponseModel.fromJson(Map<String, dynamic> parsedJson) {
    FriendRequestResponseModel friendRequestResponseModel =
        new FriendRequestResponseModel();

    friendRequestResponseModel.responseModel = ResponseModel.fromJson(parsedJson['responseModel']);

    var list = parsedJson['content'] as List;

    friendRequestResponseModel.content = list.map((i) => FriendRequest.fromJson(i)).toList();

    return friendRequestResponseModel;
  }
}
