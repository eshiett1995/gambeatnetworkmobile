import 'package:Gambeat/models/User.dart';
import 'package:Gambeat/models/rest-models/response/ResponseModel.dart';

class LoginModel {
  
  LoginModel();

  LoginModel.make(this.user,this.responseModel);

  User user;

  ResponseModel responseModel;

  factory LoginModel.fromJson(Map<String, dynamic> parsedJson) {

    LoginModel loginModel = new LoginModel();

    

    loginModel.user =  User.fromJson(parsedJson['user']);

    loginModel.responseModel = ResponseModel.fromJson(parsedJson['responseModel']);

    return loginModel;

  }


}
