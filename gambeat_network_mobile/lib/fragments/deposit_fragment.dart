import 'dart:convert';
import 'dart:io';

import 'package:Gambeat/helper/SharedPreferenceHelper.dart';
import 'package:Gambeat/models/Transactions.dart';
import 'package:Gambeat/models/User.dart';
import 'package:Gambeat/network/api/ApiHelper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_paystack/flutter_paystack.dart';
import 'package:intl/intl.dart';

//import 'package:flutter_sweet_alert/flutter_sweet_alert.dart';

class DepositFragment extends StatefulWidget {
  @override
  _DepositFragment createState() => new _DepositFragment();
}

class _DepositFragment extends State<DepositFragment> {

  bool isMakingTransaction = false;

  TextEditingController depositAmountController = new TextEditingController();
  String _inputDate = "8/2018";
  int _inputExpireYear = 0, _inputExpireMonth = 0;
  var paystackPublicKey = 'pk_test_3a7af4a93d785ab7fb183ee27eeae4be3755340e';
  var paystackSecretKey = 'sk_test_087c1932101cb6688dc4e14692ecc778c71491b3';

  @override
  void initState() {
    PaystackPlugin.initialize(publicKey: paystackPublicKey, secretKey: paystackSecretKey);
        //_makePayment();
    // secretKey is not needed if you're not using checkout as a payment method.
    //
  }

  Widget _buildInitDepositButton(){

    if(!isMakingTransaction){

      return  new Container(
        margin: EdgeInsets.only(left: 8.0),
        width: 200.0,
        alignment: Alignment.center,
        child: new Row(
          children: <Widget>[
            new Expanded(
              child:new RaisedButton(
                padding:
                EdgeInsets.symmetric(vertical: 15.0),
                shape: new RoundedRectangleBorder(
                  borderRadius:
                  new BorderRadius.circular(30.0),
                ),
                color: Colors.blue,
                onPressed: () => initializeDeposit(),
                child: new Container(
                  child: new Row(
                    mainAxisAlignment:
                    MainAxisAlignment.spaceEvenly,
                    children: <Widget>[



                      Text(
                        "Make Deposit",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ),
            )],),);

    }else{

      return Container(
        height: 48.0,
        width: 48.0,
        child: RaisedButton(
          padding: EdgeInsets.all(0.0),
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0),
          ),
          child: SizedBox(
              height: 30.0,
              width: 30.0,
              child: Stack(
                children: <Widget>[
                  CircularProgressIndicator(
                    value: null,
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                  ),
                  Positioned(
                    child: Icon(Icons.credit_card, size: 18.0, color: Colors.white,),
                    left: 0.0,
                    right: 0.0,
                    top: 0.0,
                    bottom: 0.0,
                  )
                ],
              )
          ),
          disabledColor: Colors.blue,
          onPressed: null,
        ),
      );

    }
  }

  _makePayment(Transactions transaction,{Charge charge}) async {
    PaystackPlugin.initialize(
        publicKey: paystackPublicKey, secretKey: paystackSecretKey);
    //  Charge charge = Charge()
    //    ..amount = 10000
    //    ..reference = _getReference()
    //  // or ..accessCode = _getAccessCodeFrmInitialization()
    //    ..email = 'customer@email.com';
    Charge charge = new Charge();

    charge.email = transaction.player.email;
      charge.amount = transaction.amount.toInt();
      charge.reference = _getReference();
    CheckoutResponse response = await PaystackPlugin.checkout(
      context,
      method: CheckoutMethod.selectable,
      charge: charge,
      fullscreen: true,
    );
    print("Card name oooh ---- " + response.card.name);
  }

  String _getReference() {
    return "bb";
  }

  void initializeDeposit()async {

    isMakingTransaction = true;

    setState((){});

    Transactions transaction = new Transactions();

    transaction.amount = int.tryParse(depositAmountController.text);

    HttpClientResponse transactionResponse = await new ApiHelper()
        .initTransaction(transaction);

    if (transactionResponse.statusCode == 200) {
      String transactionResponseString = await transactionResponse.transform(
          utf8.decoder).join();

      print(transactionResponseString);

      transaction = new Transactions.fromJson(json.decode(transactionResponseString));

      _makePayment(transaction);
    }
    else {


    }

    isMakingTransaction = false;

    setState((){});
  }

//  static String validateCardNum(String input, PaymentCard card) {
//    if (input.isEmpty) {
//      return Strings.invalidNumber;
//    }
//
//    input = CardUtils.getCleanedNumber(input);
//
//    return card.validNumber(input) ? null : Strings.invalidNumber;
//  }

  @override
  Widget build(BuildContext context) {
    // _makePayment();
    // TODO: implement build
    // return _getDepositWidget();
    return new Scaffold
      (
        resizeToAvoidBottomPadding: false,
        //floatingActionButton: new FloatingActionButton(
         //child: new Icon(Icons.payment),
         //onPressed: (){
         // _makePayment();
         //},
      //),
      body: new Container(
        decoration: new BoxDecoration(
            color: Colors.white,
            image: new DecorationImage(

              image: new AssetImage('images/background_main.png'),
              fit: BoxFit.cover,
            )
        ),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Enter a deposit amount"),
            Padding(
              padding: EdgeInsets.all(20.0),
              child: TextFormField(
                controller: depositAmountController,
              ),
            ),
            _buildInitDepositButton()
          ],
        ),
      )
    );
  }

  /**_getDepositWidget() {
    DateTime _date = new DateTime.now();

    TimeOfDay _time = new TimeOfDay.now();

    Future<Null> _selectedDate(BuildContext context) async {
      _inputDate = "picked.toString()";

      final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: new DateTime(2016),
        lastDate: new DateTime(_date.year + 4),
      );

      setState(() {
        var formatter = new DateFormat("MM/yy");
        String newFormat = formatter.format(picked);
        print("New Format --- " + newFormat.toString());
        _inputExpireYear = int.parse(newFormat.substring(0, 2));
        _inputExpireMonth = int.parse(newFormat.substring(3, 5));
        print(
            "InputExpireYear -- $_inputExpireYear and InputExpireMonth -- $_inputExpireMonth");
        _inputDate = picked.month.toString() + "/" + picked.year.toString();
      });
    }

    var _formKey = new GlobalKey<FormState>();
    var _autoValidate = false;

    TextEditingController cardNumberContoller = new TextEditingController(),
        cardHolderNameContoller = new TextEditingController(),
        cvvController = new TextEditingController(),
        pinController = new TextEditingController(),
        expiringDateController = TextEditingController(text: _inputDate);

    return ListView(
      children: <Widget>[
//        new Form(
//          key: _formKey,
//          autovalidate: _autoValidate,
//          child:
        Container(
          margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 30.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Card(
                  elevation: 3.0,
                  child: Padding(
                    padding: EdgeInsets.only(
                        left: 20.0, top: 5.0, bottom: 5.0, right: 20.0),
                    child: Row(
                      children: <Widget>[
                        Image(
                          width: 80.0,
                          height: 50.0,
                          image: AssetImage('images/visa.png'),
                        ),
                        Spacer(),
                        Icon(
                          Icons.check_circle,
                          color: Colors.blue,
                          size: 30.0,
                        )
                      ],
                    ),
                  )),
              SizedBox(
                height: 30.0,
              ),
              Card(
                elevation: 4.0,
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 20.0,
                      ),
                      new TextField(
                        controller: cardNumberContoller,
//                          validator: (String value) => validateCardNum(value, card),
                        inputFormatters: [
                          WhitelistingTextInputFormatter.digitsOnly,
                          new LengthLimitingTextInputFormatter(19),
//                            new CardNumberInputFormatter()
                        ],
                        decoration: new InputDecoration(
                            enabledBorder: new OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.black12,
                                width: 1.0,
                              ),
                            ),
                            suffixIcon: Icon(Icons.confirmation_number),
                            contentPadding: const EdgeInsets.only(
                                left: 10.0, top: 10.0, bottom: 10.0),
                            border: new OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.black12,
                                width: 1.0,
                              ),
                              gapPadding: 1.0,
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(5.0),
                              ),
                            ),
                            filled: true,
                            labelText: "Card Number",
                            labelStyle: TextStyle(
                              fontSize: 13.0,
                            ),
                            fillColor: Colors.white70),
                      ),
                      SizedBox(
                        height: 35.0,
                      ),
                      new TextField(
                        controller: cardHolderNameContoller,
                        decoration: new InputDecoration(
                            enabledBorder: new OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.black12,
                                width: 1.0,
                              ),
                            ),
                            suffixIcon: Icon(Icons.person),
                            contentPadding: const EdgeInsets.only(
                                left: 10.0, top: 10.0, bottom: 10.0),
                            border: new OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.black12,
                                width: 1.0,
                              ),
                              gapPadding: 1.0,
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(5.0),
                              ),
                            ),
                            filled: true,
                            labelText: "Card Holder's Name",
                            labelStyle: TextStyle(
                              fontSize: 13.0,
                            ),
                            fillColor: Colors.white70),
                      ),
                      SizedBox(
                        height: 35.0,
                      ),
                      GestureDetector(
                          onTap: () {
                            print(_selectedDate(context));
                          },
                          child: Container(
                            color: Colors.transparent,
                            child: IgnorePointer(
                              child: new TextField(
                                controller: expiringDateController,
                                decoration: new InputDecoration(
                                    enabledBorder: new OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.black12,
                                        width: 1.0,
                                      ),
                                    ),
                                    suffixIcon: Icon(Icons.date_range),
                                    contentPadding: const EdgeInsets.only(
                                        left: 10.0, top: 10.0, bottom: 10.0),
                                    border: new OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.black12,
                                        width: 1.0,
                                      ),
                                      gapPadding: 1.0,
                                      borderRadius: const BorderRadius.all(
                                        const Radius.circular(5.0),
                                      ),
                                    ),
                                    filled: true,
                                    labelText: "Card Expiry Date",
                                    labelStyle: TextStyle(
                                      fontSize: 13.0,
                                    ),
                                    fillColor: Colors.white70),
                              ),
                            ),
                          )),
                      SizedBox(
                        height: 35.0,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: new TextField(
                              controller: cvvController,
                              inputFormatters: [
                                WhitelistingTextInputFormatter.digitsOnly,
                                new LengthLimitingTextInputFormatter(4),
                              ],
                              decoration: new InputDecoration(
                                  enabledBorder: new OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.black12,
                                      width: 1.0,
                                    ),
                                  ),
                                  suffixIcon: Icon(Icons.person),
                                  contentPadding: const EdgeInsets.only(
                                      left: 10.0, top: 10.0, bottom: 10.0),
                                  border: new OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.black12,
                                      width: 1.0,
                                    ),
                                    gapPadding: 1.0,
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(5.0),
                                    ),
                                  ),
                                  filled: true,
                                  labelText: "CVV",
                                  labelStyle: TextStyle(
                                    fontSize: 13.0,
                                  ),
                                  fillColor: Colors.white70),
                            ),
                          ),
                          SizedBox(
                            width: 20.0,
                          ),
                          Expanded(
                            child: new TextField(
                              controller: pinController,
                              inputFormatters: [
                                WhitelistingTextInputFormatter.digitsOnly,
                                new LengthLimitingTextInputFormatter(4),
                              ],
                              decoration: new InputDecoration(
                                  enabledBorder: new OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.black12,
                                      width: 1.0,
                                    ),
                                  ),
                                  suffixIcon: Icon(Icons.person),
                                  contentPadding: const EdgeInsets.only(
                                      left: 10.0, top: 10.0, bottom: 10.0),
                                  border: new OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.black12,
                                      width: 1.0,
                                    ),
                                    gapPadding: 1.0,
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(5.0),
                                    ),
                                  ),
                                  filled: true,
                                  labelText: "Pin",
                                  labelStyle: TextStyle(
                                    fontSize: 13.0,
                                  ),
                                  fillColor: Colors.white70),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 35.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Checkbox(
                            value: true,
                            onChanged: (bool newValue) {},
                          ),
                          Text(
                            "Save card information",
                            style: TextStyle(color: Colors.grey),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: new RaisedButton(
                              shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(5.0)),
                              color: Colors.blue,
                              textColor: Colors.purple,
                              onPressed: () async {
                                Charge charge = new Charge();
                                SharedPreferenceHelper sharedPref =
                                    new SharedPreferenceHelper();

                                await sharedPref.getUser().then((user) {
                                  charge.email = user.email;
                                  charge.amount = 100000;
                                  charge.reference = _getReference();
                                  charge.card = new PaymentCard(
                                      cvc: cvvController.text,
                                      number: cardNumberContoller.text,
                                      name: cardHolderNameContoller.text,
                                      expiryMonth: _inputExpireMonth,
                                      expiryYear: _inputExpireYear);
                                  _makePayment(charge: charge);
                                });
                              },
                              child: new Container(
                                padding: const EdgeInsets.symmetric(
                                  vertical: 20.0,
                                  horizontal: 20.0,
                                ),
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new Expanded(
                                      child: Text(
                                        "DEPOSIT MONEY",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 30.0,
              ),
            ],
          ),
        )
//        )
      ],
    );
  } **/
}
