
import 'package:Gambeat/helper/SharedPreferenceHelper.dart';
import 'package:Gambeat/home2.dart';
import 'package:flutter/material.dart';
import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';

import './login_screen_3.dart';


SqfliteAdapter _adapter;

void main() async {

  runApp(new MaterialApp(
    theme: ThemeData(fontFamily: 'Montserrat'),
    home: await new SharedPreferenceHelper().getIsLoggedInStatus() ? new HomePage2() : new LoginScreen3(),
    routes: <String, WidgetBuilder>{
      '/login': (BuildContext context) => new LoginScreen3(),
      '/register': (BuildContext context) => new LoginScreen3(),
      '/home': (BuildContext context) => new HomePage2(),
//
    },
  ));
}

ThemeData buildTheme() {
  final ThemeData base = ThemeData();
  return base.copyWith(
    hintColor: Colors.red,
    inputDecorationTheme: InputDecorationTheme(
      border: OutlineInputBorder(
        borderSide: BorderSide(
          color: Colors.blue,
          width: 0.5,
        ),
      ),
      labelStyle: TextStyle(color: Colors.yellow, fontSize: 24.0),
    ),
  );
}

class S extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.blueAccent,
      body: new Center(
        child: null,
      ),
    );
  }
}
