import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_paystack/flutter_paystack.dart';
import 'package:http/http.dart' as http;

class PaymentPage extends StatefulWidget {
  Charge charge;
  PaymentPage({this.charge});

 @override
 _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
 var paystackPublicKey = 'pk_test_3a7af4a93d785ab7fb183ee27eeae4be3755340e';
 var paystackSecretKey = 'sk_test_087c1932101cb6688dc4e14692ecc778c71491b3';

 get _method => null;

 @override
 void initState() {
   PaystackPlugin.initialize(
       publicKey: paystackPublicKey, secretKey: paystackSecretKey);
   // secretKey is not needed if you're not using checkout as a payment method.
   //
 }

 @override
 Widget build(BuildContext context) {

   _makePayment();

   return Scaffold(
     appBar: new AppBar(title: new Text("Paystack test"), actions: <Widget>[
       new IconButton(icon: new Icon(Icons.payment), onPressed: (){
         _makePayment();
       })
     ],),
   );
 }

 _makePayment() async {
   Charge charge = Charge()
     ..amount = 10000
     ..reference = _getReference()
   // or ..accessCode = _getAccessCodeFrmInitialization()
     ..email = 'customer@email.com';
   CheckoutResponse response = await PaystackPlugin.checkout(
     context,
     method: CheckoutMethod.selectable,
     charge: charge,
   );
//   response.
 }

 String _getReference() {
   return "Reference";
 }

}
