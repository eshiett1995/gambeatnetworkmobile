import 'package:custom_radio/custom_radio.dart';
import 'package:flutter/material.dart';

class CustomMessageSenderRadioButton extends StatefulWidget {
  final GenderSelectionCallback genderSelectionCallback;

  CustomMessageSenderRadioButton({Key key, this.genderSelectionCallback}) : super(key: key);
  String radioVal = 'EMAIL';
  @override
  _CustomMessageSenderRadioButtonState createState() => _CustomMessageSenderRadioButtonState();
}

class _CustomMessageSenderRadioButtonState extends State<CustomMessageSenderRadioButton>
    with SingleTickerProviderStateMixin {
  RadioBuilder<String, dynamic> dynamicBuilder;
  AnimationController _controller;
  Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
        vsync: this, duration: new Duration(milliseconds: 100));

    _animation = CurvedAnimation(parent: _controller, curve: Curves.ease);

    _controller.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  _CustomMessageSenderRadioButtonState() {
    dynamicBuilder = (BuildContext context, List<dynamic> animValues,
        Function updateState, String value) {
      return GestureDetector(
        onTap: () {
          setState(() {
            widget.radioVal = value;
            widget.genderSelectionCallback(value);
            print("RadioButton Tapped --- Value " + value);
          });
        },
        child: Container(
          alignment: Alignment.center,
          //margin: EdgeInsets.symmetric(horizontal: 32.0, vertical: 6.0),
          margin: EdgeInsets.only(
              left: value == 'EMAIL' ? 0.0 : 32.0, top: 8.0, bottom: 8.0),
          padding: EdgeInsets.all(10.0 + animValues[0] * 15.0),
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: animValues[1],
              border: Border.all(color: animValues[2], width: 2.0)),
          child: value == 'EMAIL'
              ? new Image.asset("images/email.png", color: animValues[2], height: 25.0, width: 25.0,)
              : value == 'SMS' ? new Image.asset("images/sms.png", color: animValues[2], height: 25.0, width: 25.0,) : Icons.print,
        ),
      );
    };
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          CustomRadio<String, dynamic>(
            key: ValueKey("email"),
            value: 'EMAIL',
            groupValue: widget.radioVal,
            //duration: Duration(milliseconds: 800),
            animsBuilder: (AnimationController controller) => [
              CurvedAnimation(
                parent: controller,
                curve: Curves.easeInOut,
              ),
              ColorTween(
                  begin: Colors.white,
                  end: Colors.blueAccent.withAlpha(200))
                  .animate(controller),
              ColorTween(
                  begin: Colors.blueAccent.withAlpha(200),
                  end: Colors.white)
                  .animate(controller),
            ],
            builder: dynamicBuilder,
          ),
          CustomRadio<String, dynamic>(
            key: ValueKey("sms"),
            value: 'SMS',
            groupValue: widget.radioVal,
            //duration: Duration(milliseconds: 800),
            animsBuilder: (AnimationController controller) => [
              CurvedAnimation(
                parent: controller,
                curve: Curves.easeInOut,
              ),
              ColorTween(
                  begin: Colors.white,
                  end: Colors.blueAccent.withAlpha(200))
                  .animate(controller),
              ColorTween(
                  begin: Colors.blueAccent.withAlpha(200),
                  end: Colors.white)
                  .animate(controller),
            ],
            builder: dynamicBuilder,
          ),
        ],
      ),
    );
  }
}

typedef GenderSelectionCallback = void Function(String gender);
