import 'dart:convert';

import 'package:Gambeat/Callbacks.dart';
import 'package:Gambeat/custom-widgets/badgeo.dart';
import 'package:Gambeat/custom-widgets/custom_dialog_box.dart';
import 'package:Gambeat/fragments/activities/competitions.dart';
import 'package:Gambeat/fragments/activities/games.dart';
import 'package:Gambeat/fragments/notification/gamification_fragment.dart';
import 'package:Gambeat/fragments/notification/general_notification_fragment.dart';
import 'package:Gambeat/fragments/social/users.dart';
import 'package:Gambeat/helper/method_channel_utility.dart';
import 'package:Gambeat/fragments/SwipeAnimation/index.dart';
import 'package:Gambeat/fragments/social/user_profile.dart';
import 'package:Gambeat/fragments/wallet/overview_fragment.dart';
import 'package:Gambeat/fragments/wallet/transactions_fragment.dart';
import 'package:Gambeat/helper/SharedPreferenceHelper.dart';
import 'package:Gambeat/models/User.dart';
import 'package:Gambeat/models/rest-models/request/FirebaseModel.dart';
import 'package:Gambeat/models/socketIO-models/SocketIOLogin.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:Gambeat/network/api/ApiHelper.dart';

import './fragments/home_fragment.dart';
import './fragments/settings_fragment.dart';
import './fragments/messages/chat-list.dart';
import './fragments/deposit_fragment.dart';
import './fragments/social/friends.dart';

import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:audioplayers/audio_cache.dart';

class DrawerItem {
  String title;
  IconData icon;

  DrawerItem(this.title, this.icon);
}

class Choice {
  const Choice({this.title, this.icon});

  final String title;
  final IconData icon;
}

class HomePage2 extends StatefulWidget {

  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();



  final drawerItems = [
    new DrawerItem("Home", Icons.dashboard),
    new DrawerItem("Profile", Icons.account_circle),
    new DrawerItem("Social", Icons.group),
    new DrawerItem("Wallet", Icons.credit_card),
    new DrawerItem("Messages", Icons.chat),
    new DrawerItem("Settings", Icons.settings),
    new DrawerItem("Transactions", Icons.settings),
    new DrawerItem("Chats", Icons.settings),
    new DrawerItem("Friend Request", Icons.people),
    new DrawerItem("Overview", Icons.people),
    new DrawerItem("General Notification", Icons.people),
    new DrawerItem("Gamification", Icons.people),
    new DrawerItem("Competition", Icons.people),
    new DrawerItem("Games", Icons.people),
    new DrawerItem("Users", Icons.people)
  ];

  final List<Choice> choices = const <Choice>[
    const Choice(
        title: CompetionDropDownMenuSearchValue, icon: Icons.wb_incandescent),
    const Choice(title: CompetionDropDownMenuCreateValue, icon: Icons.search),
  ];

  @override
  State<StatefulWidget> createState() {
    return new HomePageState();
  }
}

class HomePageState extends State<HomePage2> with TickerProviderStateMixin {

  List<DropdownMenuItem<String>> genreListDrop  = [];

  void loadGenre(){

    genreListDrop.add(
        new DropdownMenuItem(child: new Text('Item No. 1'),
          value: 'Genre',

        )
    );

    setState(() {

    });

  }

  static const platform = const MethodChannel('sexy');
  bool showSearchIconInAppbar = false;
  static const String GAME_FRAGMENT_TAG = "Game Fragment";

  AnimationController _controller;
  Animation _animationFadeClose, _animationFadeOpen;
  bool _fadeInSearchbar = false, _fadeInDefaultbar = false;
  SharedPreferenceHelper sharedPreference = new SharedPreferenceHelper();

  User user = new User();

  String userName = "";
  final TextEditingController _searchQuery = new TextEditingController();
  String searchText = "";

  SearchCallback searchCallback;

  CompetitionOptionDropDownMenuCallback competitionPageDropDownCallback;

  void loadUsersDetails() async {
    user = await sharedPreference.getUser();

    setState(() {
      userName = user.userName;
    });
  }

  @override
  void initState() {
    super.initState();

    loadUsersDetails();
    loadGenre();
    _controller =
        AnimationController(vsync: this, duration: Duration(seconds: 8));

    _animationFadeClose = Tween(begin: -1.0, end: 0.0).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.fastOutSlowIn,
    ));

    _animationFadeOpen = Tween(begin: 0.0, end: -1.0).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.bounceOut,
    ));

    firebaseCloudMessaging_Listeners();
    appBar = _getDefaultAppbarNoSearchIcon();
    _setUpSearchQuery();
    MethodChannelUtility.connectToChatRelayServer().then((isSuccessful) async {
      SocketIOLogin socketIOLogin = new SocketIOLogin();

      socketIOLogin.username = user.userName;

      socketIOLogin.password = "";

      MethodChannelUtility.loginToSocketIOServer(socketIOLogin);
    });

    MethodChannelUtility.nativePlatformPortal((MethodCall call) {
      if (call.method == "callTimer") {
        String cc = call.arguments;
      } else if (call.method == "receivingMessage") {
      } else if (call.method == "sentMessageSeen") {
      } else if (call.method == "sentMessageDelivered") {}
    });
  }

  int _selectedDrawerIndex = 0;

  _getDrawerItemWidget(int pos) {
    switch (pos) {
      case 0:
        return new HomeFragment();
      case 1:
        return new UserProfile();
      case 2:
        return new Friends(homePageState: this);
      case 3:
        return new DepositFragment();

      case 4:
        return new ChatList(
          homePageState: this,
        );

      case 5:
        return new SettingsFragment();

      case 6:
        return new TransactionsFragment();

      case 7:
        return new ChatList(homePageState: this);

      case 8:
        return new CardDemo();

      case 9:
        return new OverviewFragment();

      case 10:
        return new GeneralNotificationFragment();

      case 11:
        return new GamificationFragment();

      case 12:
        return new CompetitionFragment(homePageState: this);

      case 13:
        return new GamesFragment(homePageState: this);

      case 14:
        return new UsersFragment();

      default:
        return new Text("Error");
    }
  }

  _onSelectItem(int index) {
    setState(() {
      _selectedDrawerIndex = index;
      switch (index) {
        case 2:
        case 4:
        case 7:
          print("AppBar set to Appbar WIth Icon");
          appBar = _getDefaultAppbarWithSearchIcon();
//          appBar = _defaultAppBarWithSearchIconPrefWidget();
          break;
        case 12:
          appBar = _getDefaultAppbarNoSearchIcon(actions: <Widget>[
            // overflow menu
            PopupMenuButton<Choice>(
                onSelected: (choice) {
//                  print(
//                      "Selected PopupMenuButton choice is --- " + choice.title);
//                  competitionPageDropDownCallback(choice.title);
                },
                itemBuilder: (BuildContext context) => <PopupMenuItem<Choice>>[
                      new PopupMenuItem<Choice>(
                          child: new ListTile(
                        trailing: new Icon(widget.choices[0].icon),
                        title: new Text(widget.choices[0].title),
                        onTap: () {
                          competitionPageDropDownCallback(
                              widget.choices[0].title);
                          print("Selected PopupMenuButton choice is --- " +
                              widget.choices[0].title);
                        },
                      )),
                      new PopupMenuItem<Choice>(
                          child: new ListTile(
                              trailing: new Icon(widget.choices[1].icon),
                              title: new Text(widget.choices[1].title),
                              onTap: () {
                                competitionPageDropDownCallback(
                                    widget.choices[1].title);
                                print(
                                    "Selected PopupMenuButton choice is --- " +
                                        widget.choices[1].title);
                              }))
                    ]),
          ]);
          break;
        case 13:
          appBar =
              _getDefaultAppbarWithSearchIcon(fragmentTag: GAME_FRAGMENT_TAG);
          break;
        default:
          print("AppBar set to Appbar WIth NO Icon");
          appBar = _getDefaultAppbarNoSearchIcon();
      }
    });
    Navigator.of(context).pop(); // close the drawer
  }

  PreferredSizeWidget appBar;

  @override
  Widget build(BuildContext context) {
    var drawerOptions = <Widget>[];

    drawerOptions.add(new ListTile(
        leading: new Icon(Icons.dashboard),
        title: new Text("Home", style: TextStyle(fontFamily: "Montserrat", fontSize: 15.0),),
        onTap: () => _onSelectItem(0)));

    drawerOptions.add(ExpansionTile(
      leading: Icon(Icons.account_circle),
      title: Text(
        "Social",
        style: TextStyle(fontFamily: "Montserrat", fontSize: 15.0),
      ),
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
            left: 0.0,
          ),
          child: ListTile(
              leading: new Icon(Icons.account_circle),
              title: Text("Profile", style: TextStyle(fontFamily: "Montserrat", fontSize: 15.0),),
              onTap: () => _onSelectItem(1)),
        ),
        Padding(
          padding: EdgeInsets.only(
            left: 0.0,
          ),
          child: ListTile(
            leading: new Icon(Icons.people),
            title: Text("Friends", style: TextStyle(fontFamily: "Montserrat", fontSize: 15.0),),
            onTap: () => _onSelectItem(2),
          ),
        ),

        Padding(
          padding: EdgeInsets.only(
            left: 0.0,
          ),
          child: ListTile(
            leading: new Icon(Icons.people),
            title: Text("Users", style: TextStyle(fontFamily: "Montserrat", fontSize: 15.0),),
            onTap: () => _onSelectItem(14),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            left: 0.0,
          ),
          child: ListTile(
            leading: BadgeIconButtono(
                itemCount: 1,
                badgeColor: Colors.blue,
                badgeTextColor: Colors.white,
                icon: new Icon(Icons.people),
                onPressed: () {}),
            title: Text("Friend Requests", style: TextStyle(fontFamily: "Montserrat", fontSize: 15.0),),
            onTap: () => _onSelectItem(8),
          ),
        ),
      ],
    ));

    drawerOptions.add(ExpansionTile(
      leading: Icon(Icons.inbox),
      title: Text(
        "Activities",
        style: TextStyle(fontWeight: FontWeight.w500, fontSize: 15.0),
      ),
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
            left: 0.0,
          ),
          child: ListTile(
              leading: new Icon(FontAwesomeIcons.award),
              title: Text("Competition"),
              onTap: () => _onSelectItem(12)),
        ),
        Padding(
          padding: EdgeInsets.only(
            left: 0.0,
          ),
          child: ListTile(
              leading: new Icon(Icons.gamepad),
              title: Text("Stats"),
              onTap: () => _onSelectItem(1)),
        ),
        Padding(
          padding: EdgeInsets.only(
            left: 0.0,
          ),
          child: ListTile(
              leading: new Icon(Icons.gamepad),
              title: Text("Games"),
              onTap: () => _onSelectItem(13)),
        ),
      ],
    ));

    drawerOptions.add(ExpansionTile(
      leading: Icon(Icons.credit_card),
      title: Text(
        "Wallet",
        style: TextStyle(fontWeight: FontWeight.w500, fontSize: 15.0),
      ),
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
            left: 0.0,
          ),
          child: ListTile(
            leading: new Icon(Icons.dashboard),
            title: Text("Overview"),
            onTap: () => _onSelectItem(9),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            left: 0.0,
          ),
          child: ListTile(
              leading: new Icon(Icons.attach_money),
              title: Text("Deposit"),
              onTap: () => _onSelectItem(3)),
        ),
        Padding(
          padding: EdgeInsets.only(
            left: 0.0,
          ),
          child: ListTile(
            leading: new Icon(Icons.credit_card),
            title: Text("Withdraw"),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            left: 0.0,
          ),
          child: ListTile(
              leading: Icon(Icons.table_chart),
              title: Text("Transactions"),
              onTap: () => _onSelectItem(6)),
        ),
      ],
    ));

    drawerOptions.add(new ListTile(
        leading: BadgeIconButtono(
            itemCount: 1,
            badgeColor: Colors.blue,
            badgeTextColor: Colors.white,
            icon: new Icon(Icons.chat),
            onPressed: () {}),
        title: new Text("Message"),
        onTap: () => _onSelectItem(7)));

    drawerOptions.add(ExpansionTile(
      leading: Icon(Icons.inbox),
      title: Text(
        "Notification",
        style: TextStyle(fontWeight: FontWeight.w500, fontSize: 15.0),
      ),
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
            left: 0.0,
          ),
          child: ListTile(
              leading: new Icon(Icons.account_circle),
              title: Text("General"),
              onTap: () => _onSelectItem(10)),
        ),
        Padding(
          padding: EdgeInsets.only(
            left: 0.0,
          ),
          child: ListTile(
              leading: new Icon(Icons.account_circle),
              title: Text("Gamification"),
              onTap: () => _onSelectItem(11)),
        ),
      ],
    ));

    drawerOptions.add(new Divider(
      color: Colors.blue,
    ));

    drawerOptions.add(new ListTile(
      leading: new Icon(Icons.settings),
      title: new Text("Settings"),
      selected: false,
      onTap: () => _onSelectItem(5),
    ));

    drawerOptions.add(new ListTile(
      leading: new Icon(Icons.settings_power),
      title: new Text("Logout"),
      selected: false,
      onTap: () async {
        bool isCleared =
            await new SharedPreferenceHelper().clearSharedPreference();
        if (isCleared) {
          Navigator.of(context).pushReplacementNamed('/login');
        }
      },
    ));

    return new Container(
      height: double.infinity,
      width: double.infinity,
      decoration: new BoxDecoration(
        color: Colors.white,
          image: new DecorationImage(

        image: new AssetImage('images/background_main.png'),
            fit: BoxFit.cover,
      )),
      child: new Scaffold(
        backgroundColor: Colors.transparent,
        appBar: appBar,
        drawer: SizedBox(
            width: 240.0,
            child: new Drawer(
              child: new Column(
                children: <Widget>[
                  new UserAccountsDrawerHeader(
                      accountName:
                          new Text(user.userName == null ? "" : user.userName),
                      accountEmail:
                          new Text(user.email == null ? "" : user.email),
                      currentAccountPicture: new GestureDetector(
                        child: new CircleAvatar(
                          backgroundImage: NetworkImage(
                              'http://40.87.11.131:3000/images/profile-pictures/${user.picture}'),
                        ),
                        onTap: () => print("This is your current account."),
                      ),
                      decoration: new BoxDecoration(
                          image: new DecorationImage(
                              image: new NetworkImage(
                                  "https://img00.deviantart.net/35f0/i/2015/018/2/6/low_poly_landscape__the_river_cut_by_bv_designs-d8eib00.jpg"),
                              fit: BoxFit.fill))),
                  Expanded(
                    child: ListView(
                      children: drawerOptions,
                    ),
                  )
                ],
              ),
            )),
        body: _getDrawerItemWidget(_selectedDrawerIndex),
      ),
    );
  }

  ///This method is called in the required pages to initialize the SearchCallback
  ///callback to enalble the callback method to be called when text changes in the
  ///search bar
  initSearchCallback(SearchCallback searchCallback) {
    this.searchCallback = searchCallback;
  }

  initCompetitionDropDownMenuCallback(
      CompetitionOptionDropDownMenuCallback callback) {
    this.competitionPageDropDownCallback = callback;
  }

  _fadeInSearchbarTrue() {
    setState(() {
      print("fadeInSearchbarTrue called --- ");
      _fadeInSearchbar = true;
    });
  }

  var defaultAppbarKey = new GlobalKey();

  ///This method returns the default appbar with search icon
  _getDefaultAppbarWithSearchIcon({String fragmentTag}) {
    print(
        "showSearchIconInAppbar -------- " + showSearchIconInAppbar.toString());
    return new AppBar(
      key: defaultAppbarKey,
      // here we display the title corresponding to the fragment
      // you can instead choose to have a static title
      elevation: 0.0,

      title: new Text(widget.drawerItems[_selectedDrawerIndex].title),
      actions: <Widget>[
        new IconButton(
          icon: new Icon(Icons.search, color: Colors.blue),
          onPressed: () {
            _searchIconClicked(fragmentTag: fragmentTag);
          },
        )
      ],
    );
  }

  ///This method returns the default appbar with no search icon
  _getDefaultAppbarNoSearchIcon({List<Widget> actions}) {
    print(
        "showSearchIconInAppbar -------- " + showSearchIconInAppbar.toString());
    return new AppBar(
        // here we display the title corresponding to the fragment
        // you can instead choose to have a static title
        elevation: 0.0,

        title: new Text(widget.drawerItems[_selectedDrawerIndex].title),
        actions: actions != null ? actions : <Widget>[]);
  }

  bool _searchIconClcked = false;

  ///This method is called when the search icon is clicked in the default appbar
  ///it switches the default appbar to the search appbar
  _searchIconClicked({String fragmentTag}) {
    setState(() {
      if (fragmentTag != null && fragmentTag == GAME_FRAGMENT_TAG) {
        print("Games searchIcon clicked ---- ");
        _showGamesFragmentDialog();
      } else {
        _searchIconClcked = true;
        // _animateSearchBarCloseButton();
        print("SearchIcon clicked ooh");
        // _fadeInSearchbarTrue();
        appBar = _getSearchAppbar();
//      _fadeInDefaultbarFalse();
      }
    });
  }

  var gamesFragmentDialogTextController = new TextEditingController();

  ///This is called to open up a dialog when the search icon for games fragment
  ///is clicked
  _showGamesFragmentDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return new CustomAlertDialog(
            title: new Container(
                height: 40.0,
                decoration: new BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.only(
                        topLeft: new Radius.circular(5.0),
                        topRight: new Radius.circular(5.0))),
                child: new Center(
                    child: new Text("Search a game",
                        style: new TextStyle(color: Colors.white)))),
            content: new Container(
              height: 250.0,
              child: Padding(
                  padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
                  child: new Column(
                  children: <Widget>[
                   new TextFormField(
                     controller: gamesFragmentDialogTextController,
                     focusNode: new FocusNode(),
                     decoration: InputDecoration(

                       fillColor: Colors.white,
                       hintText: "Game name",
                     ),
                   ),
                   Container(
                     height: 70.0,
                     child: new DropdownButton(items: genreListDrop, onChanged: null,),
                   ),
                    new Container(
                      margin: EdgeInsets.only(top: 8.0),
                      alignment: Alignment.bottomCenter,
                      child: new FlatButton(
                        padding: EdgeInsets.only(
                            left: 20.0, right: 20.0, top: 5.0, bottom: 5.0),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0)),

                        child: new Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              new Text(
                                "Search...",
                                style: new TextStyle(
                                    color: Colors.white, fontSize: 18.0),
                              )
                            ]),
                        onPressed: () {
                          searchCallback(
                              gamesFragmentDialogTextController.value.text);
                        },
                        splashColor: Colors.orangeAccent,
                        color: Colors.blueAccent,
                      ),
                    ),
                  ],
                ),
              )
            ),
          );
        });
  }

  ///This method is called when the back icon is clicked in the search appbar
  ///it switches the search appbar to the default appbar
  _backButtonInSearchAppbarClicked() {
    final double width = MediaQuery.of(context).size.width;

    setState(() {
      appBar = _getDefaultAppbarWithSearchIcon();
      // _animateClosingSearchBar(width);
//      _stopSearchbarButtonAnimation();
//      _fadeInSearchbarFalse();
//      _fadeInDefaultbarTrue();
    });
  }


  AnimationController _searchbarCloseButtonOpacityController;
  Animation<double> _searchbarCloseButtonOpacityAnim;

  AnimationController _searchbarAnimeController;
  Animation<Offset> _searchbarAnimation;

  ///This animation is done on the searchbar close button when search button on the appbar is clicked
  _animateSearchBarCloseButton() {
    _searchbarCloseButtonOpacityController = new AnimationController(
        vsync: this, duration: const Duration(milliseconds: 800));
    _searchbarCloseButtonOpacityAnim = new CurvedAnimation(
        parent: _searchbarCloseButtonOpacityController, curve: Curves.easeInOut)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          _searchbarCloseButtonOpacityController.reverse();
        } else if (status == AnimationStatus.dismissed) {
          _searchbarCloseButtonOpacityController.forward();
        }
      });

    _searchbarCloseButtonOpacityController.forward();
  }

  ///This animation is done on the searchbar when search button on the appbar is clicked
  _animateSearchBar(double width) {
    _searchbarAnimeController = new AnimationController(
        vsync: this, duration: const Duration(milliseconds: 8000));
    _searchbarAnimeController.forward();
//    int val = width * 2;
    _searchbarAnimation =
        Tween<Offset>(begin: Offset.zero, end: Offset(-1.0, 0.0))
            .animate(_searchbarAnimeController);

    _searchbarAnimation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
//        _searchbarAnimeController.reverse();
        _searchbarAnimeController.dispose();
      }
    });
    _searchbarAnimation.addListener(() {
      setState(() {
        print("Searchbar animation value width ooh --- " +
            (_searchbarAnimation.value.dx).toString());
      });
    });
  }

  ///This animation is done on the searchbar when d user closes it
  _animateClosingSearchBar(double width) {
    _searchbarAnimeController = new AnimationController(
        vsync: this, duration: const Duration(milliseconds: 6000));
    _searchbarAnimeController.forward();

    _searchbarAnimation =
        new Tween<Offset>(begin: Offset.zero, end: Offset(1.0, 0.0)).animate(
      new CurvedAnimation(
        parent: _searchbarAnimeController,
        curve: Curves.ease,
      ),
    );

    _searchbarAnimation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _searchbarAnimeController.dispose();
      }
    });
    _searchbarAnimation.addListener(() {
      setState(() {
        print("Searchbar animation value ooh --- " +
            _searchbarAnimeController.value.toString());
      });
    });
  }

  _stopSearchbarButtonAnimation() {
    _searchbarCloseButtonOpacityController.dispose();
  }

  ///This method returns the search appbar
  _getSearchAppbar() {
    final double width = MediaQuery.of(context).size.width;
    _animateSearchBar(width);
    print("Width oooh ---- " + width.toString());
//    print("Searchbar animation value ooh --- " + _searchbarAnimeController.value.toString());
    return new AppBar(
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      leading: new IconButton(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.only(left: 4.0, right: 2.0),
          icon: new Icon(Icons.close, color: Colors.blue),
          onPressed: _backButtonInSearchAppbarClicked),
      // new FadeTransition(
      //   opacity: _searchbarCloseButtonOpacityAnim,
      //   child: new IconButton(
      //       alignment: Alignment.centerLeft,
      //       padding: EdgeInsets.only(left: 4.0, right: 2.0),
      //       icon: new Icon(Icons.close),
      //       onPressed: _backButtonInSearchAppbarClicked),
      // ),
      titleSpacing: 4.0,
      title: TextFormField(
        autofocus: true,
        controller: _searchQuery,
        decoration: new InputDecoration(
          hintText: "Search..",
          filled: true,
          fillColor: Colors.white,
//                border: new OutlineInputBorder(
//                    borderRadius: new BorderRadius.circular(10.0)),
        ),
        keyboardType: TextInputType.text,
        style: new TextStyle(color: Colors.black87, fontSize: 15.0),
      ),
//              SlideTransition(
//                position: _searchbarAnimation,
//               child:
//               TextFormField(
//                 autofocus: true,
//                 controller: _searchQuery,
//                 decoration: new InputDecoration(
//                   hintText: "Search..",
//                   filled: true,
//                   fillColor: Colors.white,
// //                border: new OutlineInputBorder(
// //                    borderRadius: new BorderRadius.circular(10.0)),
//                 ),
//                 keyboardType: TextInputType.text,
//                 style: new TextStyle(color: Colors.black87, fontSize: 15.0),
//               ),
//             )
//          },
//        )
    );
  }

  ///This method is called in the initState to set up the search query controller
  _setUpSearchQuery() {
    _searchQuery.addListener(() {
//      if (_searchQuery.text.isEmpty) {
//        setState(() {
//          //TODO: Show toast with message "Search cant be empty"
//        });
//      } else {
      setState(() {
        searchText = _searchQuery.text;
        _onSearchQueried();
      });
//      }
    });
  }

  ///This method is called to when text in seacrh bar changes
  ///it calls he call back method
  void _onSearchQueried() {
    searchCallback(searchText);
  }

  void firebaseCloudMessaging_Listeners() {
    if (Platform.isIOS) iOS_Permission();

    widget._firebaseMessaging.getToken().then((token) {
      print("this is a token $token");

      if (token.isNotEmpty || token != null) registerTokenToServer(token);
    });

    widget._firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');

        AudioCache player = new AudioCache();
        const alarmAudioPath = "sounds/notification.mp3";
        player.play(alarmAudioPath);
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },
    );
  }

  void iOS_Permission() {
    widget._firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    widget._firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  void registerTokenToServer(String token) async {
    FirebaseModel firebaseModel = FirebaseModel();

    firebaseModel.token = token;

    ApiHelper apiHelper = new ApiHelper();

    HttpClientResponse response = await apiHelper.registerTokenWithUser(token);

    if (response.statusCode == 200 || response.statusCode == 201) {

      String jsonString = await response.transform(utf8.decoder).join();

      //FirebaseModel firebaseModel = new FirebaseModel.fromJson(json.decode(jsonString));

      new SharedPreferenceHelper().setFirebaseToken(firebaseModel.token);

    } else if (response.statusCode == 401) {
    } else {}
  }
}
