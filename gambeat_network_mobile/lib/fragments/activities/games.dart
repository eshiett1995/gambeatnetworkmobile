import 'dart:convert';
import 'dart:io';

import 'package:flutter_appavailability/flutter_appavailability.dart';
import 'package:Gambeat/models/rest-models/request/GameSearchModel.dart';
import 'package:Gambeat/models/rest-models/response/GamePageModel.dart';
import 'package:Gambeat/network/api/ApiHelper.dart';
import 'package:flutter/material.dart';
import 'package:Gambeat/models/Game.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

class GamesFragment extends StatefulWidget {
  var homePageState;

  GamesFragment({this.homePageState});

  @override
  _GamesFragmentState createState() => _GamesFragmentState();
}

class _GamesFragmentState extends State<GamesFragment> {
  String searchKeyword = "";
  List<Game> games = new List();
  GamePageModel gamePageModel;
  int currentPageNumber = 0;
  bool isLastPage = false;
  int endOfPageCalled;
  int currentLength = 0;
  bool _isInitialListLoading = false;
  int totalNumberOfPages = 0;
  bool _isSubsequentListLoading = false;

  @override
  initState() {
    super.initState();
    widget.homePageState.initSearchCallback((searchKeyword) {
      setState(() {
        this.searchKeyword = searchKeyword;
      });
    });
    _getGamesFromServer();
  }

  ///-------------------------------------------------------------------------
  ///This method sets state setting _initialListLoading bool var to false
  ///so as to control the progressbar indicating initial list items are being
  ///fetched from the api
  ///-------------------------------------------------------------------------
  _setInitialListLoadingFalse() {
    setState(() {
      _isInitialListLoading = false;
    });
  }

  ///This method sets state setting _initialListLoading bool var to true
  ///so as to control the progressbar indicating initial list items are being
  ///fetched from the api
  _setInitialListLoadingTrue() {
    setState(() {
      _isInitialListLoading = true;
    });
  }

  void _setSubsequentListLoadingTrue() {
    setState(() {
      _isSubsequentListLoading = true;
    });
  }

  void _setSubsequentListLoadingFalse() {
    setState(() {
      _isSubsequentListLoading = false;
    });
  }

  Widget _getBottomProgressBar() {
    return new LinearProgressIndicator();
  }


  void _getGamesFromServer() async {
    _setInitialListLoadingTrue();

    ApiHelper apiHelper = new ApiHelper();

    HttpClientResponse response =
        await apiHelper.getAllGames(1, new GameSearchModel());

    String mm = await response.transform(utf8.decoder).join();

    print(mm);

    var jsonString = json.decode(mm);

    if (response.statusCode == 200) {
      _setInitialListLoadingFalse();

      GamePageModel gamePageModel = GamePageModel.fromJson(jsonString);
      totalNumberOfPages = gamePageModel.totalPage;
      setState(() {
        gamePageModel.content.forEach((game) {
          games.add(game);
        });
      });
    } else {
      _setInitialListLoadingFalse();
    }
  }

  void showGameInfo({Game game}) {
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return new Container(
              padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
              color: Colors.white,
              child: ListView(
                children: <Widget>[
                  new Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(
                        height: 1.0,
                      ),
                      new Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          new Image.network(
                            "http://40.87.11.131:3000/images/games/${game.image}",
                            width: 200.0,
                          ),
                          new Container(
                              margin: EdgeInsets.only(left: 15.0),
                              child: new Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Text(
                                    game.name,
                                    style: TextStyle(
                                      fontSize: 20.0,
                                      color: Colors.blue,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  Text(
                                    game.genres.first.name,
                                    style: TextStyle(
                                      color: Colors.black54,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  new Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.laptop,
                                        color: Colors.blue,
                                        size: 15.0,
                                      ),
                                      SizedBox(
                                        width: 15.0,
                                      ),
                                      Icon(
                                        Icons.android,
                                        color: Colors.blue,
                                        size: 15.0,
                                      ),
                                      SizedBox(
                                        width: 15.0,
                                      ),
                                      Icon(
                                        Icons.phone_iphone,
                                        color: Colors.blue,
                                        size: 15.0,
                                      ),
                                    ],
                                  )
                                ],
                              ))
                        ],
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Text(
                        "          Please note that every match is supposed to be played within 2 days of notification and " +
                            "any delay in match will lead to your disqualification also we advice that you should be in constant" +
                            "communication with your competitor, so as to set a date and time for the match.",
                        style: TextStyle(
                          height: 1.3,
                        ),
                        textAlign: TextAlign.justify,
                      ),
                      Text(
                        " Finally, please note that you have the option to forfiet the match, the forfiet button is below.",
                        style: TextStyle(
                          height: 1.3,
                        ),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      RaisedButton(
                          onPressed: () async {

                             Future.forEach(game.platformLinks, (platformLink) async {

                               print(platformLink.url);

                              AppAvailability.launchApp(platformLink.url)
                                  .then((_)
                                  {
                                    print("good" + platformLink.url);

                                  })
                                  .catchError((err) {

                                     print("error" + platformLink.url);

                                  });

                            });

                            game.platformLinks.forEach((platformLink) =>{



                            });

                          },
                          color: Colors.blue,
                          child: Container(
                            width: double.infinity,
                            height: 50.0,
                            child: Center(
                              child: Text(
                                "OPEN GAME",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          )),
                      SizedBox(
                        height: 30.0,
                      ),
                      RaisedButton(
                          onPressed: () {},
                          color: Colors.red,
                          child: Container(
                            width: double.infinity,
                            height: 50.0,
                            child: Center(
                              child: Text(
                                "DOWNLOAD GAME",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          )),
                    ],
                  ),
                ],
              ));
        });
  }

  _getGridList() {
    return new GridView.builder(
        gridDelegate:
            new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemCount: games == null ? 0 : games.length,
        itemBuilder: (BuildContext context, int index) {
          Game game = games[index];

          return new GridTile(
              child: new GestureDetector(
            onTap: () {
              showGameInfo(game: game);
            },
            child: new Card(
              elevation: 4.0,
              margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 8.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  AspectRatio(
                    aspectRatio: 20.0 / 12.0,
                    child: Image.network(
                      "http://40.87.11.131:3000/images/games/${game.image}",
                      fit: BoxFit.cover,
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.fromLTRB(7.0, 0.0, 4.0, 2.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        SizedBox(
                          height: 3.0,
                        ),
                        Text(
                          game.name,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: TextStyle(
                            fontSize: 12.0,
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          height: 4.0,
                        ),
                        Text(
                          game.genres.first.name,
                          style: TextStyle(
                            color: Colors.black54,
                            fontSize: 9.0,
                          ),
                        ),
                        SizedBox(
                          height: 7.0,
                        ),
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(right: 4.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.laptop,
                                        color: Colors.blue,
                                        size: 15.0,
                                      ),
                                      SizedBox(
                                        width: 15.0,
                                      ),
                                      Icon(
                                        Icons.android,
                                        color: Colors.blue,
                                        size: 15.0,
                                      ),
                                      SizedBox(
                                        width: 15.0,
                                      ),
                                      Icon(
                                        Icons.phone_iphone,
                                        color: Colors.blue,
                                        size: 15.0,
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ));
        });
  }

  void _lazyLoading() {
    endOfPageCalled = endOfPageCalled + 1;
    print("===========================================================");
    print("LazyMethod Called!! --- end of Page");
    print("===========================================================");
    if (!(endOfPageCalled > 1)) //This is so to prevent multiple calling
    if (!(currentPageNumber >= totalNumberOfPages)) {
      print(
          "currentPageNumber NOT greater than or equal to totalNumberOfPages -- ");
      currentPageNumber = currentPageNumber + 1;
      _loadMoreGamesFromServer(pageNum: currentPageNumber);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: new BoxDecoration(
            color: Colors.white,
            image: new DecorationImage(
              image: new AssetImage('images/background_main.png'),
              fit: BoxFit.cover,
            )),
        child: new Column(
          children: <Widget>[
            new Expanded(
              child: _isInitialListLoading
                  ? Container(
//                color:Colors.blueAccent,
                      height: MediaQuery.of(context).size.height,
                      decoration: new BoxDecoration(
                          image: new DecorationImage(
                        image: new AssetImage('images/background_main.png'),
                        fit: BoxFit.cover,
                      )),
                      child: Center(child: CircularProgressIndicator()),
                    )
                  : LazyLoadScrollView(
                      child: _getGridList(),
                      onEndOfPage: _lazyLoading,
                    ),
            ),
            _isSubsequentListLoading ? _getBottomProgressBar() : Center()
          ],
        ));
  }

  void _loadMoreGamesFromServer({int pageNum}) async {
    _setSubsequentListLoadingTrue();
    print("got ete");

    ApiHelper apiHelper = new ApiHelper();

    HttpClientResponse c =
        await apiHelper.getAllGames(pageNum, new GameSearchModel());

    //var mm = json.decode(response.body);

    String mm = await c.transform(utf8.decoder).join();

    var jsonString = json.decode(mm);

    if (c.statusCode == 200) {
      GamePageModel gamePageModel = GamePageModel.fromJson(jsonString);

      setState(() {
        endOfPageCalled = 0;
        gamePageModel.content.forEach((game) {
          games.add(game);
        });
      });
      _setSubsequentListLoadingFalse();
    } else {
      //TODO: Display a toast or snackbar that getting the friends from server failed
      _setSubsequentListLoadingFalse();
      Scaffold.of(context).showSnackBar(new SnackBar(
        content: new Text("Error in getting games"),
      ));
    }
  }
}
